﻿// ReSharper disable InconsistentNaming

namespace JNBDemo.Constants
{
    public class Cipher
    {
        public const int VERSION_SIZE = 8;
        public const int VERSION_SIZE_BYTES = 4;
    }

    public class DSSCubeStates
    {
        public const int DssCubeReserved = 0;
        public const int DssCubeProcessing = 1;
        public const int DssCubeActive = 2;
        public const int DssCubePersisted = 4;
        public const int DssCubeInfoDirty = 8;
        public const int DssCubeDirty = 16;
        public const int DssCubeLoaded = 32;
        public const int DssCubeReady = 64;
        public const int DssCubeLoadPending = 128;
        public const int DssCubeUnloadPending = 256;
        public const int DssCubePendingForEngine = 512;
        public const int DssCubeImported = 1024;
        public const int DssCubeForeign = 2048;
    }

    public class DSSExportFormat
    {
        public const int DssExportFormatReserved = 0;
        public const int DssExportFormatExcel = 1;
        public const int DssExportFormatPdf = 2;
        public const int DssExportFormatCSV = 3;
        public const int DssExportFormatHtml = 4;
        public const int DssExportFormatPlainText = 5;
        public const int DssExportFormatXml = 6;
        public const int DssExportFormatFlash = 7;
        public const int DssExportFormatView = 8;
        public const int DssExportFormatInteractive = 9;
        public const int DssExportFormatEditable = 10;
        public const int DssExportFormatExportFlash = 11;
        public const int DssExportFormatFlashInMht = 12;
        public const int DssExportFormatFlashInPdf = 13;
        public const int DssExportFormatMobileSmartPhone = 14;
        public const int DssExportFormatMobileTablet = 15;
        public const int DssExportFormatInstance = 100;
    }

    public class DSSExtendedType
    {
        public const int DssExtendedTypeRelational = 1;
        public const int DssExtendedTypeMDX = 2;
        public const int DssExtendedTypeCustomSQLFreeForm = 3;
        public const int DssExtendedTypeCustomSQLWizard = 4;
        public const int DssExtendedTypeDataImport = 256;
        public const int DssExtendedTypeDataImportFileExcel = 272;
        public const int DssExtendedTypeDataImportFileText = 288;
        public const int DssExtendedTypeDataImportCustomSQL = 304;
        public const int DssExtendedTypeDataImportTable = 320;
        public const int DssExtendedTypeDataImportCustomSQLWizard = 352;
    }

    public class DSSPropagateACLFlags
    {
        public const int DssPropagateACLDirectChildren = 1;
        public const int DssPropagateACLReplace = 4;
        public const int DssPropagateACLPrecisely = 8;
    }

    public class DSSRWDCacheStates
    {
        public const int DssRWDCacheStatusReserved = 0;
        public const int DssRWDCacheStatusReady = 1;
        public const int DssRWDCacheStatusProcessing = 2;
        public const int DssRWDCacheStatusInvalid = 4;
        public const int DssRWDCacheStatusExpired = 8;
        public const int DssRWDCacheStatusLoaded = 16;
        public const int DssRWDCacheStatusPersisted = 32;
        public const int DssRWDCacheStatusInfoDirty = 64;
        public const int DssRWDCacheStatusResultDirty = 128;
        public const int DssRWDCacheStatusConvertedFromReport = 256;
        public const int DssRWDCacheStatusForeign = 512;
    }

    public class DSSServerCmd
    {
        public const int KillCache = 0;
        public const int CloseJob = 1;
        public const int Echo = 2;
        public const int SignIn = 3;
        public const int GetServerEnv = 4;
        public const int GetServerStatus = 5;
        public const int GetErrorMsg = 6;
        public const int GetFinishedJobs = 7;
        public const int GetJobStatus = 8;
        public const int GetJobStatusAll = 9;
        public const int GetSQLstmtCount = 10;
        public const int GetSQL = 11;
        public const int IsFinishedJobsInfoReady = 12;
        public const int KillJob = 13;
        public const int RequestFinishedJobs = 14;
        public const int SignOut = 15;
        public const int PreSubmitJob = 16;
        public const int SubmitJob = 17;
        public const int GetServerTime = 18;
        public const int GetJobInfo = 19;
        public const int GetJobStatusEx = 20;
        public const int GetJobStatusAllEx = 21;
        public const int GetServerEnvEx = 22;
        public const int GetThreadInfo = 23;
        public const int DisplayAllJobs = 24;
        public const int DisplayAllThreads = 25;
        public const int RunSinglePassSQL = 26;
        public const int GenerateSQL = 27;
        public const int GetJobResult = 28;
        public const int GetJobResultIfReady = 29;
        public const int GetReportResult = 30;
        public const int GetMDObject = 31;
        public const int RunReport = 32;
        public const int GetMDObjRoot = 33;
        public const int RunReportByPath = 34;
        public const int DisplayAllUserSessions = 35;
        public const int StopServer = 36;
        public const int LogoutUserFromProject = 37;
        public const int DisconnectUser = 38;
        public const int MDObjectRequest = 39;
        public const int DSSElementRequest = 40;
        public const int DSSReportRequest = 41;
        public const int DSSAutoPrompt = 42;
        public const int DSSOpenProject = 43;
        public const int DSSCloseProject = 44;
        public const int DSSIdleProject = 45;
        public const int DSSResumeProject = 46;
        public const int GetUserJobs = 47;
        public const int GetProjectInstances = 48;
        public const int ScheduleCommand = 49;
        public const int GetScheduleInstances = 50;
        public const int LoadScheduleObject = 51;
        public const int UnloadScheduleObject = 52;
        public const int LoadScheduleObjects = 53;
        public const int UnloadScheduleObjects = 54;
        public const int FireEvent = 55;
        public const int GetDBMonitors = 56;
        public const int GetDBRoleMonitors = 57;
        public const int GetDatabaseMonitors = 58;
        public const int SetDBCLoadLimit = 59;
        public const int SetDBCConnectionLimit = 60;
        public const int SetDBRoleLoadLimit = 61;
        public const int SetDBRoleConnectionLimit = 62;
        public const int AddRequest = 63;
        public const int CancelRequest = 64;
        public const int LoginStandard = 65;
        public const int LoginNTCredential = 66;
        public const int LoginAnonymous = 67;
        public const int Web_DSSServerName = 68;
        public const int Web_Status = 69;
        public const int Web_Open = 70;
        public const int Web_Close = 71;
        public const int Web_GetConnectionPoolSize = 72;
        public const int Web_PutConnectionPoolSize = 73;
        public const int Web_SharedConnections = 74;
        public const int Web_SharedConnectionClose = 75;
        public const int Ping = 76;
        public const int CloseUserSession = 77;
        public const int GetServerID = 78;
        public const int GetCollectionSetting = 79;
        public const int SetCollectionSetting = 80;
        public const int PurgeStatistics = 81;
        public const int DSSDSCreateProject = 82;
        public const int UpdateDiagnostics = 83;
        public const int LoginPassThru = 84;
        public const int GetServerMessages = 85;
        public const int ChangeInBoxMsgStatus = 86;
        public const int DeleteInBoxMsg = 87;
        public const int SaveInBoxToFile = 88;
        public const int RefreshSQLEngineSchema = 89;
        public const int RefreshUserObjects = 90;
        public const int ApplyRunTimeSettings = 91;
        public const int XMLRequest = 92;
        public const int ClusterJoin = 93;
        public const int ClusterMerge = 94;
        public const int ClusterLeave = 95;
        public const int ClusterSynchMetaData = 96;
        public const int ClusterSynchMembership = 97;
        public const int ClusterSynchServerLoad = 98;
        public const int ClusterSynchMemberInfo = 99;
        public const int ClusterBroadcast = 100;
        public const int ClusterGetMembership = 101;
        public const int ClusterReplyMembership = 102;
        public const int ClusterKeepAlive = 103;
        public const int ClusterPing = 104;
        public const int ClusterRemove = 105;
        public const int ClusterAdminJoin = 106;
        public const int ClusterAdminLeave = 107;
        public const int ClusterAdminRemove = 108;
        public const int GetDiagLogFlag = 109;
        public const int SetDiagLogFlag = 110;
        public const int DSSDocumentRequest = 111;
        public const int GetGeneralCacheInfo = 112;
        public const int GetReportCacheInfos = 113;
        public const int CacheAdministrating = 114;
        public const int SetCacheSwapPolicy = 115;
        public const int DocumentExecute = 116;
        public const int GetCachedReportInstance = 117;
        public const int AddCache = 118;
        public const int SnapShot = 119;
        public const int GetServerVersion = 120;
        public const int AllHeapByteCount = 121;
        public const int NTCounter = 122;
        public const int GetInBoxMsgResult = 123;
        public const int ChangeUserPassword = 124;
        public const int GetRequestResult = 125;
        public const int SummaryCounter = 126;
        public const int ServerMemMisc = 127;
        public const int GetClusterCacheHitCounts = 128;
        public const int GeminiXmlRequest = 132;
        public const int DSSServerCmdLast = 133;
    }

    public class DSSServerEncryption
    {
        public const int ServerEncryptionUnknown = -1;
        public const int ServerEncryptionMatching = 0;
        public const int ServerEncryptionNone = 1;
        public const int ServerEncryptionLowerLevel = 2;
    }

    public class DSSSourceManipulationSchema
    {
        public const int DssSourceManipulationTableKey = 1;
        public const int DssSourceManipulationEntryLevel = 2;
        public const int DssSourceManipulationLogicalSize = 4;
        public const int DssSourceManipulationSchemaAll = 65535;
    }

    public class DSSXML3DConnGrpMinorTypes
    {
        public const int DssXml3DConnGrpMinorTypeAreas = 1;
        public const int DssXml3DConnGrpMinorTypeRibbons = 2;
        public const int DssXml3DConnGrpMinorTypeSteps = 4;
    }

    public class DSSXML3DConnSeriesMinorTypes
    {
        public const int DssXml3DConnSeriesMinorTypeAreas = 1;
        public const int DssXml3DConnSeriesMinorTypeRibbons = 2;
        public const int DssXml3DConnSeriesMinorTypeSteps = 4;
    }

    public class DSSXML3DFloatMinorTypes
    {
        public const int DssXml3DFloatMinorTypeCubes = 1;
        public const int DssXml3DFloatMinorTypeSpheres = 2;
    }

    public class DSSXML3DRiserMinorTypes
    {
        public const int DssXml3DRiserMinorTypeBars = 1;
        public const int DssXml3DRiserMinorTypePyramids = 2;
        public const int DssXml3DRiserMinorTypeOctogans = 4;
        public const int DssXml3DRiserMinorTypeCutCorner = 8;
    }

    public class DSSXML3DScatterMinorTypes
    {
        public const int DssXml3DScatterMinorTypeXYZ = 1;
        public const int DssXml3DScatterMinorTypeXYZLabels = 2;
    }

    public class DSSXML3DSurfaceMinorTypes
    {
        public const int DssXml3DSurfaceMinorTypeSurface = 1;
        public const int DssXml3DSurfaceMinorTypeWithSides = 2;
        public const int DssXml3DSurfaceMinorTypeHoneycomb = 4;
    }

    public class DSSXMLAccessEntryType
    {
        public const int DssAccessEntryTypeReserved = 0;
        public const int DssAccessEntryTypeObject = 1;
        public const int DssAccessEntryTypeAudit = 2;
    }

    public class DSSXMLAccessRightFlags
    {
        public const int DssXmlAccessRightBrowse = 1;
        public const int DssXmlAccessRightUseExecute = 2;
        public const int DssXmlAccessRightRead = 4;
        public const int DssXmlAccessRightWrite = 8;
        public const int DssXmlAccessRightDelete = 16;
        public const int DssXmlAccessRightControl = 32;
        public const int DssXmlAccessRightUse = 64;
        public const int DssXmlAccessRightExecute = 128;
        public const int DssXmlAccessRightFullControl = 255;
        public const int DssXmlAccessRightInheritable = 536870912;
    }

    public class DSSXMLAEAggregation
    {
        public const int DssXmlAggregationDefault = 0;
        public const int DssXmlAggregationDisable = 1;
        public const int DssXmlAggregationEnable = 2;
    }

    public class DSSXMLAggregation
    {
        public const int DssXmlAggregationReserved = 0;
        public const int DssXmlAggregationNormal = 1;
        public const int DssXmlAggregationFirstInFact = 2;
        public const int DssXmlAggregationLastInFact = 3;
        public const int DssXmlAggregationFirstInRelationship = 4;
        public const int DssXmlAggregationLastInRelationship = 5;
    }

    public class DSSXMLApplicationType
    {
        public const int DssXmlApplicationOffice = 1;
        public const int DssXmlApplicationServerAdmin = 2;
        public const int DssXmlApplicationWebAdmin = 3;
        public const int DssXmlApplicationServer = 4;
        public const int DssXmlApplicationProjectUpgrade = 5;
        public const int DssXmlApplicationDSSWeb = 6;
        public const int DssXmlApplicationDSSScheduler = 7;
        public const int DssXmlApplicationCustomApp = 8;
        public const int DssXmlApplicationNarrowcastServer = 9;
        public const int DssXmlApplicationObjectManager = 10;
        public const int DssXmlApplicationODBOProvider = 11;
        public const int DssXmlApplicationODBOCubeDesigner = 12;
        public const int DssXmlApplicationCommandManager = 13;
        public const int DssXmlApplicationEnterpriseManager = 14;
        public const int DssXmlApplicationCommandLineInterface = 15;
        public const int DssXmlApplicationProjectBuilder = 16;
        public const int DssXmlApplicationConfigurationWizard = 17;
        public const int DssXmlApplicationMDScan = 18;
        public const int DssXmlApplicationCacheUtility = 19;
        public const int DssXmlApplicationFireEvent = 20;
        public const int DssXmlApplicationTypeJavaDesktop = 21;
        public const int DssXmlApplicationWebServices = 22;
        public const int DssXmlApplicationOfficeWS = 23;
        public const int DssXmlApplicationTools = 24;
        public const int DssXmlApplicationPortal = 25;
        public const int DssXmlApplicationTester = 26;
        public const int DssXmlApplicationMDUpdate = 27;
        public const int DssXmlApplicationCOMBrowser = 28;
        public const int DssXmlApplicationMobile = 29;
        public const int DssXmlApplicationBulkTranslationTool = 30;
        public const int DssXmlApplicationHealthCenter = 31;
        public const int DssXmlApplicationCubeAdvisor = 32;
    }

    public class DSSXMLAreaShapes
    {
        public const int DssXMLAreaShapePolygon = 6;
        public const int DssXMLAreaShapeRectangle = 7;
        public const int DssXMLAreaShapeCircle = 100;
    }

    public class DSSXMLAttributeDirection
    {
        public const int DssXmlDirectionReserved = 0;
        public const int DssXmlDirectionAttributeOnly = 1;
        public const int DssXmlDirectionParent = 2;
        public const int DssXmlDirectionChildren = 3;
        public const int DssXmlDirectionParentAndChildren = 4;
    }

    public class DSSXMLAttributeLockType
    {
        public const int DssXmlLockReserved = 0;
        public const int DssXmlLockNone = 1;
        public const int DssXmlLockCustom = 2;
        public const int DssXmlLockLimit = 3;
    }

    public class DSSXMLAuditorTypes
    {
        public const int DssXmlAuditorDefault = 0;
        public const int DssXmlAuditorIncludeLDAPGroups = 1;
    }

    public class DSSXMLAuditUserFilter
    {
        public const int DssXmlAuditEnabledUsers = 1;
        public const int DssXmlAuditDisabledUsers = 2;
        public const int DssXmlAuditIgnoreEnabledFlag = 3;
    }

    public class DSSXMLAuthModes
    {
        public const int DssXmlAuthStandard = 1;
        public const int DssXmlAuthNTCredential = 2;
        public const int DssXmlAuthPassThrough = 4;
        public const int DssXmlAuthAnonymous = 8;
        public const int DssXmlAuthLDAP = 16;
        public const int DssXmlAuthWarehousePassthrough = 32;
        public const int DssXmlAuthSimpleSecurityPlugIn = 64;
        public const int DssXmlAuthIntegrated = 128;
        public const int DssXmlAuthTrusted = 64;
    }

    public class DSSXMLAutoTextType
    {
        public const int DssXmlAutoTextPage = 1;
        public const int DssXmlAutoTextNPage = 2;
        public const int DssXmlAutoTextDateTime = 3;
        public const int DssXmlAutoTextUser = 4;
        public const int DssXmlAutoTextDocument = 5;
        public const int DssXmlAutoTextProject = 6;
        public const int DssXmlAutoTextGroupBy = 7;
        public const int DssXmlAutoTextPrompt = 8;
        public const int DssXmlAutoTextExecutionTime = 9;
        public const int DssXmlAutoTextReport = 10;
        public const int DssXmlAutoTextReportFilterDetails = 11;
        public const int DssXmlAutoTextReportTemplateDetails = 12;
        public const int DssXmlAutoTextReportExecutionTime = 13;
    }

    public class DSSXMLAxesBitMap
    {
        public const int DssXmlAxisNone = 0;
        public const int DssXmlAxisRowsBit = 1;
        public const int DssXmlAxisColumnsBit = 2;
        public const int DssXmlAxisPagesBit = 4;
        public const int DssXmlAxisChaptersBit = 8;
    }

    public class DSSXMLAxisName
    {
        public const int DssXmlAxisNameRows = 1;
        public const int DssXmlAxisNameColumns = 2;
        public const int DssXmlAxisNamePages = 3;
        public const int DssXmlAxisNameChapters = 4;
    }

    public class DSSXMLBaseFormType
    {
        public const int DssXmlBaseFormReserved = 0;
        public const int DssXmlBaseFormHidden = -1;
        public const int DssXmlBaseFormDateTime = 1;
        public const int DssXmlBaseFormNumber = 2;
        public const int DssXmlBaseFormText = 3;
        public const int DssXmlBaseFormPicture = 4;
        public const int DssXmlBaseFormUrl = 5;
        public const int DssXmlBaseFormEmail = 6;
        public const int DssXmlBaseFormHTMLTag = 7;
        public const int DssXmlBaseFormDate = 8;
        public const int DssXmlBaseFormTime = 9;
        public const int DssXmlBaseFormSymbol = 10;
        public const int DssXmlBaseFormBigDecimal = 11;
        public const int DssXmlBaseFormPhoneNumber = 12;
    }

    public class DSSXMLBinaryContextDetailFlags
    {
        public const int DssXMLBinaryContextDetailDefault = 0;
        public const int DssXMLBinaryContextDetailCurrentLayout = 1;
        public const int DssXMLBinaryContextDetailAnnotation = 2;
        public const int DssXMLBinaryContextDetailVisFramework = 4;
        public const int DssXMLBinaryContextDetailCurrentGroupBy = 8;
        public const int DssXMLBinaryContextDetailPartial = 16;
        public const int DssXMLBinaryContextDetailSlice = 32;
    }

    public class DSSXMLBinaryContextFlags
    {
        public const int DssXMLBinaryContextFlexDefn = 1;
        public const int DssXMLBinaryContextFlexData = 2;
    }

    public class DSSXMLBoxPlotMinorTypes
    {
        public const int DssXmlBoxPlotMinorType = 1;
    }

    public class DSSXMLBubbleMinorTypes
    {
        public const int DssXmlBubbleMinorTypeBubble = 1;
        public const int DssXmlBubbleMinorTypeBubbleDualAxis = 2;
        public const int DssXmlBubbleMinorTypeBubbleLabels = 4;
        public const int DssXmlBubbleMinorTypeBubbleDualAxisLabels = 8;
    }

    public class DSSXMLCacheAdminAction
    {
        public const int DssXmlInvalidateCache = 1;
        public const int DssXmlDeleteCache = 2;
        public const int DssXmlLoadCache = 3;
        public const int DssXmlUnloadCache = 4;
    }

    public class DSSXMLCacheExpCommand
    {
        public const int DssXmlCacheExpCommandInvalidate = 0;
        public const int DssXmlCacheExpCommandDelete = 1;
        public const int DssXmlCacheExpCommandElementPurge = 2;
    }

    public class DSSXMLCacheExpOptions
    {
        public const int DssXmlCacheExpReserved = 0;
        public const int DssXmlCacheExpOptionsInboxOnly = 1;
        public const int DssXmlCacheExpOptionsReportCacheOnly = 2;
        public const int DssXmlCacheExpOptionsBoth = 3;
    }

    public class DSSXMLCacheInfo
    {
        public const int DssXmlCacheInfoId = 1;
        public const int DssXmlCacheInfoProjectGuid = 2;
        public const int DssXmlCacheInfoReportId = 3;
        public const int DssXmlCacheInfoWHTable = 4;
        public const int DssXmlCacheInfoReportName = 5;
        public const int DssXmlCacheInfoStatus = 6;
        public const int DssXmlCacheInfoSize = 7;
        public const int DssXmlCacheInfoType = 8;
        public const int DssXmlCacheInfoHitCount = 9;
        public const int DssXmlCacheInfoLastUpdateTime = 10;
        public const int DssXmlCacheInfoLastHitTime = 11;
        public const int DssXmlCacheInfoExpireTime = 12;
        public const int DssXmlCacheInfoCreateTime = 13;
        public const int DssXmlCacheInfoDataLocale = 14;
        public const int DssXmlCacheInfoUserId = 15;
        public const int DssXmlCacheInfoDBLoginId = 16;
        public const int DssXmlCacheInfoDBConnectionId = 17;
        public const int DssXmlCacheInfoSecurityFilterId = 18;
        public const int DssXmlCacheInfoWHLoginName = 19;
        public const int DssXmlCacheInfoCreator = 20;
    }

    public class DSSXMLCacheSetting
    {
        public const int DssXmlCacheSettingReserved = 0;
        public const int DssXmlCacheSettingObjectMaxCacheCount = 1;
        public const int DssXmlCacheSettingObjectMaxMemoryConsumption = 2;
        public const int DssXmlCacheSettingObjectCacheTableBucketCount = 3;
        public const int DssXmlCacheSettingObjectCacheTableBucketPerLock = 4;
        public const int DssXmlCacheSettingObjectFileCachePath = 5;
        public const int DssXmlCacheSettingObjectSwapPolicy = 6;
        public const int DssXmlCacheSettingObjectCacheLifeTime = 7;
        public const int DssXmlCacheSettingElementMaxCacheCount = 8;
        public const int DssXmlCacheSettingElementMaxMemoryConsumption = 9;
        public const int DssXmlCacheSettingElementCacheTableBucketCount = 10;
        public const int DssXmlCacheSettingElementCacheTableBucketPerLock = 11;
        public const int DssXmlCacheSettingElementFileCachePath = 12;
        public const int DssXmlCacheSettingElementSwapPolicy = 13;
        public const int DssXmlCacheSettingElementCacheLifeTime = 14;
        public const int DssXmlCacheSettingReportMaxCacheCount = 15;
        public const int DssXmlCacheSettingReportMaxMemoryConsumption = 16;
        public const int DssXmlCacheSettingReportCacheTableBucketCount = 17;
        public const int DssXmlCacheSettingReportCacheTableBucketPerLock = 18;
        public const int DssXmlCacheSettingReportFileCachePath = 19;
        public const int DssXmlCacheSettingReportSwapPolicy = 20;
        public const int DssXmlCacheSettingReportCacheLifeTime = 21;
        public const int DssXmlCacheSettingReportDisableCaching = 22;
        public const int DssXmlCacheSettingReportCacheDBMatchingFlag = 23;
        public const int DssXmlCacheSettingLinkCacheMaxMemConsumption = 24;
        public const int DssXmlCacheSettingLinkCacheTableBucketCount = 25;
        public const int DssXmlCacheSettingLinkCacheTableBucketPerLock = 26;
        public const int DssXmlCacheSettingLinkCacheSwapPolicy = 27;
        public const int DssXmlCacheSettingReportCacheLoadOnStart = 28;
        public const int DssXmlCacheSettingMaxElementRowCount = 29;
        public const int DssXmlCacheSettingElementCacheUsesSecurityFilters = 30;
        public const int DssXmlCacheSettingElementCacheUseConnectionMap = 31;
        public const int DssXmlCacheSettingElementCacheUsePassThroughLogin = 32;
        public const int DssXmlCacheSettingLastOne = 33;
    }

    public class DSSXMLCacheSwapPolicy
    {
        public const int DssXmlCacheSwapPolicyLRU = 0;
        public const int DssXmlCacheSwapPolicyLHC = 1;
    }

    public class DSSXMLCacheType
    {
        public const int DssXmlReportCache = 1;
        public const int DssXmlDocumentCache = 2;
        public const int DssXmlCubeCache = 3;
    }

    public class DSSXMLChangeJournalCommandType
    {
        public const int DssXmlChangeJournalCommandReserved = 0;
        public const int DssXmlChangeJournalCommandQuery = 1;
        public const int DssXmlChangeJournalCommandEnable = 2;
        public const int DssXmlChangeJournalCommandDisable = 3;
        public const int DssXmlChangeJournalCommandPurge = 4;
    }

    public class DSSXMLChannelType
    {
        public const int DssXmlChannelReserved = 0;
        public const int DssXmlChannelTCPIP = 1;
        public const int DssXmlChannelConsole = 2;
    }

    public class DSSXMLCharacterEncoding
    {
        public const int MultiByte = 0;
        public const int UTF8 = 1;
    }

    public class DSSXMLClientTypes
    {
        public const int DssXmlClientTypeAgent = 1;
        public const int DssXmlClientTypeSysAdmin = 2;
        public const int DssXmlApplicationWebAdmin = 3;
        public const int DssXmlApplicationServer = 4;
        public const int DssXmlApplicationProjectUpgrade = 5;
        public const int DssXmlClientTypeWeb = 6;
        public const int DssXmlClientTypeScheduler = 7;
        public const int DssXmlApplicationCustomApp = 8;
        public const int DssXmlApplicationNarrowcastServer = 9;
        public const int DssXmlApplicationObjectManager = 10;
        public const int DssXmlApplicationODBOProvider = 11;
        public const int DssXmlApplicationODBOCubeDesigner = 12;
        public const int DssXmlApplicationCommandManager = 13;
        public const int DssXmlApplicationEnterpriseManager = 14;
        public const int DssXmlApplicationCommandLineInterface = 15;
        public const int DssXmlApplicationProjectBuilder = 16;
        public const int DssXmlApplicationConfigurationWizard = 17;
        public const int DssXmlApplicationMDScan = 18;
        public const int DssXmlApplicationCacheUtility = 19;
        public const int DssXmlApplicationFireEvent = 20;
        public const int DssXmlApplicationTypeJavaDesktop = 21;
        public const int DssXmlApplicationWebServices = 22;
        public const int DSSXmlApplicationOfficeWS = 23;
        public const int DssXmlApplicationTools = 24;
        public const int DssXmlApplicationPortal = 25;
        public const int DssXmlApplicationTester = 26;
        public const int DssXmlApplicationMDUpdate = 27;
        public const int DssXmlApplicationCOMBrowser = 28;
        public const int DssXmlApplicationMobile = 29;
        public const int DssXmlApplicationBulkTranslationTool = 30;
        public const int DssXmlApplicationHealthCenter = 31;
        public const int DssXmlApplicationCubeAdvisor = 32;
    }

    public class DSSXMLClusterMemberStatus
    {
        public const int DssXmlClusterMemberStatusExecuting = 1;
        public const int DssXmlClusterMemberStatusPause = 2;
        public const int DssXmlClusterMemberStatusStopped = 3;
    }

    public class DSSXMLColumnClass
    {
        public const int DssXmlColumnClassHeader = 1;
        public const int DssXmlColumnClassExtra = 2;
        public const int DssXmlColumnClassGrid = 3;
    }

    public class DSSXMLCombinationMinorTypes
    {
        public const int DssXmlBarAreaMinorType = 1;
        public const int DssXmlBarLineMinorType = 2;
        public const int DssXmlAreaLineMinorType = 4;
        public const int DssXmlDualAxisBarAreaMinorType = 8;
        public const int DssXmlDualAxisBarLineMinorType = 16;
        public const int DssXmlDualAxisAreaLineMinorType = 32;
    }

    public class DSSXMLConflictDomain
    {
        public const int DssXmlConflictReserved = 0;
        public const int DssXmlConflictExplicitObject = 1;
        public const int DssXmlConflictFolder = 2;
        public const int DssXmlConflictObjectType = 3;
        public const int DssXmlConflictApplication = 4;
        public const int DssXmlConflictSchema = 5;
        public const int DssXmlConflictConfiguration = 6;
        public const int DssXmlConflictAll = 7;
    }

    public class DSSXMLConflictResolution
    {
        public const int DssXmlConflictNone = 0;
        public const int DssXmlConflictExisting = 1;
        public const int DssXmlConflictReplace = 2;
        public const int DssXmlConflictEarlier = 3;
        public const int DssXmlConflictLater = 4;
        public const int DssXmlConflictKeepBoth = 5;
        public const int DssXmlConflictMask = 15;
        public const int DssXmlConflictForce = 16;
        public const int DssXmlConflictPreserveHidden = 32;
        public const int DssXmlConflictForceKeepBoth = 21;
        public const int DssXmlConflictForceReplace = 18;
    }

    public class DSSXMLConnectionMode
    {
        public const int DssXmlConnectionModeDirectAccess = 2;
        public const int DssXmlConnectionModeServerAccess = 3;
        public const int DssXmlConnectionModeWebAccess = 4;
        public const int DssXmlConnectionModeCastorServer = 5;
    }

    public class DSSXMLConnectionState
    {
        public const int DssXmlConnectionStateFree = 1;
        public const int DssXmlConnectionStateBusy = 2;
    }

    public class DSSXMLConnParam
    {
        public const int DssXmlConnParamPoolSizeLimit = 1024;
        public const int DssXmlConnParamInitialPoolSize = 4;
        public const int DssXmlConnParamMaximumPoolSize = 100;
        public const int DssXmlConnParamProtocol = 1;
        public const int DssXmlConnParamPort = 0;
    }

    public class DSSXMLControlAttributeFormDisplayOption
    {
        public const int DssXmlControlAttributeFormReserved = 0;
        public const int DssXmlControlAttributeDefaultForms = 1;
        public const int DssXmlControlAttributeBrowseForms = 2;
        public const int DssXmlControlAttributeDisplayForms = 3;
        public const int DssXmlControlAttributeCustomForms = 4;
    }

    public class DSSXMLCreateFlags
    {
        public const int DssXMLCreateProject = 1;
        public const int DssXMLCreateConfiguration = 2;
        public const int DssXMLCreateServerInstance = 3;
        public const int DssXMLCreateEntityMask = 4095;
        public const int DssXMLCreateFromString = 4096;
        public const int DssXMLCreateChangeJournal = 16384;
        public const int DssXMLCreateEnableQuickSearch = 32768;
    }

    public class DSSXMLCubeInfo
    {
        public const int DssXmlCubeInfoCubeId = 1;
        public const int DssXmlCubeInfoCubeDefId = 2;
        public const int DssXmlCubeInfoProjectId = 3;
        public const int DssXmlCubeInfoProjectName = 4;
        public const int DssXmlCubeInfoCubeName = 5;
        public const int DssXmlCubeInfoCreator = 6;
        public const int DssXmlCubeInfoStatus = 7;
        public const int DssXmlCubeInfoSize = 8;
        public const int DssXmlCubeInfoHitCount = 9;
        public const int DssXmlCubeInfoHistoricHitCount = 10;
        public const int DssXmlCubeInfoDataLocale = 11;
        public const int DssXmlCubeInfoLastUpdateTime = 12;
        public const int DssXmlCubeInfoLastHitTime = 13;
        public const int DssXmlCubeInfoLastUpdateJob = 14;
        public const int DssXmlCubeInfoCreateTime = 15;
        public const int DssXmlCubeInfoFileName = 16;
        public const int DssXmlCubeInfoOpenViewCount = 17;
        public const int DssXmlCubeInfoCreatorId = 18;
        public const int DssXmlCubeInfoDBLoginId = 19;
        public const int DssXmlCubeInfoDBConnectionId = 20;
        public const int DssXmlCubeInfoWHLoginID = 21;
        public const int DssXmlCubeInfoPromptAnswer = 22;
    }

    public class DSSXMLCurrentElementAction
    {
        public const int DssXmlCurrentElementActionReserved = 0;
        public const int DssXmlCurrentElementActionFirst = 1;
        public const int DssXmlCurrentElementActionLast = 2;
        public const int DssXmlCurrentElementActionPrevious = 3;
        public const int DssXmlCurrentElementActionNext = 4;
    }

    public class DSSXMLCurrentElementStatus
    {
        public const int DssXmlCurrentElementReserved = 0;
        public const int DssXmlCurrentElementUnset = 1;
        public const int DssXmlCurrentElementChosen = 2;
        public const int DssXmlCurrentElementDesired = 3;
    }

    public class DSSXMLDatabaseType
    {
        public const int DssXmlDatabaseTypeReserved = 0;
        public const int DssXmlDBAccess = 100;
        public const int DssXmlDBDB2 = 700;
        public const int DssxmlDBGeneric = 1100;
        public const int DssXmlDBInformix = 400;
        public const int DssXmlDBOracle = 200;
        public const int DssXmlDBRedBrick = 600;
        public const int DssXmlDBSQLServer = 300;
        public const int DssXmlDBSybase = 500;
        public const int DssXmlDBTandem = 800;
        public const int DssXmlDBTeradata = 900;
        public const int DssXmlDBSAP = 1200;
        public const int DssXmlDBNetezza = 1300;
        public const int DssXmlDBExcel = 1400;
        public const int DssXmlDBMicrosoftAS = 1500;
        public const int DssXmlDBEssBase = 1600;
        public const int DssXmlDBMySQL = 1700;
        public const int DssXmlDBPostgreSQL = 1800;
        public const int DssXmlDBNeoview = 1900;
        public const int DssXmlDBUnknown = 1000;
        public const int DssXmlDBMetamatrix = 2000;
        public const int DssXmlDBDATAllegro = 2100;
        public const int DssXmlDBComposite = 2200;
        public const int DssXmlDBAster = 2300;
        public const int DssXmlDBVertica = 2400;
        public const int DssXmlDBOpenAccess = 2500;
        public const int DssXmlDBSybaseSQLAny = 2600;
    }

    public class DSSXMLDatabaseVersion
    {
        public const int DssXmlDatabaseVersionDefault = -1;
        public const int DssXmlDatabaseVersionReserved = 0;
        public const int DssXmlDBTandemMPD45 = 1;
        public const int DssXmlDBTandemMX1 = 2;
        public const int DssXmlDBTandemMPD42 = 3;
        public const int DssXmlDBTeradataV2R1 = 4;
        public const int DssXmlDBTeradataV2R20002 = 5;
        public const int DssXmlDBTeradataV2R21 = 6;
        public const int DssXmlDBTeradatav2R3 = 7;
        public const int DssXmlDBOracle733 = 8;
        public const int DssXmlDBOracle8003 = 9;
        public const int DssXmlDBSQLServer65 = 10;
        public const int DssXmlDBSQLServer70 = 11;
        public const int DssXmlDBAccess20 = 12;
        public const int DssXmlDBAccess70 = 13;
        public const int DssXmlDBSybaseAdaptive115 = 14;
        public const int DssXmlDBSybaseSQL112 = 15;
        public const int DssXmlDBSybaseIQ112 = 16;
        public const int DssXmlDBIBMDB2OS39041 = 17;
        public const int DssXmlDBIBMDB2OS39050 = 18;
        public const int DssXmlDBIBMUDBSMP50 = 19;
        public const int DssXmlDBIBMUDBEEE50 = 20;
        public const int DssXmlDBIBMDB2PE12 = 21;
        public const int DssXmlDBIBMDB2CS212 = 22;
        public const int DssXmlDBIBMDB2400V3R7 = 23;
        public const int DssXmlDBIBMDB2400V4R1 = 24;
        public const int DssXmlDBIBMDB2400V4R2 = 25;
        public const int DssXmlDBInformixODS724UC1 = 26;
        public const int DssXmlDBInformixXPS82 = 27;
        public const int DssXmlDBAdabaseD6112 = 28;
        public const int DssXmlDBRedBrick5007 = 29;
        public const int DssXmlDBRedBrick5012 = 30;
        public const int DssXmlDBRedBrick5105 = 31;
        public const int DssXmlDBTeradataNTV2R2 = 32;
        public const int DssXmlDBTeradataNTV2R3 = 33;
        public const int DssXmlDBOracle8i = 34;
        public const int DssXmlDBSybaseIQ12 = 35;
        public const int DssXmlDBIBMUDB52 = 36;
        public const int DssXmlDBIBMDB2400V4R3 = 37;
        public const int DssXmlDBInformixODS731 = 38;
        public const int DssXmlDBInformixUDO92 = 39;
        public const int DssXmlDBInformixXPS83 = 40;
        public const int DssXmlDBIBMUDB61 = 41;
        public const int DssXmlDBOracle805 = 42;
        public const int DssXmlDBOracle8iR2 = 43;
        public const int DssXmlDBIBMUDB7 = 44;
        public const int DssXmlDBIBMDB2OS39062 = 45;
        public const int DssXmlDBIBMDB2OS3907 = 46;
        public const int DssXmlDBIBMDB2400V4R4 = 47;
        public const int DssXmlDBIBMDB2400V4R5 = 48;
        public const int DssXmlDBRedBrick6 = 49;
        public const int DssXmlDBTeradataV2R4 = 50;
        public const int DssXmlDBSybaseAdaptive12 = 51;
        public const int DssXmlDBSQLServer2000 = 52;
        public const int DssXmlDBAccess2000 = 53;
        public const int DssXmlDBInformix10 = 54;
        public const int DssXmlDBOracle8iR3 = 55;
        public const int DssXmlDBOracle9i = 56;
        public const int DssXmlRedBrick61 = 57;
        public const int DssXmlDBIBMUDB8 = 58;
        public const int DssXmlDBOracle8iR2SE = 59;
        public const int DssXmlDBTeradataV2R41 = 61;
        public const int DssXmlDBAccess2002 = 62;
        public const int DssXmlDBIBMDB2400V5R1 = 63;
        public const int DssXmlDBSybaseAdaptive125 = 64;
        public const int DssXmlDBRedBrick62 = 65;
        public const int DssXmlDBIBMDB2400V5R2 = 66;
        public const int DssXmlDBTeradataV2R5 = 67;
        public const int DssXmlMDXSAPBW30 = 68;
        public const int DssXmlDBOracle10g = 69;
        public const int DssXmlDBTeradataV2R51 = 70;
        public const int DssXmlDBInformixIDS93 = 71;
        public const int DssXmlDBInformixIDS94 = 72;
        public const int DssXmlDBRedBrick63 = 73;
        public const int DssXmlDBIBMDB2OS3908 = 74;
        public const int DssXmlDBNetezza22 = 75;
        public const int DssXmlDBExcel2003 = 76;
        public const int DssXmlDBNetezza25 = 77;
        public const int DssXmlMDXEssbaseHyperion = 78;
        public const int DssXmlMDXMicrosoftAS2005 = 79;
        public const int DssXmlDBMySQL50 = 80;
        public const int DssXmlDBPostgreSQL81 = 81;
        public const int DssXmlDBInformixIDS10 = 82;
        public const int DssXmlDBOracle10gR2 = 83;
        public const int DssXmlDBTeradataV2R6 = 84;
        public const int DssXmlDBTeradataV2R61 = 85;
        public const int DssXmlDBSybaseASE15 = 86;
        public const int DssXmlDBSQLServer2005 = 87;
        public const int DssXmlMDXMicrosoftAS2000 = 88;
        public const int DssXmlDBIBMDB2400V5R4 = 89;
        public const int DssXmlDBNetezza3 = 90;
        public const int DssXmlDBNetezza30 = 90;
        public const int DssXmlDBSybaseIQ127 = 91;
        public const int DssXmlDBIBMUDB91 = 92;
        public const int DssXmlDBTeradataV2R62 = 93;
        public const int DssXmlDBPostgreSQL82 = 94;
        public const int DssXmlDBHPNeoview22 = 95;
        public const int DssXmlDBHPNeoview20 = 95;
        public const int DssXmlDBOracle11g = 96;
        public const int DssXmlDBNetezza4 = 97;
        public const int DssXmlDBNetezza40 = 97;
        public const int DssXmlMDXEssbaseHyperion9 = 98;
        public const int DssXmlDBGreenplum3x = 99;
        public const int DssXmlDBHPNeoview23 = 100;
        public const int DssXmlDBIBMUDB95 = 101;
        public const int DssXmlDBTeradata12 = 102;
        public const int DssXmlDBTeradata120 = 102;
        public const int DssXmlDBIBMUDB91zOS = 103;
        public const int DssXmlDBAccess2007 = 104;
        public const int DssXmlDBIBMDB2400V6R1 = 105;
        public const int DssXmlDBMetamatrix55 = 106;
        public const int DssXmlDBDATAllegro3x = 107;
        public const int DssXmlDBComposite450 = 108;
        public const int DssXmlDBAccess2003 = 109;
        public const int DssXmlDBSQLServer2008 = 110;
        public const int DssXmlDBMySQL51 = 111;
        public const int DssXmlMDXMicrosoftAS2008 = 112;
        public const int DssXmlDBAsternCluster30 = 113;
        public const int DssXmlDBVertica25 = 114;
        public const int DssXmlMDXEssbaseHyperion9x = 115;
        public const int DssXmlDBOpenAccess14 = 116;
        public const int DssXmlDBSybaseSQLAny11 = 117;
        public const int DssXmlDBNetezza46 = 118;
        public const int DssXmlDBTeradata13 = 119;
        public const int DssXmlDBSybaseIQ15 = 120;
        public const int DssXmlDBSybaseIQ151 = 121;
        public const int DssXmlDBHPNeoview24 = 122;
        public const int DssXmlDBIBMUDB97 = 123;
        public const int DssXmlDBNetezza50 = 124;
        public const int DssXmlDBVertica30 = 125;
        public const int DssXmlMDXSAPBW7x = 126;
        public const int DssXmlDBOracle11gR2 = 127;
        public const int DssXmlDBInformixIDS115 = 128;
        public const int DssXmlDBPostgreSQL84 = 129;
        public const int DssXmlDBSQLServer2008NativeClient = 130;
        public const int DssXmlDBParAccel2x = 131;
        public const int DssXmlDBNetezza4xDBlytix10 = 132;
        public const int DssXmlMDXTM1x = 133;
        public const int DssXmlDBIBMDB2400V7R1 = 134;
        public const int DssXmlDBAsternCluster40 = 135;
        public const int DssXmlDBInfobright33 = 136;
        public const int DssXmlDBVertica40 = 137;
        public const int DssXmlDBHive05x = 138;
        public const int DssXmlDBSybaseIQ152 = 139;
        public const int DssXmlDBAsternCluster45 = 140;
        public const int DssXmlDBGreenplum4x = 141;
        public const int DssXmlXQuery1x = 142;
        public const int DssXmlDBHPNeoview25 = 143;
        public const int DssXmlMDXEssbaseHyperion11x = 144;
        public const int DssXmlDBSANDCDBMS61 = 145;
        public const int DSSXmlDBComposite510 = 146;
        public const int DssXmlDBNetezza60 = 147;
        public const int DssXmlDBSQLServer2008R2ParallelDataWH = 148;
        public const int DssXmlDBPostgreSQL90 = 149;
        public const int DssXmlDBTeradata131 = 150;
        public const int DssXmlDBParAccel3x = 151;
        public const int DssXmlDBHive06x = 152;
        public const int DssXmlDBHive07x = 153;
        public const int DssXmlDBSalesforce1 = 154;
        public const int DssXmlDBPostgreSQL91 = 155;
        public const int DssXmlDBHiveThrift1 = 156;
        public const int DssXmlDBIBMUDBV10zOS = 157;
        public const int DssXmlDBVectorWise15 = 158;
        public const int DssXmlDBSQLServer2012 = 159;
        public const int DssXmlDBInfobright40 = 160;
        public const int DssXmlDBSybaseIQ153 = 161;
        public const int DssXmlDBSybaseIQ154 = 162;
        public const int DssXmlDBEnterpriseDB9x = 163;
        public const int DssXmlDBKognitioWX27x = 164;
        public const int DssXmlDBEXASolution3x = 165;
        public const int DssXmlDBEXASolution40 = 166;
        public const int DssXmlDBVertica5051 = 167;
        public const int DssXmlDBParAccel31x = 168;
        public const int DssXmlDBParAccel35x = 169;
        public const int DssXmlDBComposite6x = 170;
        public const int DssXmlDBInformatica91x = 171;
        public const int DssXmlDBIBMUDB101 = 172;
        public const int DssXmlDBAsternCluster46 = 173;
        public const int DssXmlDBVectorWise20x = 174;
        public const int DssXmlMDXMicrosoftAS2005and08BinaryConnector = 175;
        public const int DssXmlMDXMicrosoftAS2012BinaryConnector = 176;
        public const int DssXmlDBTeradata14 = 177;
        public const int DssXmlDBHiveThrift08x = 178;
        public const int DssXmlDBHive08x = 179;
        public const int DssXmlDBVertica60 = 180;
        public const int DssXmlDBSAPHANA1 = 181;
        public const int DssXmlMDXMicrosoftAS2012 = 182;
        public const int DssXmlDBInformixIDS117 = 183;
        public const int DssXmlDBAsternCluster50 = 184;
        public const int DssXmlDBNetezza70 = 185;
        public const int DssXmlDBHive09x = 186;
        public const int DssXmlDBHiveThrift09x = 187;
        public const int DssXmlDBImpala1x = 188;
        public const int DssXmlDBParAccel40x = 189;
        public const int DssXmlDBAmazonRedShif = 190;
        public const int DssXmlDBSQLDBforWindowsAzure = 191;
        public const int DssXmlDBInformatica95 = 192;
        public const int DssXmlOracleEssbase93 = 193;
        public const int DssXmlDBInformixIDS11 = 82;
        public const int DssXmlDBPostgreSQL83 = 94;
    }

    public class DSSXMLDataLineType
    {
        public const int DssXmlDataLineBoth = 0;
        public const int DssXmlDataLineMarker = 1;
        public const int DssXmlDataLineLine = 2;
    }

    public class DSSXMLDataSourceConnectionType
    {
        public const int DssXmlDataSourceConnection2Tier = 1;
        public const int DssXmlDataSourceConnection2TierAbell = 2;
        public const int DssXmlDataSourceConnection3Tier = 3;
    }

    public class DSSXMLDataSourceFlags
    {
        public const int DssXmlDataSourceUseWHPartition = 1;
        public const int DssXmlDataSourceUsePersistentID = 2;
        public const int DssXmlDataSourceMDConnectionMultiProcess = 4;
    }

    public class DSSXMLDataSourceType
    {
        public const int DssXmlDataSourceTypeReserved = 0;
        public const int DssXmlDataSourceTypeConfiguration = 1;
        public const int DssXmlDataSourceTypeServer = 2;
        public const int DssXmlDataSourceTypeProjectMD7 = 3;
        public const int DssXmlDataSourceTypeProjectMD4 = 4;
        public const int DssXmlDataSourceTypeCacheFiles = 5;
    }

    public class DSSXMLDataType
    {
        public const int DssXmlDataTypeUnknown = -1;
        public const int DssXmlDataTypeReserved = 0;
        public const int DssXmlDataTypeInteger = 1;
        public const int DssXmlDataTypeUnsigned = 2;
        public const int DssXmlDataTypeNumeric = 3;
        public const int DssXmlDataTypeDecimal = 4;
        public const int DssXmlDataTypeReal = 5;
        public const int DssXmlDataTypeDouble = 6;
        public const int DssXmlDataTypeFloat = 7;
        public const int DssXmlDataTypeChar = 8;
        public const int DssXmlDataTypeVarChar = 9;
        public const int DssXmlDataTypeLongVarChar = 10;
        public const int DssXmlDataTypeBinary = 11;
        public const int DssXmlDataTypeVarBin = 12;
        public const int DssXmlDataTypeLongVarBin = 13;
        public const int DssXmlDataTypeDate = 14;
        public const int DssXmlDataTypeTime = 15;
        public const int DssXmlDataTypeTimeStamp = 16;
        public const int DssXmlDataTypeShort = 21;
        public const int DssXmlDataTypeLong = 22;
        public const int DssXmlDataTypeMBChar = 23;
        public const int DssXmlDataTypeBool = 24;
        public const int DssXmlDataTypePattern = 25;
        public const int DssXmlDataTypeBigDecimal = 30;
    }

    public class DSSXMLDayOfWeek
    {
        public const int DssXmlDaySunday = 0;
        public const int DssXmlDayMonday = 1;
        public const int DssXmlDayTuesday = 2;
        public const int DssXmlDayWednesday = 3;
        public const int DssXmlDayThursday = 4;
        public const int DssXmlDayFriday = 5;
        public const int DssXmlDaySaturday = 6;
    }

    public class DSSXMLDayOfWeekBitEncoded
    {
        public const int DssXmlDaySunday = 1;
        public const int DssXmlDayMonday = 2;
        public const int DssXmlDayTuesday = 4;
        public const int DssXmlDayWednesday = 8;
        public const int DssXmlDayThursday = 16;
        public const int DssXmlDayFriday = 32;
        public const int DssXmlDaySaturday = 64;
    }

    public class DSSXMLDBConnectionCacheOption
    {
        public const int DssDBConnectionCacheReserved = 0;
        public const int DssDBConnectionCacheNone = 1;
        public const int DssDBConnectionCacheReuse = 2;
        public const int DssDBConnectionCacheSerialReuse = 3;
    }

    public class DSSXMLDBConnectionDriverMode
    {
        public const int DssDBConnectionDriverReserved = 0;
        public const int DssDBConnectionDriverODBC = 1;
        public const int DssDBConnectionDriverNativeDBAPI = 2;
    }

    public class DSSXMLDBConnectionExecutionMode
    {
        public const int DssDBConnectionExecutionReserved = 0;
        public const int DssDBConnectionExecutionAsynchConnection = 1;
        public const int DssDBConnectionExecutionAsynchStatement = 2;
        public const int DssDBConnectionExecutionSynchronous = 3;
    }

    public class DSSXMLDBConnectionInfo
    {
        public const int DssXmlDBConnDBConnectionNo = 0;
        public const int DssXmlDBConnDBConnectionStatus = 1;
        public const int DssXmlDBConnDBInstance = 2;
        public const int DssXmlDBConnDBConnectionID = 3;
        public const int DssXmlDBConnUserName = 4;
        public const int DssXmlDBConnLogin = 5;
        public const int DssXmlDBConnServerName = 6;
        public const int DssXmlDBConnDBConnectionName = 7;
    }

    public class DSSXMLDBConnectionMultiProcessOption
    {
        public const int DssDBConnectionMultiProcessReserved = 0;
        public const int DssDBConnectionMultiProcessMultiThreadMode = 1;
        public const int DssDBConnectionMultiProcessMultiProcessMode = 2;
    }

    public class DSSXMLDBConnectionStatus
    {
        public const int DssXmlDBConnectionBusy = 0;
        public const int DssXmlDBConnectionCached = 1;
    }

    public class DSSXMLDBRoleType
    {
        public const int DssXmlDBRoleReserved = 0;
        public const int DssXmlDBRoleNormal = 1;
        public const int DssXmlDBRoleDataImport = 2;
        public const int DssXmlDBRoleDataImportPrimary = 3;
    }

    public class DSSXMLDecomposable
    {
        public const int DssXmlDecomposableReserved = 0;
        public const int DssXmlDecomposableDefault = 1;
        public const int DssXmlDecomposableFalse = 2;
        public const int DssXmlDecomposableTrue = 3;
        public const int DssXmlDecomposableOneLevel = 4;
    }

    public class DSSXMLDefaultFormats
    {
        public const int DSSXmlDefaultFormatLabel = 1;
        public const int DSSXmlDefaultFormatText = 2;
        public const int DSSXmlDefaultFormatImage = 3;
        public const int DSSXmlDefaultFormatCrosstab = 4;
        public const int DSSXmlDefaultFormatLine = 5;
        public const int DSSXmlDefaultFormatShape = 6;
        public const int DSSXmlDefaultFormatSection = 7;
        public const int DSSXmlDefaultFormatTitle = 8;
        public const int DSSXmlDefaultFormatHTML = 9;
        public const int DSSDefaultFormatHTML = 9;
        public const int DSSXmlDefaultFormatPanelStack = 10;
        public const int DSSXmlDefaultFormatControl = 11;
        public const int DSSXmlDefaultFormatUnselectedItem = 12;
        public const int DSSXmlDefaultFormatSelectedItem = 13;
        public const int DSSXmlDefaultFormatHighlightedItem = 14;
        public const int DSSXmlDefaultFormatGridTitle = 15;
        public const int DSSXmlDefaultFormatPanelStackTitle = 16;
        public const int DSSXmlDefaultFormatPanel = 17;
        public const int DSSXmlDefaultFormatControlTitle = 18;
        public const int DSSXmlDefaultFormatOther = -1;
    }

    public class DSSXMLDerivedElementSaveAsFlags
    {
        public const int DssXmlDerivedElementSaveAsDefault = 0;
        public const int DssXmlDerivedElementSaveAsClearDomain = 1;
        public const int DssXmlDerivedElementSaveAsOverwrite = 2;
    }

    public class DSSXMLDimtyUnitType
    {
        public const int DssXmlDimtyUnitTypeDefault = 1;
        public const int DssXmlDimtyUnitTypeReserved = 0;
        public const int DssXmlDimtyUnitTypeAttribute = -1;
        public const int DssXmlDimtyUnitTypeDimension = -2;
        public const int DssXmlDimtyUnitTypeReportLevel = -3;
        public const int DssXmlDimtyUnitTypeReportBaseLevel = -4;
        public const int DssXmlDimtyUnitTypeRole = -5;
    }

    public class DSSXMLDisplayMode
    {
        public const int DssXmlDisplayModeReserved = 0;
        public const int DssXmlDisplayModeGrid = 1;
        public const int DssXmlDisplayModeGraph = 2;
        public const int DssXmlDisplayModeEngine = 3;
        public const int DssXmlDisplayModeText = 4;
        public const int DssXmlDisplayModeDatamart = 5;
        public const int DssXmlDisplayModeBase = 6;
        public const int DssXmlDisplayModeGridAndGraph = 7;
        public const int DssXmlDisplayModeNonInteractive = 8;
        public const int DssXmlDisplayModeCube = 9;
    }

    public class DSSXMLDocExecutionFlags
    {
        public const int DssXmlDocExecutionFresh = 1;
        public const int DssXmlDocExecutionHTML = 2;
        public const int DssXmlDocExecutionXML = 4;
        public const int DssXmlDocExecutionResolve = 131072;
        public const int DssXmlDocExecutionInboxKeepAsIs = 32;
        public const int DssXmlDocExecutionSaveToInbox = 64;
        public const int DssXmlDocExecuteDefaultAutoprompt = 1024;
        public const int DssXmlDocExecutionCheckWebCache = 16777216;
        public const int DssXmlDocExecutionUseWebCacheOnly = 33554432;
        public const int DssXmlDocExecutionExportPDF = 268435456;
        public const int DssXmlDocExecutionExportExcel = 536870912;
        public const int DssXmlDocExecutionExportCurrent = 4096;
        public const int DssXmlDocExecutionExportAll = 8192;
        public const int DssXmlDocExecutionExportCSV = 1073741824;
        public const int DssXmlDocumentExecuteStatic = 2048;
        public const int DssXmlDocExecutionUseRWDCache = 16384;
        public const int DssXmlDocExecutionUpdateRWDCache = 32768;
        public const int DssXmlDocExecutionNoUpdateDatasetCache = 65536;
        public const int DssXmlDocExecutionReprompt = 4194304;
        public const int DssXmlDocExecutionExportFlash = 67108864;
        public const int DssXmlDocExecutionFlash = 134217728;
        public const int DssXmlDocExecutionOnBackground = 524288;
        public const int DssXmlDocExecuteCheckSQLPrompt = -2147483648;
    }

    public class DSSXMLDocResultFlags
    {
        public const int DssXmlDocResultDefinition = 1;
        public const int DssXmlDocResultData = 2;
        public const int DssXmlDocResultResults = 4;
        public const int DssXmlDocResultID = 8;
        public const int DssXmlDocResultDates = 16;
        public const int DssXmlDocResultBrowser = 32;
        public const int DssXmlDocResultProperties = 64;
        public const int DssXmlDocResultErrors = 128;
        public const int DssXmlDocResultPartialPageByTree = 256;
        public const int DssXmlDocResultDrill = 512;
        public const int DssXmlDocResultFlexDefinition = 1024;
        public const int DssXmlDocResultFlexXTabData = 2048;
        public const int DssXmlDocResultFlexResource = 4096;
        public const int DssXmlDocResultDocumentDetails = 8192;
        public const int DssXmlDocResultFlexVizDefinition = 16384;
        public const int DssXmlDocResultDefinitionCurrentPanel = 32768;
        public const int DssXmlDocResultFullPageByTree = 65536;
        public const int DssXmlDocResultDefinitionLayout = 524288;
        public const int DssXmlDocResultOptimizedPageByTree = 1048576;
        public const int DssXmlDocResultPageByTreeCurrentElementOnly = 2097152;
        public const int DssXmlDocResultTransactionReportDefinition = 8388608;
        public const int DssXmlDocResultPutToInboxRead = 16777216;
        public const int DssXmlDocResultStatusOnlyIfNotReady = 33554432;
        public const int DssXmlDocResultCheckFlashBinary = 67108864;
        public const int DssXmlDocResultInboxMessage = 134217728;
        public const int DssXmlDocResultTransactionReportData = 268435456;
        public const int DssXmlDocResultPreserve = -2147483648;
        public const int DssXmlDocResultPartialCSSRetrieval = 262144;
        public const int DssXmlDocResultAllDataSets = 1073741824;
    }

    public class DSSXMLDocSaveAsFlags
    {
        public const int DssXmlDocSaveAsDefault = 0;
        public const int DssXmlDocSaveAsOverwrite = 1;
        public const int DssXmlDocSaveAsWithAnswersOpen = 2;
        public const int DssXmlDocSaveAsWithAnswersClosed = 4;
        public const int DssXmlDocSaveAsRemoveNonPrimaryNameTranslations = 8;
    }

    public class DSSXMLDrillImportance
    {
        public const int DssXmlDrillImportanceReserved = 0;
        public const int DssXmlDrillImportanceHigh = 1;
        public const int DssXmlDrillImportanceMedium = 2;
        public const int DssXmlDrillImportanceLow = 3;
    }

    public class DSSXMLDrillOption
    {
        public const int DssXmlDrillOptionReserved = 0;
        public const int DssXmlDrillOptionWithin = 1;
        public const int DssXmlDrillOptionOut = 2;
    }

    public class DSSXMLDrillQual
    {
        public const int DssXmlDrillQualOld = 1;
        public const int DssXmlDrillQualNew = 2;
        public const int DssXmlDrillQualBoth = 3;
    }

    public class DSSXMLDrillType
    {
        public const int DssXmlDrillReserved = 0;
        public const int DssXmlDrillToParent = 1;
        public const int DssXmlDrillToChild = 2;
        public const int DssXmlDrillToUnit = 3;
        public const int DssXmlDrillToTemplate = 4;
        public const int DssXmlDrillToForm = 5;
        public const int DssXmlDrillToDetails = 6;
        public const int DssXmlDrillToMetric = 7;
        public const int DssXmlDrillFixTemplate = 8;
        public const int DssXmlDrillRemoveMetric = 9;
        public const int DssXmlDrillRemoveUnit = 10;
        public const int DssXmlDrillToGraphCoordinates = 11;
    }

    public class DSSXMLDrillUnitSource
    {
        public const int DssXmlDrillUnitReserved = 0;
        public const int DssXmlDrillUnitInstance = 1;
        public const int DssXmlDrillUnitLocal = 2;
        public const int DssXmlDrillUnitNone = 3;
        public const int DssXmlDrillUnitRow = 4;
        public const int DssXmlDrillUnitColumn = 5;
        public const int DssXmlDrillUnitPageBy = 6;
    }

    public class DSSXMLDynamicTime
    {
        public const int DssXmlTimeEnd = 0;
        public const int DssXmlTimeBeginning = 1;
        public const int DssXmlTimeNow = 2;
        public const int DssXmlTimeToday = 3;
        public const int DssXmlTimeThisWeek = 4;
        public const int DssXmlTimeThisMonth = 5;
        public const int DssXmlTimeThisYear = 6;
        public const int DssXmlTimeThisSunday = 7;
        public const int DssXmlTimeThisMonday = 8;
        public const int DssXmlTimeThisTuesday = 9;
        public const int DssXmlTimeThisWednesday = 10;
        public const int DssXmlTimeThisThursday = 11;
        public const int DssXmlTimeThisFriday = 12;
        public const int DssXmlTimeThisSaturday = 13;
        public const int DssXmlTimeNotDynamicTime = -100;
        public const int DssXmlTimeInvalidTime = -101;
        public const int DssXmlTimeStaticDate = 32;
        public const int DssXmlTimeDynamicHour = 64;
        public const int DssXmlTimeDynamicMinute = 128;
    }

    public class DSSXMLElementType
    {
        public const int DssXmlElementReserved = 0;
        public const int DssXmlElementConcrete = 1;
        public const int DssXmlElementMetric = 2;
        public const int DssXmlElementSubtotal = 3;
        public const int DssXmlElementFirst = 4;
        public const int DssXmlElementLast = 5;
        public const int DssXmlElementDimension = 6;
        public const int DssXmlElementConsolidation = 7;
        public const int DssXmlElementFilter = 8;
        public const int DssXmlElementCustomGroup = 9;
        public const int DssXmlElementJoint = 10;
        public const int DssXmlElementBand = 11;
        public const int DssXmlElementResidue = 12;
        public const int DssXmlElementSubexpression = 13;
        public const int DssXmlElementAll = 14;
        public const int DssXmlElementNode = 15;
        public const int DssXmlElementNULL = 16;
    }

    public class DSSXMLERType
    {
        public const int DssXmlERType11 = 1;
        public const int DssXmlERType1M = 2;
        public const int DssXmlERTypeM1 = 3;
        public const int DssXmlERTypeMM = 4;
        public const int DssXmlERTypeParallel = 5;
        public const int DssXmlERTypePerpendicular = 6;
    }

    public class DSSXMLExecutionFlags
    {
        public const int DssXmlExecutionFresh = 1;
        public const int DssXmlExecutionDefaultPrompt = 2;
        public const int DssXmlExecutionUseCache = 128;
        public const int DssXmlExecutionUpdateCache = 256;
        public const int DssXmlExecutionInboxKeepAsIs = 1024;
        public const int DssXmlExecutionSaveToInbox = 2048;
        public const int DssXmlExecutionReprompt = 4096;
        public const int DssXmlExecutionCheckSQLPromp = 32768;
        public const int DssXmlExecutionResolve = 65536;
        public const int DssXmlExecutionGenerateSQL = 131072;
        public const int DssXmlExecutionWebQueryBuilderOrFFSQL = 134217728;
        public const int DssXmlExecutionCheckWebCache = 16777216;
        public const int DssXmlExecutionUseWebCacheOnly = 33554432;
        public const int DssXmlExecutionGenerateDatamart = 67108864;
        public const int DssXmlExecutionSubsetting = -2147483648;
        public const int DssXmlExecutionReBuildPreviewOnly = 268435456;
        public const int DssXmlExecutionExport = 262144;
        public const int DssXmlExecutionDefault = 384;
        public const int DssXmlExecutionNoAction = 524288;
        public const int DssXmlExecutionDrillByManipulation = 134217728;
    }

    public class DSSXMLExportFormat
    {
        public const int DssXmlExportExcel = 1;
        public const int DssXmlExportPdf = 2;
        public const int DssXmlExportCSV = 3;
        public const int DssXmlExportHTML = 4;
        public const int DssXmlExportPlainText = 5;
        public const int DssXmlExportXml = 6;
        public const int DssXmlExportFlash = 7;
        public const int DssXmlExportView = 8;
        public const int DssXmlExportInteractive = 9;
        public const int DssXmlExportEditable = 10;
        public const int DssXmlExportExportFlash = 11;
    }

    public class DSSXMLExpressionType
    {
        public const int DssXmlExpressionReserved = 0;
        public const int DssXmlExpressionGeneric = 1;
        public const int DssXmlFilterSingleBaseFormQual = 2;
        public const int DssXmlFilterMultiBaseFormQual = 3;
        public const int DssXmlFilterJointFormQual = 4;
        public const int DssXmlFilterListQual = 5;
        public const int DssXmlFilterListFormQual = 6;
        public const int DssXmlFilterJointListQual = 7;
        public const int DssXmlFilterJointListFormQual = 8;
        public const int DssXmlFilterSingleBaseFormExpression = 9;
        public const int DssXmlFilterSingleMetricQual = 10;
        public const int DssXmlFilterMultiMetricQual = 11;
        public const int DssXmlFilterMetricExpression = 12;
        public const int DssXmlFilterEmbedQual = 13;
        public const int DssXmlFilterBranchQual = 14;
        public const int DssXmlFilterRelationshipQual = 15;
        public const int DssXmlFilterAllAttributeQual = 16;
        public const int DssXmlFilterAttributeIDQual = 17;
        public const int DssXmlFilterAttributeDESCQual = 18;
        public const int DssXmlExpressionAggMetric = 19;
        public const int DssXmlExpressionBanding = 20;
        public const int DssXmlFilterReportQual = 21;
        public const int DssXmlExpressionMDXSAPVariable = 22;
        public const int DssXmlExpressionSQLQueryQual = 23;
        public const int DssXmlExpressionCanceledPrompt = 24;
        public const int DssXmlExpressionElementList = 25;
        public const int DssXmlExpressionElementSingle = 26;
    }

    public class DSSXMLExtendedType
    {
        public const int DssXmlExtendedTypeReserved = 0;
        public const int DssXmlExtendedTypeRelational = 1;
        public const int DssXmlExtendedTypeMDX = 2;
        public const int DssXmlExtendedTypeCustomSQLFreeForm = 3;
        public const int DssXmlExtendedTypeCustomSQLWizard = 4;
        public const int DssXmlExtendedTypeDataImport = 256;
        public const int DssXmlExtendedTypeDataImportFileExcel = 272;
        public const int DssXmlExtendedTypeDataImportFileText = 288;
        public const int DssXmlExtendedTypeDataImportCustomSQL = 304;
        public const int DssXmlExtendedTypeDataImportTable = 320;
        public const int DssXmlExtendedTypeDataImportCustomSQLWizard = 352;
        public const int DssXmlExtendedTypeDataImportOAuth = 336;
        public const int DssXmlExtendedTypeDataImportOAuthSFDC = 337;
        public const int DssXmlExtendedTypeDataImportOAuthGDrive = 338;
        public const int DssXmlExtendedTypeDataImportOAuthDropbox = 339;
    }

    public class DSSXMLExternalSourceFlags
    {
        public const int DssXmlExternalSourceNone = 0;
        public const int DssXmlExternalSourceForBrowsing = 1;
        public const int DssXmlExternalSourceSearchElementsEnabled = 2;
    }

    public class DSSXMLExtnType
    {
        public const int DssXmlExtnTypeXProduct = 1;
        public const int DssXmlExtnTypeBreak = 2;
        public const int DssXmlExtnTypeFact = 3;
        public const int DssXmlExtnTypeAttribute = 4;
        public const int DssXmlExtnTypeTable = 5;
        public const int DssXmlExtnTypeDegradYes = 6;
        public const int DssXmlExtnTypeDegradNo = 7;
    }

    public class DSSXMLFieldGroupDataLevel
    {
        public const int DssXmlFieldGroupDataLevelDefault = 1;
        public const int DssXmlFieldGroupDataLevelSimple = 2;
        public const int DssXmlFieldGroupDataLevelDefaultReport = 3;
        public const int DssXmlFieldGroupDataLevelGroupBy = 4;
    }

    public class DSSXMLFiltering
    {
        public const int DssXmlFilteringReserved = 0;
        public const int DssXmlFilteringApply = 1;
        public const int DssXmlFilteringAbsolute = 2;
        public const int DssXmlFilteringIgnore = 3;
        public const int DssXmlFilteringNone = 4;
    }

    public class DSSXMLFilterType
    {
        public const int DssXmlFilterReserved = 0;
        public const int DssXmlFilterNormal = 1;
        public const int DssXmlCustomGroup = 2;
        public const int DssXmlFilterPartition = 3;
    }

    public class DSSXMLFolderNames
    {
        public const int DssXmlFolderNamePublicObjects = 1;
        public const int DssXmlFolderNamePublicConsolidations = 2;
        public const int DssXmlFolderNamePublicCustomGroups = 3;
        public const int DssXmlFolderNamePublicFilters = 4;
        public const int DssXmlFolderNamePublicMetrics = 5;
        public const int DssXmlFolderNamePublicPrompts = 6;
        public const int DssXmlFolderNamePublicReports = 7;
        public const int DssXmlFolderNamePublicSearches = 8;
        public const int DssXmlFolderNamePublicTemplates = 9;
        public const int DssXmlFolderNameTemplateObjects = 10;
        public const int DssXmlFolderNameTemplateConsolidations = 11;
        public const int DssXmlFolderNameTemplateCustomGroups = 12;
        public const int DssXmlFolderNameTemplateFilters = 13;
        public const int DssXmlFolderNameTemplateMetrics = 14;
        public const int DssXmlFolderNameTemplatePrompts = 15;
        public const int DssXmlFolderNameTemplateReports = 16;
        public const int DssXmlFolderNameTemplateSearches = 17;
        public const int DssXmlFolderNameTemplateTemplates = 18;
        public const int DssXmlFolderNameProfileObjects = 19;
        public const int DssXmlFolderNameProfileReports = 20;
        public const int DssXmlFolderNameProfileAnswers = 21;
        public const int DssXmlFolderNameProfileFavorites = 22;
        public const int DssXmlFolderNameProfileOther = 23;
        public const int DssXmlFolderNameSchemaObjects = 24;
        public const int DssXmlFolderNameSchemaAttributeForms = 25;
        public const int DssXmlFolderNameSchemaAttributes = 26;
        public const int DssXmlFolderNameSchemaColumns = 27;
        public const int DssXmlFolderNameSchemaDataExplorer = 28;
        public const int DssXmlFolderNameSchemaFacts = 29;
        public const int DssXmlFolderNameSchemaFunctions = 30;
        public const int DssXmlFolderNameSchemaHierarchies = 31;
        public const int DssXmlFolderNameSchemaPartitionFilters = 32;
        public const int DssXmlFolderNameSchemaPartitionMappings = 33;
        public const int DssXmlFolderNameSchemaSubtotals = 34;
        public const int DssXmlFolderNameSchemaTables = 35;
        public const int DssXmlFolderNameSchemaWarehouseTables = 36;
        public const int DssXmlFolderNameSchemaTransformationAttributes = 37;
        public const int DssXmlFolderNameSchemaTransformations = 38;
        public const int DssXmlFolderNameRoot = 39;
        public const int DssXmlFolderNameSchemaFunctionsNested = 40;
        public const int DssXmlFolderNameSchemaBasicFunctions = 41;
        public const int DssXmlFolderNameSchemaDateAndTimeFunctions = 42;
        public const int DssXmlFolderNameSchemaInternalFunctions = 43;
        public const int DssXmlFolderNameSchemaNullZeroFunctions = 44;
        public const int DssXmlFolderNameSchemaOlapFunctions = 45;
        public const int DssXmlFolderNameSchemaRankAndNTileFunctions = 46;
        public const int DssXmlFolderNameSchemaStringFunctions = 47;
        public const int DssXmlFolderNameSchemaOperators = 48;
        public const int DssXmlFolderNameSchemaArithmeticOperators = 49;
        public const int DssXmlFolderNameSchemaComparisonOperators = 50;
        public const int DssXmlFolderNameSchemaComparisonForRankOperators = 51;
        public const int DssXmlFolderNameSchemaLogicalOperators = 52;
        public const int DssXmlFolderNameSchemaPlugInPackages = 53;
        public const int DssXmlFolderNameSchemaFinancialFunctions = 54;
        public const int DssXmlFolderNameSchemaMathFunctions = 55;
        public const int DssXmlFolderNameSchemaStatisticalFunctions = 56;
        public const int DssXmlFolderNameAutoStyles = 57;
        public const int DssXmlFolderNameConfigureMonitors = 58;
        public const int DssXmlFolderNameConfigureServerDefs = 59;
        public const int DssXmlFolderNameTemplateDocuments = 60;
        public const int DssXmlFolderNameSystemObjects = 61;
        public const int DssXmlFolderNameSystemLinks = 62;
        public const int DssXmlFolderNameSystemPropertySets = 63;
        public const int DssXmlFolderNameSystemParserFolder = 64;
        public const int DssXmlFolderNameSystemSchemaFolder = 65;
        public const int DssXmlFolderNameSystemWarehouseCatalog = 66;
        public const int DssXmlFolderNameSystemSystemHierarchy = 67;
        public const int DssXmlFolderNameSystemDrillMap = 68;
        public const int DssXmlFolderNameSystemMDSecurityFilters = 69;
        public const int DssXmlFolderNameSystemDummyPartitionTables = 70;
        public const int DssXmlFolderNameSystemSystemPrompts = 71;
        public const int DssXmlFolderNameEvents = 72;
        public const int DssXmlFolderNameConfigureDBRoles = 73;
        public const int DssXmlFolderNameLocales = 74;
        public const int DssXmlFolderNamePropertySets = 75;
        public const int DssXmlFolderNameDBMS = 76;
        public const int DssXmlFolderNameProjects = 77;
        public const int DssXmlFolderNameUsers = 78;
        public const int DssXmlFolderNameUserGroups = 79;
        public const int DssXmlFolderNameSecurityRoles = 80;
        public const int DssXmlFolderNameDBConnections = 81;
        public const int DssXmlFolderNameDBLogins = 82;
        public const int DssXmlFolderNameLinks = 83;
        public const int DssXmlFolderNameScheduleObjects = 84;
        public const int DssXmlFolderNameScheduleTriggers = 85;
        public const int DssXmlFolderNameTableSources = 86;
        public const int DssXmlFolderNameVersionUpdateHistory = 87;
        public const int DssXmlFolderNameDevices = 88;
        public const int DssXmlFolderNameTransmitters = 89;
        public const int DssXmlFolderNameTemplateDashboards = 90;
        public const int DssXmlFolderNameSystemDimension = 91;
        public const int DssXmlFolderNameMaximum = 92;
    }

    public class DSSXMLFolderType
    {
        public const int DssXmlFolderReserved = 0;
        public const int DssXmlFolderUser = 1;
        public const int DssXmlFolderSystem = 2;
        public const int DssXmlFolderSearch = 4;
        public const int DssXmlFolderImage = 5;
        public const int DssXmlFolderContainer = 7;
        public const int DssXmlFolderEmbedded = 8;
    }

    public class DSSXMLFontAlignTypes
    {
        public const int DssXmlFontAlignLeft = 1;
        public const int DssXmlFontAlignCenter = 2;
        public const int DssXmlFontAlignRight = 4;
        public const int DssXmlFontAlignJustify = 8;
    }

    public class DSSXMLFontAntiAlias
    {
        public const int DssXmlFontAntiAliasNever = 0;
        public const int DssXmlFontAntiAliasAlways = 1;
        public const int DssXmlFontAntiAliasDefault = 2;
    }

    public class DSSXMLFontStyles
    {
        public const int DssXmlFontStyleBold = 1;
        public const int DssXmlFontStyleItalic = 2;
        public const int DssXmlFontStyleUnderline = 4;
        public const int DssXmlFontStyleStrikeout = 8;
    }

    public class DSSXMLFunction
    {
        public const int DssXmlFunctionCustom = -1;
        public const int DssXmlFunctionReserved = 0;
        public const int DssXmlFunctionPlus = 1;
        public const int DssXmlFunctionMinus = 2;
        public const int DssXmlFunctionTimes = 3;
        public const int DssXmlFunctionDivide = 4;
        public const int DssXmlFunctionUnaryMinus = 5;
        public const int DssXmlFunctionEquals = 6;
        public const int DssXmlFunctionNotEqual = 7;
        public const int DssXmlFunctionGreater = 8;
        public const int DssXmlFunctionLess = 9;
        public const int DssXmlFunctionGreaterEqual = 10;
        public const int DssXmlFunctionLessEqual = 11;
        public const int DssXmlFunctionSum = 12;
        public const int DssXmlFunctionCount = 13;
        public const int DssXmlFunctionAvg = 14;
        public const int DssXmlFunctionMin = 15;
        public const int DssXmlFunctionMax = 16;
        public const int DssXmlFunctionBetween = 17;
        public const int DssXmlFunctionLike = 18;
        public const int DssXmlFunctionAnd = 19;
        public const int DssXmlFunctionOr = 20;
        public const int DssXmlFunctionNot = 21;
        public const int DssXmlFunctionIn = 22;
        public const int DssXmlFunctionRank = 23;
        public const int DssXmlFunctionAbs = 24;
        public const int DssXmlFunctionRunningSum = 25;
        public const int DssXmlFunctionRunningAvg = 26;
        public const int DssXmlFunctionMovingSum = 28;
        public const int DssXmlFunctionMovingAvg = 27;
        public const int DssXmlFunctionProduct = 29;
        public const int DssXmlFunctionMedian = 30;
        public const int DssXmlFunctionMode = 31;
        public const int DssXmlFunctionStdev = 32;
        public const int DssXmlFunctionVar = 33;
        public const int DssXmlFunctionGeomean = 34;
        public const int DssXmlFunctionEqualEnhanced = 35;
        public const int DssXmlFunctionNotEqualEnhanced = 36;
        public const int DssXmlFunctionGreaterEqualEnhanced = 37;
        public const int DssXmlFunctionLessEqualEnhanced = 38;
        public const int DssXmlFunctionBetweenEnhanced = 39;
        public const int DssXmlFunctionBanding = 40;
        public const int DssXmlFunctionBandingC = 41;
        public const int DssXmlFunctionBandingP = 42;
        public const int DssXmlFunctionNotLike = 43;
        public const int DssXmlFunctionNotBetween = 44;
        public const int DssXmlFunctionIntersect = 45;
        public const int DssXmlFunctionIntersectIn = 46;
        public const int DssXmlFunctionNullToZero = 47;
        public const int DssXmlFunctionZeroToNull = 48;
        public const int DssXmlFunctionApplySimple = 49;
        public const int DssXmlFunctionApplyAggregation = 50;
        public const int DssXmlFunctionApplyLogic = 51;
        public const int DssXmlFunctionApplyComparison = 52;
        public const int DssXmlFunctionApplyRelative = 53;
        public const int DssXmlFunctionIsNull = 54;
        public const int DssXmlFunctionIsNotNull = 55;
        public const int DssXmlFunctionUcase = 56;
        public const int DssXmlFunctionNotIn = 57;
        public const int DssXmlFunctionNTile = 58;
        public const int DssXmlFunctionPercentile = 59;
        public const int DssXmlFunctionMovingMax = 60;
        public const int DssXmlFunctionMovingMin = 61;
        public const int DssXmlFunctionMovingDifference = 62;
        public const int DssXmlFunctionMovingStdev = 63;
        public const int DssXmlFunctionExpWghMovingAvg = 64;
        public const int DssXmlFunctionMovingCount = 65;
        public const int DssXmlFunctionRunningMax = 66;
        public const int DssXmlFunctionRunningMin = 67;
        public const int DssXmlFunctionRunningStdev = 68;
        public const int DssXmlFunctionRunningCount = 69;
        public const int DssXmlFunctionExpWghRunningAvg = 70;
        public const int DssXmlFunctionNotBetweenEnhanced = 71;
        public const int DssXmlFunctionConcat = 72;
        public const int DssXmlFunctionFirstInRange = 73;
        public const int DssXmlFunctionLastInRange = 74;
        public const int DssXmlFunctionValueSegment = 75;
        public const int DssXmlFunctionContains = 76;
        public const int DssXmlFunctionBeginsWith = 77;
        public const int DssXmlFunctionEndsWith = 78;
        public const int DssXmlFunctionNotContains = 79;
        public const int DssXmlFunctionNotBeginsWith = 80;
        public const int DssXmlFunctionNotEndsWith = 81;
        public const int DssXmlFunctionCase = 82;
        public const int DssXmlFunctionCaseV = 83;
        public const int DssXmlFunctionStdevP = 84;
        public const int DssXmlFunctionRunningStdevP = 85;
        public const int DssXmlFunctionMovingStdevP = 86;
        public const int DssXmlFunctionNTileS = 87;
        public const int DssXmlFunctionNTileVS = 88;
        public const int DssXmlFunctionVarP = 89;
        public const int DssXmlFunctionCurrentDate = 90;
        public const int DssXmlFunctionDayOfMonth = 91;
        public const int DssXmlFunctionDayOfWeek = 92;
        public const int DssXmlFunctionDayOfYear = 93;
        public const int DssXmlFunctionWeek = 94;
        public const int DssXmlFunctionMonth = 95;
        public const int DssXmlFunctionQuarter = 96;
        public const int DssXmlFunctionYear = 97;
        public const int DssXmlFunctionCurrentDateTime = 98;
        public const int DssXmlFunctionCurrentTime = 99;
        public const int DssXmlFunctionHour = 100;
        public const int DssXmlFunctionMinute = 101;
        public const int DssXmlFunctionSecond = 102;
        public const int DssXmlFunctionMilliSecond = 103;
        public const int DssXmlFunctionConcatNoBlank = 104;
        public const int DssXmlFunctionLength = 105;
        public const int DssXmlFunctionLower = 106;
        public const int DssXmlFunctionLTrim = 107;
        public const int DssXmlFunctionPosition = 108;
        public const int DssXmlFunctionRTrim = 109;
        public const int DssXmlFunctionSubStr = 110;
        public const int DssXmlFunctionInitCap = 111;
        public const int DssXmlFunctionTrim = 112;
        public const int DssXmlFunctionRightStr = 113;
        public const int DssXmlFunctionLeftStr = 114;
        public const int DssXmlFunctionGreatest = 115;
        public const int DssXmlFunctionLeast = 116;
        public const int DssXmlFunctionAdd = 134;
        public const int DssXMlFunctionAverage = 135;
        public const int DssXMLFunctionMultiply = 136;
        public const int DssXmlFunctionBandingM = 137;
        public const int DssXmlFunctionReservedLastOne = 138;
        public const int DssXmlFunctionTuple = 1000;
    }

    public class DSSXMLFunctionsFlags
    {
        public const int DssXmlFunctionsDefault = 0;
        public const int DssXmlFunctionsSkipDBOnly = 1;
        public const int DssXmlFunctionsSkipNonPrefix = 2;
        public const int DssXmlFunctionsSkipNonSQLArithmetic = 4;
        public const int DssXmlFunctionsIncludeDefinitions = -2147483648;
    }

    public class DSSXMLFunnelMinorTypes
    {
        public const int DssXmlFunnelMinorType = 1;
    }

    public class DSSXMLGanttMinorTypes
    {
        public const int DssXmlGanttMinorType = 1;
    }

    public class DSSXMLGaugeMinorTypes
    {
        public const int DssXmlGaugeMinorType = 1;
    }

    public class DSSXMLGetClusterMembershipFlags
    {
        public const int DssXmlGetClusterMembershipFlag = 1;
    }

    public class DSSXMLGetElementsFlags
    {
        public const int DssXmlGetElementsDefault = 0;
        public const int DssXmlGetElementTerseIds = 1;
        public const int DssXmlGetElementLeafElements = 2;
    }

    public class DSSXMLGetScheduleFlags
    {
        public const int DssXmlGetScheduleReserved = 0;
        public const int DssXmlGetScheduleSimple = 1;
        public const int DssXmlGetScheduleDetails = 2;
    }

    public class DSSXMLGraphAttributeID
    {
        public const int DssXmlAttributeIDShow2DO = -117;
        public const int DssXmlAttributeIDShow2DX = -127;
        public const int DssXmlAttributeIDShow2DY1 = -128;
        public const int DssXmlAttributeIDShow2DY2 = -129;
        public const int DssXmlAttributeIDFormatDataTextX = 28;
        public const int DssXmlAttributeIDFormatDataTextY1 = 29;
        public const int DssXmlAttributeIDFormatDataTextY2 = 30;
        public const int DssXmlAttributeIDFormatX = 33;
        public const int DssXmlAttributeIDFormatY1 = 34;
        public const int DssXmlAttributeIDFormatY2 = 35;
        public const int DssXmlAttributeIDFormatZ = 36;
        public const int DssXmlAttributeIDLogScaleX = 57;
        public const int DssXmlAttributeIDLogScaleY1 = 58;
        public const int DssXmlAttributeIDLogScaleY2 = 59;
        public const int DssXmlAttributeID2DMarkerShape = 60;
        public const int DssXmlAttributeID2DScalefreqO1 = 87;
        public const int DssXmlAttributeID2DScalefreqX = 89;
        public const int DssXmlAttributeID2DScalefreqY1 = 90;
        public const int DssXmlAttributeID2DScalefreqY2 = 91;
        public const int DssXmlAttributeIDScaleX = 92;
        public const int DssXmlAttributeIDScaleY1 = 93;
        public const int DssXmlAttributeIDScaleY2 = 94;
        public const int DssXmlAttributeIDShowSDDataLineType = 96;
        public const int DxxXmlAttributeIDSDRiserType = 97;
        public const int DssXmlAttributeIDShowDataText = 113;
        public const int DssXmlAttributeIDShowDivBiPolar = 114;
        public const int DssXmlAttributeIDShowErrorBar = 115;
        public const int DssXmlAttributeIDShowLegendText = 116;
        public const int DssXmlAttributeIDShow2DOLabel = 117;
        public const int DssXmlAttributeIDShow2DXTitle = 124;
        public const int DssXmlAttributeIDShow2DY1Title = 125;
        public const int DssXmlAttributeIDShow2DY2Title = 126;
        public const int DssXmlAttributeIDShow2DXValue = 127;
        public const int DssXmlAttributeIDShow2DY1Value = 128;
        public const int DssXmlAttributeIDShow2DY2Value = 129;
        public const int DssXmlAttributeIDShowZeroLineX = 130;
        public const int DssXmlAttributeIDShowZeroLineY1 = 131;
        public const int DssXmlAttributeIDShowZeroLineY2 = 132;
        public const int DssXmlAttributeIDSeriesOnY2 = 140;
        public const int DssXmlAttributeIDStaggerO = 142;
        public const int DssXmlAttributeIDStaggerX = 144;
        public const int DssXmlAttributeIDStaggerY1 = 145;
        public const int DssXmlAttributeIDStaggerY2 = 146;
        public const int DssXmlAttributeIDShowFeeler = 204;
        public const int DssXmlAttributeIDShowLBLFeeler = 205;
        public const int DssXmlAttributeIDShowLBLPie = 206;
        public const int DssXmlAttributeIDShowPolarConnectLines = 218;
        public const int DssXmlAttributeIDShowBubbleGrid = 245;
        public const int DssXmlAttributeIDSDShowDataText = 247;
        public const int DssXmlAttributeIDShowTitleSeries = 248;
        public const int DssXmlAttributeID3DScalefreqX = 299;
        public const int DssXmlAttributeID3DScalefreqY = 300;
        public const int DssXmlAttributeID3DScalefreqZ = 301;
        public const int DssXmlAttributeIDShow3DScatterLine = 308;
        public const int DssXmlAttributeIDShow3DCHeader = 318;
        public const int DssXmlAttributeIDShow3DColTitle = 319;
        public const int DssXmlAttributeIDShow3DLNumber = 321;
        public const int DssXmlAttributeIDShow3DLYTitle = 322;
        public const int DssXmlAttributeIDShow3DRNumber = 323;
        public const int DssXmlAttributeIDShow3DRYTitle = 324;
        public const int DssXmlAttributeIDShow3DRHeader = 325;
        public const int DssXmlAttributeIDShow3DRowTitle = 326;
        public const int DssXmlAttributeIDShow3DFloor = 327;
        public const int DssXmlAttributeIDShow3DLWall = 329;
        public const int DssXmlAttributeIDShow3DRWall = 330;
        public const int DssXmlAttributeIDSeriesColor = 556;
        public const int DssXmlAttributeIDAreaTransparent = 563;
        public const int DssXmlAttributeIDAreaColor = 564;
        public const int DssXmlAttributeIDFontAlign = 570;
        public const int DssXmlAttributeIDFontTransparent = 573;
        public const int DssXmlAttributeIDFontColor = 574;
        public const int DssXmlAttributeIDFontName = 576;
        public const int DssXmlAttributeIDFontStyle = 584;
        public const int DssXmlAttributeIDLineTransparent = 587;
        public const int DssXmlAttributeIDLineColor = 588;
        public const int DssXmlAttributeIDLinePattern = 589;
        public const int DssXmlAttributeIDShowfootnote = 596;
        public const int DssXmlAttributeIDShowSubtitle = 597;
        public const int DssXmlAttributeIDShowTitle = 598;
        public const int DssXmlAttributeIDFontBox = 601;
        public const int DssXmlAttributeIDFontSize = 603;
        public const int DssXmlAttributeIDLockFontSize = 604;
        public const int DssXmlAttributeIDLineWidth = 605;
        public const int DssXmlAttributeIDFormatDataTextAdv = 616;
        public const int DssXmlAttributeIDShowShowUserLine = 632;
        public const int DssXmlAttributeIDLegendLock = 644;
        public const int DssXmlAttributeIDGridStepY1 = 645;
        public const int DssXmlAttributeIDGridStepY2 = 646;
        public const int DssXmlAttributeIDGridStepX = 647;
        public const int DssXmlAttributeIDScaleZ = 659;
        public const int DssXmlAttributeIDGridStepZ = 665;
        public const int DssXmlAttributeIDGaugeStyle = 795;
        public const int DssXmlAttributeIDGaugeBorderStyle = 796;
        public const int DssXmlAttributeIDGaugeBorderThickness = 797;
        public const int DssXmlAttributeIDGaugeRangeThickness = 798;
        public const int DssXmlAttributeIDGaugeRangeStartAngle = 799;
        public const int DssXmlAttributeIDGaugeRangeStopAngle = 800;
        public const int DssXmlAttributeIDGaugeNeedleStyle = 801;
        public const int DssXmlAttributeIDFontAntiAlias = 881;
        public const int DssXmlAttributeIDGaugeQBShow = 10120;
        public const int DssXmlAttributeIDGaugeQBStart = 10121;
        public const int DssXmlAttributeIDGaugeQBEnd = 10122;
        public const int DssXmlAttributeIDFillEffect = 10002;
        public const int DssXmlAttributeIDShowToolTip = 10041;
        public const int DssXmlAttributeIDSDShowToolTip = 10004;
        public const int DssXmlAttributeIDToolTip = 10042;
        public const int DssXmlAttributeIDSDToolTip = 10005;
        public const int DssXmlAttributeIDContent = 9999;
        public const int DssXmlAttributeIDOriContent = 10001;
        public const int DssXmlAttributeIDNumberCategory = 10006;
        public const int DssXmlAttributeIDDecimalPlaces = 10007;
        public const int DssXmlAttributeIDThousandSeparator = 10008;
        public const int DssXmlAttributeIDCurrencySymbol = 10009;
        public const int DssXmlAttributeIDCurrencyPosition = 10010;
        public const int DssXmlAttributeIDNegativeNumber = 10011;
        public const int DssXmlAttributeIDNumberFormat = 10012;
        public const int DssXmlAttributeIDAutoArrange = 10053;
        public const int DssXmlAttributeIDAreaSFX = 567;
        public const int DssXmlAttributeIDDualAxisAuto = 10054;
        public const int DssXmlGraphAxisNumberFormat = 10077;
        public const int DssXmlAttributeIDAreaAlpha = 608;
        public const int DssXmlAttributeIDLineAlpha = 609;
        public const int DssXmlAttributeIDFontAlpha = 610;
        public const int DssXmlAttributeIDBevelType = 879;
        public const int DssXmlAttributeIDCurvedLines = 804;
        public const int DssXmlEnhancedSeriesEffect = 10123;
        public const int DssXmlAttributeIDFormatXAxisValues = 618;
        public const int DssXmlAttributeIDFormatY1AxisValues = 619;
        public const int DssXmlAttributeIDFormatY2AxisValues = 620;
        public const int DssXmlAttributeIDFormatZAxisValues = 621;
    }

    public class DSSXMLGraphGaugeBorderStyle
    {
        public const int DssXmlGaugeBorderStyleNone = 0;
        public const int DssXmlGaugeBorderStyleSimple = 1;
        public const int DssXmlGaugeBorderStyle3D = 2;
        public const int DssXmlGaugeBorderStyleEmboss = 3;
        public const int DssXmlGaugeBorderStyleDonut = 4;
        public const int DssXmlGaugeBorderStyleMetallic = 5;
        public const int DssXmlGaugeBorderStyleClipped = 6;
    }

    public class DSSXMLGraphGaugeNeedleStyle
    {
        public const int DssXmlGaugeNeedleStyleSimple = 0;
        public const int DssXmlGaugeNeedleStyleSimpleLong = 1;
        public const int DssXmlGaugeNeedleStyleSimpleWithBase = 2;
        public const int DssXmlGaugeNeedleStyleSteeple = 3;
        public const int DssXmlGaugeNeedleStyleSteepleLong = 4;
        public const int DssXmlGaugeNeedleStyleRectangular = 5;
        public const int DssXmlGaugeNeedleStyleThin = 6;
        public const int DssXmlGaugeNeedleStyleThinLong = 7;
        public const int DssXmlGaugeNeedleStyleBar = 100;
        public const int DssXmlGaugeNeedleStyleSlider = 101;
        public const int DssXmlGaugeNeedleStyleThermometer = 102;
        public const int DssXmlGaugeNeedleStyleSymbol = 103;
    }

    public class DSSXMLGraphGaugeStyle
    {
        public const int DssXmlGaugeStyleCircular = 0;
        public const int DssXmlGaugeStyleAlphaNumeric = 1;
        public const int DssXmlGaugeStyleLinear = 2;
        public const int DssXmlGaugeStyleLED = 3;
        public const int DssXmlGaugeStyleMultiLED = 4;
        public const int DssXmlGaugeStyleThermometer = 5;
        public const int DssXmlGaugeStyleCircularNew = 6;
    }

    public class DSSXMLGraphImgTypes
    {
        public const int DssXmlGraphImgGIF = 1;
        public const int DssXmlGraphImgCGIF = 2;
        public const int DssXmlGraphImgJPEG = 3;
        public const int DssXmlGraphImgPNG = 4;
        public const int DssXmlGraphImgPCT = 5;
        public const int DssXmlGraphImgPCX = 6;
        public const int DssXmlGraphImgPSD = 7;
        public const int DssXmlGraphImgTGA = 8;
        public const int DssXmlGraphImgTIF = 9;
        public const int DssXmlGraphImgBMP = 10;
        public const int DssXmlGraphImgVECTOR = 11;
    }

    public class DSSXMLGraphMarkerShape
    {
        public const int DssXmlMarkerShapeNone = 0;
        public const int DssXmlMarkerShapeRectangle = 1;
        public const int DssXmlMarkerShapeStar45 = 2;
        public const int DssXmlMarkerShapePlus = 3;
        public const int DssXmlMarkerShapeCircle = 4;
        public const int DssXmlMarkerShapeDiamond = 5;
        public const int DssXmlMarkerShapeSpikedX = 6;
        public const int DssXmlMarkerShapePlainX = 7;
        public const int DssXmlMarkerShapeTriangle1 = 8;
        public const int DssXmlMarkerShapeStarSkewed = 9;
        public const int DssXmlMarkerShapeFatPlus = 10;
        public const int DssXmlMarkerShapeStar90 = 11;
        public const int DssXmlMarkerShapeSoftX = 12;
        public const int DssXmlMarkerShapePiratePlus = 13;
        public const int DssXmlMarkerShapeFatX = 14;
        public const int DssXmlMarkerShapeCastle = 15;
        public const int DssXmlMarkerShapeTriangle2 = 16;
        public const int DssXmlMarkerShapeTriangle3 = 17;
        public const int DssXmlMarkerShapeTriangle4 = 18;
        public const int DssXmlMarkerShapeTriangle5 = 19;
        public const int DssXmlMarkerShapeTriangle6 = 20;
        public const int DssXmlMarkerShapeTriangle7 = 21;
        public const int DssXmlMarkerShapeTriangle8 = 22;
        public const int DssXmlMarkerShapeEllipse = 23;
        public const int DssXmlMarkerShapeSquare = 24;
        public const int DssXmlMarkerShapeHexagon = 25;
        public const int DssXmlMarkerShapePentagon = 26;
        public const int DssXmlMarkerShapeHouse = 27;
        public const int DssXmlMarkerShapePentagram = 28;
        public const int DssXmlMarkerShapeFontMarker = 29;
        public const int DssXmlMarkerShapeBoxedPlus = 30;
        public const int DssXmlMarkerShapeBoxedX = 31;
        public const int DssXmlMarkerShapeHourGlass = 32;
        public const int DssXmlMarkerShapeHourGlassTransparent = 33;
        public const int DssXmlMarkerShapeVerticalLine = 34;
        public const int DssXmlMarkerShapeHorizontalLine = 35;
        public const int DssXmlMarkerShapeAsterisk = 36;
        public const int DssXmlMarkerShapeStar5 = 37;
        public const int DssXmlMarkerShapeStar6 = 38;
        public const int DssXmlMarkerShapeStar8 = 39;
        public const int DssXmlMarkerShapeBevelBox = 40;
        public const int DssXmlMarkerShapeReverseBevelBox = 41;
        public const int DssXmlMarkerShapeSlimHorizontalLine = 42;
        public const int DssXmlMarkerShapeSlimVerticalLine = 43;
        public const int DssXmlMarkerShapeSlimPlus = 44;
        public const int DssXmlMarkerShapeSlimBoxedPlus = 45;
        public const int DssXmlMarkerShapeSlimX = 46;
        public const int DssXmlMarkerShapeSlimBoxedX = 47;
        public const int DssXmlMarkerShapeRotatedHourGlass = 48;
        public const int DssXmlMarkerShapeSmallBevelBox = 49;
    }

    public class DSSXMLGraphObjectID
    {
        public const int DssXmlObjectIDBackground = 1;
        public const int DssXmlObjectIDFrame = 2;
        public const int DssXmlObjectIDFootnote = 3;
        public const int DssXmlObjectIDSubtitle = 4;
        public const int DssXmlObjectIDTitle = 5;
        public const int DssXmlObjectIDLegendArea = 6;
        public const int DssXmlObjectIDLegendLine = 7;
        public const int DssXmlObjectIDLegendMarker = 8;
        public const int DssXmlObjectIDLegendText = 9;
        public const int DssXmlObjectIDCurve = 257;
        public const int DssXmlObjectIDDataLine = 258;
        public const int DssXmlObjectIDDataText = 260;
        public const int DssXmlObjectIDDivBiPolar = 261;
        public const int DssXmlObjectIDErrorBar = 262;
        public const int DssXmlObjectIDLinrExp = 263;
        public const int DssXmlObjectIDLinrLine = 264;
        public const int DssXmlObjectIDLinrLog = 265;
        public const int DssXmlObjectIDLinrNatLog = 266;
        public const int DssXmlObjectIDLinrNPoly = 267;
        public const int DssXmlObjectIDLinrText = 268;
        public const int DssXmlObjectIDMeanLine = 269;
        public const int DssXmlObjectIDMovAvgLine = 270;
        public const int DssXmlObjectIDO1Body = 271;
        public const int DssXmlObjectID2DO1Label = 272;
        public const int DssXmlObjectIDO1Major = 273;
        public const int DssXmlObjectIDO1Minor = 274;
        public const int DssXmlObjectIDO1Super = 275;
        public const int DssXmlObjectIDRiser = 286;
        public const int DssXmlObjectIDRiserShadow = 287;
        public const int DssXmlObjectIDStdDevLine = 288;
        public const int DssXmlObjectIDX1Body = 289;
        public const int DssXmlObjectID2DX1Label = 290;
        public const int DssXmlObjectIDX1Major = 291;
        public const int DssXmlObjectIDX1Minor = 292;
        public const int DssXmlObjectID2DXTitle = 293;
        public const int DssXmlObjectIDX1Zero = 294;
        public const int DssXmlObjectIDY1Body = 295;
        public const int DssXmlObjectID2DY1Label = 296;
        public const int DssXmlObjectIDY1Major = 297;
        public const int DssXmlObjectIDY1Minor = 298;
        public const int DssXmlObjectID2DY1Title = 299;
        public const int DssXmlObjectIDY1Zero = 300;
        public const int DssXmlObjectIDY2Body = 301;
        public const int DssXmlObjectID2DY2Label = 302;
        public const int DssXmlObjectIDY2Major = 303;
        public const int DssXmlObjectIDY2Minor = 304;
        public const int DssXmlObjectID2DY2Title = 305;
        public const int DssXmlObjectIDY2Zero = 306;
        public const int DssXmlObjectIDOBPBox = 329;
        public const int DssXmlObjectIDOBPMedian = 330;
        public const int DssXmlObjectIDOBPStem = 331;
        public const int DssXmlObjectIDOBPTail = 332;
        public const int DssXmlObjectIDPieFeelerLine = 337;
        public const int DssXmlObjectIDPieFeelerLabel = 338;
        public const int DssXmlObjectIDPieGroupLabel = 339;
        public const int DssXmlObjectIDPieRingLabel = 340;
        public const int DssXmlObjectIDPieSlice = 341;
        public const int DssXmlObjectIDPieSliceCrust = 342;
        public const int DssXmlObjectIDPieSliceFace = 343;
        public const int DssXmlObjectIDPieSliceRing = 344;
        public const int DssXmlObjectIDPolarAxisCircles = 346;
        public const int DssXmlObjectIDPolarAxisLabels = 347;
        public const int DssXmlObjectIDPolarAxisLines = 348;
        public const int DssXmlObjectIDPolarAxisThick = 349;
        public const int DssXmlObjectIDBubbleQuadLine = 363;
        public const int DssXmlObjectIDMDLabel0 = 364;
        public const int DssXmlObjectIDMDLabel1 = 365;
        public const int DssXmlObjectIDMDLabel2 = 366;
        public const int DssXmlObjectIDMDLabel3 = 367;
        public const int DssXmlObjectIDMDLabel4 = 368;
        public const int DssXmlObjectIDMDLabel5 = 369;
        public const int DssXmlObjectIDMDLabel6 = 370;
        public const int DssXmlObjectIDMDLabel7 = 371;
        public const int DssXmlObjectIDMDLabel8 = 372;
        public const int DssXmlObjectIDMDLabel9 = 373;
        public const int DssXmlObjectIDMDLabel10 = 374;
        public const int DssXmlObjectIDMDLabel11 = 375;
        public const int DssXmlObjectIDMDLabel12 = 376;
        public const int DssXmlObjectIDMDLabel13 = 377;
        public const int DssXmlObjectIDMDLabel14 = 378;
        public const int DssXmlObjectIDMDLabel15 = 379;
        public const int DssXmlObjectIDMDLabel16 = 380;
        public const int DssXmlObjectIDMDLabel17 = 381;
        public const int DssXmlObjectIDMDLabel18 = 382;
        public const int DssXmlObjectIDMDLabel19 = 383;
        public const int DssXmlObjectIDMDLabel20 = 384;
        public const int DssXmlObjectIDMDLabel21 = 385;
        public const int DssXmlObjectIDMDLabel22 = 386;
        public const int DssXmlObjectIDMDLabel23 = 387;
        public const int DssXmlObjectIDMDLabel24 = 388;
        public const int DssXmlObjectIDMDLabel25 = 389;
        public const int DssXmlObjectIDMDLabel26 = 390;
        public const int DssXmlObjectIDMDLabel27 = 391;
        public const int DssXmlObjectIDMDLabel28 = 392;
        public const int DssXmlObjectIDMDLabel29 = 393;
        public const int DssXmlObjectIDMDLabel30 = 394;
        public const int DssXmlObjectIDMDLabel31 = 395;
        public const int DssXmlObjectIDRiser2 = 396;
        public const int DssXmlObjectIDRiser3 = 397;
        public const int DssXmlObjectIDSeriesTitle = 398;
        public const int DssXmlObjectIDConnectStackLine = 399;
        public const int DssXmlObjectIDUserLine1 = 414;
        public const int DssXmlObjectIDUserLine2 = 415;
        public const int DssXmlObjectIDUserLine3 = 416;
        public const int DssXmlObjectIDUserLine4 = 417;
        public const int DssXmlObjectIDOPILBLValue = 418;
        public const int DssXmlObjectIDGauge = 444;
        public const int DssXmlObjectIDGaugeContainer = 445;
        public const int DssXmlObjectIDGaugeTitle = 450;
        public const int DssXmlObjectIDGaugeRange = 451;
        public const int DssXmlObjectID3DColHeader = 512;
        public const int DssXmlObjectID3DColHeaderBox = 514;
        public const int DssXmlObjectID3DColTitle = 515;
        public const int DssXmlObjectID3DColTitleBox = 517;
        public const int DssXmlObjectID3DDataLabelBox = 519;
        public const int DssXmlObjectID3DFloor1 = 521;
        public const int DssXmlObjectID3DFloor2 = 522;
        public const int DssXmlObjectID3DFloor3 = 523;
        public const int DssXmlObjectID3DFrameGridX = 524;
        public const int DssXmlObjectID3DFrameGridY = 525;
        public const int DssXmlObjectID3DFrameGridZ = 526;
        public const int DssXmlObjectID3DLastRiserFace = 527;
        public const int DssXmlObjectID3DLeftWall1 = 528;
        public const int DssXmlObjectID3DLeftWall2 = 529;
        public const int DssXmlObjectID3DLeftWall3 = 530;
        public const int DssXmlObjectID3DLYHeader = 531;
        public const int DssXmlObjectID3DLYHeaderBox = 533;
        public const int DssXmlObjectID3DLYTitle = 534;
        public const int DssXmlObjectID3DLYTitleBox = 536;
        public const int DssXmlObjectID3DRightWall1 = 537;
        public const int DssXmlObjectID3DRightWall2 = 538;
        public const int DssXmlObjectID3DRightWall3 = 539;
        public const int DssXmlObjectID3DRiserFace1 = 540;
        public const int DssXmlObjectID3DRiserFace2 = 541;
        public const int DssXmlObjectID3DRiserFace3 = 542;
        public const int DssXmlObjectID3DRiserFace4 = 543;
        public const int DssXmlObjectID3DRiserFace5 = 544;
        public const int DssXmlObjectID3DRiserFace6 = 545;
        public const int DssXmlObjectID3DRiserFace7 = 546;
        public const int DssXmlObjectID3DRiserFace8 = 547;
        public const int DssXmlObjectID3DRiserFace9 = 548;
        public const int DssXmlObjectID3DRiserGridX = 549;
        public const int DssXmlObjectID3DRiserGridY = 550;
        public const int DssXmlObjectID3DRiserGridZ = 551;
        public const int DssXmlObjectID3DRowHeader = 552;
        public const int DssXmlObjectID3DRowHeaderBox = 554;
        public const int DssXmlObjectID3DRowTitle = 555;
        public const int DssXmlObjectID3DRowTitleBox = 557;
        public const int DssXmlObjectID3DLRHeader = 558;
        public const int DssXmlObjectID3DRYHeaderBox = 560;
        public const int DssXmlObjectID3DRYTitle = 561;
        public const int DssXmlObjectID3DRYTitleBox = 563;
        public const int DssXmlObjectID3DScatterLine = 564;
        public const int DssXmlObjectID3DTieLineX = 565;
        public const int DssXmlObjectID3DTieLineY = 566;
        public const int DssXmlObjectID3DTieLineZ = 567;
        public const int DssXmlObjectID3DRiserVarFace = 568;
        public const int DssXmlObjectIDGaugeBorder = 1319;
        public const int DssXmlObjectIDNull = 9999;
        public const int DssXmlObjectIDAllTexts = 10000;
        public const int DssXmlObjectIDAllTitles = 10001;
        public const int DssXmlObjectIDAllAxes = 10002;
        public const int DssXmlObjectIDAllDataText = 10003;
        public const int DssXmlObjectIDTooltips = 10004;
    }

    public class DSSXMLGraphRiserEmphasisType
    {
        public const int DssXmlRiserEmphasisNone = 0;
        public const int DssXmlRiserEmphasisLineOrLineToBar = 1;
        public const int DssXmlRiserEmphasisAreaOrAreaToBar = 2;
    }

    public class DSSXMLHAreaMinorTypes
    {
        public const int DssXmlHAreaMinorTypeAbsolute = 1;
        public const int DssXmlHAreaMinorTypeStacked = 2;
        public const int DssXmlHAreaMinorTypeBiPolarAbsolute = 4;
        public const int DssXmlHAreaMinorTypeBiPolarStacked = 8;
        public const int DssXmlHAreaMinorTypeDualAxisAbsolute = 16;
        public const int DssXmlHAreaMinorTypeDualAxisStacked = 32;
        public const int DssXmlHAreaMinorTypePercent = 64;
    }

    public class DSSXMLHBarMinorTypes
    {
        public const int DssXmlVBarMinorTypeSideBySide = 1;
        public const int DssXmlVBarMinorTypeStacked = 2;
        public const int DssXmlVBarMinorTypeDualAxisSideBySide = 16;
        public const int DssXmlVBarMinorTypeDualAxisStacked = 32;
        public const int DssXmlVBarMinorTypeBiPolarSideBySide = 4;
        public const int DssXmlVBarMinorTypeBiPolarStacked = 8;
        public const int DssXmlVBarMinorTypePercent = 64;
        public const int DssXmlHBarMinorTypeAbsolute = 128;
    }

    public class DSSXMLHichertMinorTypes
    {
        public const int DssXmlHichertLipstickMinorTypeVertical = 1;
        public const int DssXmlHichertLipstickMinorTypeHorizontal = 2;
        public const int DssXmlHichertOverlappingBarsMinorTypeVertical = 4;
        public const int DssXmlHichertOverlappingBarsMinorTypeHorizontal = 8;
    }

    public class DSSXMLHiloStockMinorTypes
    {
        public const int DssXmlHiloStockMinorTypeHiloStock = 1;
        public const int DssXmlHiloStockMinorTypeDualAxis = 2;
        public const int DssXmlHiloStockMinorTypeHiloOpen = 4;
        public const int DssXmlHiloStockMinorTypeHiloOpenDA = 8;
        public const int DssXmlHiloStockMinorTypeHiloOpenClose = 16;
        public const int DssXmlHiloStockMinorTypeHiloOpenCloseDA = 32;
    }

    public class DSSXMLHistogramMinorTypes
    {
        public const int DssXmlHistogramMinorTypeVertical = 1;
        public const int DssXmlHistogramMinorTypeHorizontal = 2;
    }

    public class DSSXMLHLineMinorTypes
    {
        public const int DssXmlHLineMinorTypeAbsolute = 1;
        public const int DssXmlHLineMinorTypeStacked = 2;
        public const int DssXmlHLineMinorTypeBiPolarAbsolute = 4;
        public const int DssXmlHLineMinorTypeBiPolarStacked = 8;
        public const int DssXmlHLineMinorTypeDualAxisAbsolute = 16;
        public const int DssXmlHLineMinorTypeDualAxisStacked = 32;
        public const int DssXmlHLineMinorTypePercent = 64;
    }

    public class DSSXMLIDType
    {
        public const int DssXmlIDTypeDocumentID = 0;
        public const int DssXmlIDTypeReportID = 1;
        public const int DssXmlIDTypeMessageID = 2;
    }

    public class DSSXMLInboxDeleteFlags
    {
        public const int DssXmlInboxDeleteReserved = 0;
        public const int DssXmlInboxDeleteKeepInWorkingSet = 4096;
        public const int DssXmlInboxDeleteOnlyFromWorkingSet = 8192;
        public const int InboxDeleteFlagsRetainWorkingSetMessage = 4096;
    }

    public class DSSXMLInboxFlags
    {
        public const int DssXmlInboxBrowsingInfo = 1;
        public const int DssXmlInboxBrowsingDetail = 2;
        public const int DssXmlInboxCurrentProject = 3;
        public const int DssXmlInboxAllProjects = 4;
        public const int DssXmlInboxKeepLatest = 5;
        public const int DssXmlInboxKeepAsIs = 6;
    }

    public class DSSXMLInitRepositoryModes
    {
        public const int DssXmlCreateRepository = 2;
        public const int DssXmlUseRepository = 1;
    }

    public class DSSXMLJobInfo
    {
        public const int DssXmlJobInfoJobID = 0;
        public const int DssXmlJobInfoUserName = 1;
        public const int DssXmlJobInfoJobStatus = 2;
        public const int DssXmlJobInfoDescription = 3;
        public const int DssXmlJobInfoServerProjectId = 4;
        public const int DssXmlJobInfoProjectGUID = 5;
        public const int DssXmlJobInfoProjectName = 6;
        public const int DssXmlJobInfoJobPriority = 7;
        public const int DssXmlJobInfoCreationTime = 8;
        public const int DssXmlJobInfoServerName = 9;
        public const int DssXmlJobInfoClientMachine = 10;
        public const int DssXmlJobInfoDuration = 11;
        public const int DssXmlJobInfoUserLoginName = 12;
    }

    public class DSSXMLJobStatus
    {
        public const int DssXmlJobStatusReady = 0;
        public const int DssXmlJobStatusExecuting = 1;
        public const int DssXmlJobStatusWaiting = 2;
        public const int DssXmlJobStatusCompleted = 3;
        public const int DssXmlJobStatusError = 4;
        public const int DssXmlJobStatusCanceled = 5;
        public const int DssXmlJobStatusStopped = 6;
        public const int DssXmlJobStatusWaitingOnGoverner = 7;
        public const int DssXmlJobStatusWaitingForAutoPrompt = 8;
        public const int DssXmlJobStatusWaitingForProject = 9;
        public const int DssXmlJobStatusWaitingForCache = 10;
        public const int DssXmlJobStatusWaitingForChildren = 11;
        public const int DssXmlJobStatusWaitingForFetchingResults = 12;
    }

    public class DSSXMLJoinType
    {
        public const int DssXmlJoinTypeSmart = 1;
        public const int DssXmlJoinTypeDirect = 2;
        public const int DssXmlJoinTypeCorrelated = 3;
        public const int DssXmlJoinTypeCross = 4;
    }

    public class DSSXMLKeepIndependent
    {
        public const int DssXmlKeepIndependentReserved = 0;
        public const int DssXmlKeepIndependentDefault = 1;
        public const int DssXmlKeepIndependentSet = 2;
        public const int DssXmlKeepIndependentClear = 3;
    }

    public class DSSXMLKeepSort
    {
        public const int DssXmlKeepSortDefinition = 1;
        public const int DssXmlKeepSortOrder = 2;
    }

    public class DSSXMLLDAPCallConvention
    {
        public const int DssXmlLDAPCallConventionReserved = 0;
        public const int DssXmlLDAPCallConventionCDecl = 1;
        public const int DssXmlLDAPCallConventionStdCall = 2;
    }

    public class DSSXMLLDAPImport
    {
        public const int DssXmlLDAPImportNotImport = 0;
        public const int DssXmlLDAPImportImport = 1;
        public const int DssXmlLDAPImportForceNotImport = 2;
        public const int DssXmlLDAPImportForceImport = 3;
    }

    public class DSSXMLLDAPPlatform
    {
        public const int DssXmlLDAPPlatformReserved = 0;
        public const int DssXmlLDAPPlatformWindows = 1;
        public const int DssXmlLDAPPlatformSolaris = 2;
        public const int DssXmlLDAPPlatformUnix = 2;
        public const int DssXmlLDAPPlatformAIX = 3;
        public const int DssXmlLDAPPlatformLinux = 4;
        public const int DssXmlLDAPPlatformHPUX = 5;
        public const int DssXmlLDAPPlatformLastOne = 6;
    }

    public class DSSXMLLDAPServerSecureConnection
    {
        public const int DssXmlLDAPServerSecureConnectionPlainText = 0;
        public const int DssXmlLDAPServerSecureConnectionSSL = 1;
    }

    public class DSSXMLLDAPStringCoding
    {
        public const int DssXmlLDAPStringCodingUTF8 = 0;
        public const int DssXmlLDAPStringCodingMultiByte = 1;
    }

    public class DSSXMLLDAPSynch
    {
        public const int DssXmlLDAPSynchUser = 1;
        public const int DssXmlLDAPSynchGroup = 2;
        public const int DssXmlLDAPSynchAll = 3;
    }

    public class DSSXMLLDAPVendor
    {
        public const int DssXmlLDAPVendorOthers = 0;
        public const int DssXmlLDAPVendorNovell = 1;
        public const int DssXmlLDAPVendorADS = 2;
        public const int DssXmlLDAPVendorSunOne = 3;
        public const int DssXmlLDAPVendoriPlanet = 3;
        public const int DssXmlLDAPVendorIBM = 4;
        public const int DssXmlLDAPVendorOpenLDAP = 5;
        public const int DssXmlLDAPVendorHPUX = 6;
    }

    public class DSSXMLLDAPVerification
    {
        public const int DssXmlLDAPVerificationBind = 1;
        public const int DssXmlLDAPVerificationPasswordCompare = 2;
    }

    public class DSSXMLLDAPVersion
    {
        public const int DssXmlLDAPVersionV3 = 0;
    }

    public class DSSXMLLegendLock
    {
        public const int DssXmlLengendLockFreeFloat = 0;
        public const int DssXmlLegendLockRight = 1;
        public const int DssXmlLegendLockLeft = 2;
        public const int DssXmlLegendLockBottom = 3;
    }

    public class DSSXMLLevelFlags
    {
        public const int DssXmlCountLevel = 1;
        public const int DssXmlBrowsingLevel = 2;
        public const int DssXmlDetailLevel = 4;
    }

    public class DSSXMLLicenseComplianceCategory
    {
        public const int DssXmlLicenseComplianceCategoryUnknown = 0;
        public const int DssXmlLicenseComplianceCategoryCPU = 1;
        public const int DssXmlLicenseComplianceCategoryNamedUsers = 2;
        public const int DssXmlLicenseComplianceCategoryActivation = 3;
    }

    public class DSSXMLLicenseScheme
    {
        public const int DssXmlLicenseSchemeLatest = -1;
        public const int DssXmlLicenseScheme0701xx = 1;
        public const int DssXmlLicenseScheme070202 = 2;
        public const int DssXmlLicenseScheme070203 = 3;
        public const int DssXmlLicenseScheme070204 = 4;
        public const int DssXmlLicenseScheme070501 = 5;
    }

    public class DSSXMLLicenseStatus
    {
        public const int DssXmlLicenseStatusOK = 0;
        public const int DssXmlLicenseStatusOutOfCompliance = 1;
    }

    public class DSSXMLLicenseType
    {
        public const int DssXmlLicenseTypeUnknown = 0;
        public const int DssXmlLicenseTypeWebReporter = 1;
        public const int DssXmlLicenseTypeWebAnalyst = 2;
        public const int DssXmlLicenseTypeWebProfessional = 3;
        public const int DssXmlLicenseTypeDesktopAnalyst = 4;
        public const int DssXmlLicenseTypeDesktopDesigner = 5;
        public const int DssXmlLicenseTypeArchitect = 6;
        public const int DssXmlLicenseTypeAdministrator = 7;
        public const int DssXmlLicenseTypeGutenberg = 8;
        public const int DssXmlLicenseTypeOfficeIntelligence = 9;
        public const int DssXmlLicenseTypeOLAPServices = 10;
        public const int DssXmlLicenseTypeWebMultimediaTraining = 11;
        public const int DssXmlLicenseTypeWebReportClientBundle = 12;
        public const int DssXmlLicenseTypeMobile = 13;
        public const int DssXmlLicenseTypeIntegrityManager = 14;
        public const int DssXmlLicenseTypeMultiSource = 15;
        public const int DssXmlLicenseTypeDistribution = 16;
        public const int DssLicenseTypeCommandManager = 17;
        public const int DssLicenseTypeEnterpriseManager = 18;
        public const int DssLicenseTypeObjectManager = 19;
        public const int DssLicenseTypeTransactionServices = 20;
        public const int DssXmlLicenseTypeCount = 20;
    }

    public class DSSXMLLinePattern
    {
        public const int DssXmlLinePatSolid = 0;
        public const int DssXmlLinePatDash = 1;
        public const int DssXmlLinePatDot = 2;
        public const int DssXmlLinePatDotAndDash = 3;
        public const int DssXmlLinePatDashandTwoDots = 4;
        public const int DssXmlLinePatMediumDash = 5;
        public const int DssXmlLinePatShortDash = 6;
        public const int DssXmlLinePatLongestDash = 7;
        public const int DssXmlLinePatLongDot = 8;
        public const int DssXmlLinePatDotDotDot = 9;
        public const int DssXmlLinePatDashDashDot = 10;
        public const int DssXmlLinePatDashDashDotDot = 11;
        public const int DssXmlLinePatLongDashDot = 12;
        public const int DssXmlLinePatLongDashDotDot = 13;
        public const int DssXmlLinePatLongDashDashDot = 14;
        public const int DssXmlLinePatLongDashDashDotDot = 15;
    }

    public class DSSXMLLockQuery
    {
        public const int DssXMLLockQueryReserved = 0;
        public const int DssXmlLLockQueryState = 1;
        public const int DssXmlLockQueryGovernor = 2;
        public const int DssXmlLockQueryTime = 4;
        public const int DssXmlLockQueryOwner = 8;
        public const int DssXmlLockQueryComment = 16;
        public const int DssXmlLockQueryMachine = 32;
    }

    public class DSSXMLLockState
    {
        public const int DssXmlLockStateNotLocked = 0;
        public const int DssXmlLockStateTransientIndividual = 1;
        public const int DssXmlLockStateTransientConstinuents = 3;
        public const int DssXmlLockStatePermanentIndividual = 5;
        public const int DssXmlLockStatePermanentConstituents = 7;
    }

    public class DSSXMLMajorGraphTypes
    {
        public const int DssXmlMajorGraphTypeVerticalArea = 1;
        public const int DssXmlMajorGraphTypeHorizontalArea = 2;
        public const int DssXmlMajorGraphTypeVerticalBar = 4;
        public const int DssXmlMajorGraphTypeHorizontalBar = 8;
        public const int DssXmlMajorGraphTypeVerticalLine = 16;
        public const int DssXmlMajorGraphTypeHorizontalLine = 32;
        public const int DssXmlMajorGraphTypePie = 64;
        public const int DssXmlMajorGraphTypeScatter = 128;
        public const int DssXmlMajorGraphTypePolar = 256;
        public const int DssXmlMajorGraphTypeRadar = 512;
        public const int DssXmlMajorGraphTypeBubble = 1024;
        public const int DssXmlMajorGraphTypeHiloStock = 2048;
        public const int DssXmlMajorGraphTypeHistogram = 4096;
        public const int DssXmlMajorGraphType3DRisers = 8192;
        public const int DssXmlMajorGraphType3DFloating = 16384;
        public const int DssXmlMajorGraphType3DConnectGroup = 32768;
        public const int DssXmlMajorGraphType3DConnectSeries = 65536;
        public const int DssXmlMajorGraphType3DSurface = 131072;
        public const int DssXmlMajorGraphType3DScatter = 262144;
        public const int DssXmlMajorGraphTypeGauge = 524288;
        public const int DssXmlMajorGraphTypeFunnel = 1048576;
        public const int DssXmlMajorGraphTypePareto = 2097152;
        public const int DssXmlMajorGraphTypeBoxPlot = 4194304;
        public const int DssXmlMajorGraphTypeGantt = 8388608;
        public const int DssXmlMajorGraphTypeCombination = 16777216;
        public const int DssXmlMajorGraphTypeHichert = 33554432;
    }

    public class DSSXMLManipulationType
    {
        public const int DssXmlManipulationNone = 0;
        public const int DssXmlManipulationFormat = 1;
        public const int DssXmlManipulationPivot = 2;
        public const int DssXmlManipulationSubset = 3;
        public const int DssXmlManipulationSQL = 4;
    }

    public class DSSXMLMDUpdateCommands
    {
        public const int DssXmlMDUpdateReserved = 0;
        public const int DssXmlMDUpdateCurrentVersion = 1;
        public const int DssXmlMDUpdateNeedUpdate = 2;
        public const int DssXmlMDUpdateUpdate = 3;
        public const int DssXmlMDUpdateUpdateSecurityModel = 4;
        public const int DssXmlMDUpdateUpdateDocDefns = 5;
        public const int DssXmlMDUpdateUpdateHistoryListSchedules = 6;
        public const int DssXmlMDUpdateUpdateTranslations = 7;
        public const int DssXmlMDUpdateUpdateMDXSourceObjects = 8;
        public const int DssXmlMDUpdateUpdateReportDefns = 9;
        public const int DssXmlMDUpdateUpdatePrivileges = 10;
        public const int DssXmlMDUpdateUpdateDefns = 11;
        public const int DssXmlMDUpdateUpdateUnifySystemObjects = 12;
        public const int DssXmlMDUpdateUpdatePrivilegesIncrmental = 13;
        public const int DssXmlMDUpdateUpdateXDAExtendedTypes = 14;
        public const int DssXmlMDUpdateConvertObjectsDefinitionToLeanFormat = 15;
        public const int DssXmlMDUpdateUnifyDSSystemObjects = 16;
    }

    public class DSSXMLMDUpdateFlags
    {
        public const int DssXmlMDUpdateFlagNeedUpdateCheckOnly = 1;
        public const int DssXmlMDUpdateFlagQuickCheck = 65536;
    }

    public class DSSXMLMDVersion
    {
        public const int DssXmlMDVersionReserved = 0;
        public const int DssXmlMDVersionMD4 = 4;
        public const int DssXmlMDVersionMD7 = 6;
    }

    public class DSSXMLMessageDuplicationMode
    {
        public const int DSSXMLMessageDuplicateModeDoNotAddToInbox = 0;
        public const int DSSXMLMessageDuplicateModeDerived = 1;
        public const int DSSXMLMessageDuplicateModeAddToInbox = 2;
        public const int DSSXMLMessageDuplicateModeLegacy = -1;
    }

    public class DSSXMLMessageStatus
    {
        public const int DssXmlMessageStatusReserved = 0;
        public const int DssXmlMessageStatusNewMsg = 1;
        public const int DssXmlMessageStatusReadMsg = 2;
        public const int DssXmlMessageStatusDeletedMsg = 4;
        public const int DssXmlMessageStatusWorkingMsg = 1073741824;
        public const int DssXmlMessageStatusSavedMsg = -2147483648;
    }

    public class DSSXMLMessageTypes
    {
        public const int DssXmlMessageTypeReserved = 0;
        public const int DssXmlMessageTypeGeneral = 1;
        public const int DssXmlMessageTypePersistent = 2;
        public const int DssXmlMessageTypeActive = 3;
    }

    public class DSSXMLMetricEditType
    {
        public const int DssXmlMetricEditTypeReserved = 0;
        public const int DssXmlMetricEditTypeAdvancedMetric = 65536;
        public const int DssXmlMetricEditTypeSimpleMetric = 131072;
        public const int DssXmlMetricEditTypeFunctionMetric = 196608;
    }

    public class DSSXMLMetricFormulaType
    {
        public const int DssXmlMetricFormulaIdentity = 1;
        public const int DssXmlMetricFormulaDifference = 2;
        public const int DssXmlMetricFormulaVarianceRatio = 3;
    }

    public class DSSXMLMetricType
    {
        public const int DssXmlMetricDMX = -7;
        public const int DssXmlMetricNormal = -8;
        public const int DssXmlMetricSubtotal = -9;
        public const int DssXmlMetricSubtotalSum = -10;
        public const int DssXmlMetricSubtotalCount = -11;
        public const int DssXmlMetricSubtotalAverage = -12;
        public const int DssXmlMetricSubtotalMin = -13;
        public const int DssXmlMetricSubtotalMax = -14;
        public const int DssXmlMetricSubtotalProduct = -15;
        public const int DssXmlMetricSubtotalMedian = -16;
        public const int DssXmlMetricSubtotalMode = -17;
        public const int DssXmlMetricSubtotalStdDev = -18;
        public const int DssXmlMetricSubtotalVariance = -19;
        public const int DssXmlMetricSubtotalGeometricMean = -20;
        public const int DssXmlMetricSubtotalAggregation = -21;
    }

    public class DSSXMLMonitorFilterOperator
    {
        public const int DssXmlGreater = 0;
        public const int DssXmlGreaterOrEqual = 1;
        public const int DssXmlLess = 2;
        public const int DssXmlLessOrEqual = 3;
        public const int DssXmlEqual = 4;
        public const int DssXmlBetween = 5;
        public const int DssXmlAny = 6;
    }

    public class DSSXMLMonitorType
    {
        public const int DssXmlMonitorTypeReserved = 0;
        public const int DssXmlMonitorTypePerformanceCounter = 1;
        public const int DssXmlMonitorTypeJobs = 2;
        public const int DssXmlMonitorUserConnections = 3;
        public const int DssXmlMonitorDBConnections = 4;
    }

    public class DSSXMLMonth
    {
        public const int DssXmlMonthJanuary = 1;
        public const int DssXmlMonthFebruary = 2;
        public const int DssXmlMonthMarch = 3;
        public const int DssXmlMonthApril = 4;
        public const int DssXmlMonthMay = 5;
        public const int DssXmlMonthJune = 6;
        public const int DssXmlMonthJuly = 7;
        public const int DssXmlMonthAugust = 8;
        public const int DssXmlMonthSeptember = 9;
        public const int DssXmlMonthOctober = 10;
        public const int DssXmlMonthNovember = 11;
        public const int DssXmlMonthDecember = 12;
    }

    public class DSSXMLNCSAllowChangeMaskType
    {
        public const int DssXmlNCSAllowChangeNone = 0;
        public const int DssXmlNCSAllowChangeDelivery = 1;
        public const int DssXmlNCSAllowChangePersonalization = 2;
        public const int DssXmlNCSAllowUnsubscribe = 4;
    }

    public class DSSXMLNCSContactSourceField
    {
        public const int DssXmlNCSContactDefaultField = 1;
        public const int DssXmlNCSContactDefinitionField = 2;
    }

    public class DSSXMLNCSEmailDestinationType
    {
        public const int DssXmlNCSEmailDestinationTypeTo = 1;
        public const int DssXmlNCSEmailDestinationTypeCC = 2;
        public const int DssXmlNCSEmailDestinationTypeBCC = 4;
    }

    public class DSSXMLNCSInstanceSourceField
    {
        public const int DssXmlNCSInstanceDefaultField = 1;
        public const int DssXmlNCSInstanceDefinitionField = 2;
        public const int DssXmlNCSInstanceDefaultFieldWithDisabledContact = 3;
        public const int DssXmlNCSInstanceDefinitionFieldWithDisabledContact = 4;
        public const int DssXmlNCSInstanceLightWeightField = 5;
        public const int DssXmlNCSInstanceCountField = 6;
        public const int DssXmlNCSInstanceAllInstancesLightWeightField = 7;
    }

    public class DSSXMLNCSOrderByContactField
    {
        public const int DssXmlNCSOrderByContactFieldReserved = 0;
        public const int DssXmlNCSOrderByContactLoginASC = 1;
        public const int DssXmlNCSOrderByContactLoginDESC = 2;
        public const int DssXmlNCSOrderByContactTypeASC = 3;
        public const int DssXmlNCSOrderByContactTypeDESC = 4;
        public const int DssXmlNCSOrderByContactStatusASC = 5;
        public const int DssXmlNCSOrderByContactStatusDESC = 6;
        public const int DssXmlNCSOrderByContactDescASC = 7;
        public const int DssXmlNCSOrderByContactDescDESC = 8;
    }

    public class DSSXMLNCSSubscriptionType
    {
        public const int DssXmlNCSSubscriptionSingle = 1;
        public const int DssXmlNCSSubscriptionMultipleParent = 2;
        public const int DssXmlNCSSubscriptionMultipleChild = 3;
    }

    public class DSSXMLNetProtocol
    {
        public const int DssXmlNetProtocolDefault = 0;
        public const int DssXmlNetProtocolTCP = 1;
        public const int DssXmlNetProtocolIPX = 2;
        public const int DssXmlNetProtocolNB = 3;
        public const int DssXmlNetProtocolUDP = 4;
        public const int DssXmlNetProtocolMUDP = 5;
    }

    public class DSSXMLNodeDimty
    {
        public const int DssXmlNodeDimtyReserved = 0;
        public const int DssXmlNodeDimtyNone = 1;
        public const int DssXmlNodeDimtyContinuation = 2;
        public const int DssXmlNodeDimtyExclusiveContinuation = 3;
        public const int DssXmlNodeDimtyOutputLevel = 4;
        public const int DssXmlNodeDimtyBreakBy = 5;
        public const int DssXmlNodeDimtyEmbedded = 6;
        public const int DssXmlNodeDimtyUnspecified = 7;
    }

    public class DSSXMLNodeState
    {
        public const int DssXmlNodeStateConnecting = 1;
        public const int DssXmlNodeStateConnected = 2;
        public const int DssXmlNodeStateDisconnecting = 3;
    }

    public class DSSXMLNodeType
    {
        public const int DssXmlNodeReserved = 0;
        public const int DssXmlNodeFormShortcut = 1;
        public const int DssXmlNodeElementsObject = 2;
        public const int DssXmlNodeConstant = 3;
        public const int DssXmlNodeOperator = 4;
        public const int DssXmlNodeShortcut = 5;
        public const int DssXmlNodeTime = 6;
        public const int DssXmlNodeRelationship = 7;
        public const int DssXmlNodeResidue = 8;
        public const int DssXmlNodeMe = 9;
        public const int DssXmlNodeBigDecimal = 10;
        public const int DssXmlNodeGroup = 14;
        public const int DssXmlNodeRef = 15;
    }

    public class DSSXMLNotificationDataID
    {
        public const int DssXmlNotificationDataIDReserved = 0;
        public const int DssXmlNotificationFileModuleReserved = 100;
        public const int DssXmlNotificationFileEnableEmail = 101;
        public const int DssXmlNotificationFileRecipientName = 102;
        public const int DssXmlNotificationFileOnwerName = 103;
        public const int DssXmlNotificationFileReportOrDocumentName = 104;
        public const int DssXmlNotificationFileProjectName = 105;
        public const int DssXmlNotificationFileDeliveryMethod = 106;
        public const int DssXmlNotificationFileSchedule = 107;
        public const int DssXmlNotificationFileDeliveryStatus = 108;
        public const int DssXmlNotificationFileDate = 109;
        public const int DssXmlNotificationFileTime = 110;
        public const int DssXmlNotificationFileFileLocation = 111;
        public const int DssXmlNotificationFileLinkToFile = 112;
        public const int DssXmlNotificationFileErrorMessage = 113;
        public const int DssXmlNotificationFileAppendText = 114;
        public const int DssXmlNotificationFileTextToAppend = 115;
        public const int DssXmlNotificationFileOnlySendSuccessToRecipient = 116;
        public const int DssXmlNotificationFileSendFailureToEmailAddress = 117;
        public const int DssXmlNotificationFileLastData = 118;
        public const int DssXmlNotificationPrintModuleReserved = 200;
        public const int DssXmlNotificationPrintEnableEmail = 201;
        public const int DssXmlNotificationPrintRecipientName = 202;
        public const int DssXmlNotificationPrintOnwerName = 203;
        public const int DssXmlNotificationPrintReportOrDocumentName = 204;
        public const int DssXmlNotificationPrintProjectName = 205;
        public const int DssXmlNotificationPrintDeliveryMethod = 206;
        public const int DssXmlNotificationPrintSchedule = 207;
        public const int DssXmlNotificationPrintDeliveryStatus = 208;
        public const int DssXmlNotificationPrintDate = 209;
        public const int DssXmlNotificationPrintTime = 210;
        public const int DssXmlNotificationPrintPrinterName = 211;
        public const int DssXmlNotificationPrintErrorMessage = 212;
        public const int DssXmlNotificationPrintAppendText = 213;
        public const int DssXmlNotificationPrintTextToAppend = 214;
        public const int DssXmlNotificationPrintOnlySendSuccessToRecipient = 215;
        public const int DssXmlNotificationPrintSendFailureToEmailAddress = 216;
        public const int DssXmlNotificationPrintLastData = 217;
        public const int DssXmlNotificationHistoryListModuleReserved = 300;
        public const int DssXmlNotificationHistoryListEnableEmail = 301;
        public const int DssXmlNotificationHistoryListRecipientName = 302;
        public const int DssXmlNotificationHistoryListOnwerName = 303;
        public const int DssXmlNotificationHistoryListReportOrDocumentName = 304;
        public const int DssXmlNotificationHistoryListProjectName = 305;
        public const int DssXmlNotificationHistoryListDeliveryMethod = 306;
        public const int DssXmlNotificationHistoryListSchedule = 307;
        public const int DssXmlNotificationHistoryListDeliveryStatus = 308;
        public const int DssXmlNotificationHistoryListDate = 309;
        public const int DssXmlNotificationHistoryListTime = 310;
        public const int DssXmlNotificationHistoryListLinkToHistoryList = 311;
        public const int DssXmlNotificationHistoryListErrorMessage = 312;
        public const int DssXmlNotificationHistoryListAppendText = 313;
        public const int DssXmlNotificationHistoryListTextToAppend = 314;
        public const int DssXmlNotificationHistoryListOnlySendSuccessToRecipient = 315;
        public const int DssXmlNotificationHistoryListSendFailureToEmailAddress = 316;
        public const int DssXmlNotificationHistoryListLastData = 317;
        public const int DssXmlNotificationEmailModuleReserved = 400;
        public const int DssXmlNotificationEmailEnableEmail = 401;
        public const int DssXmlNotificationEmailRecipientName = 402;
        public const int DssXmlNotificationEmailOnwerName = 403;
        public const int DssXmlNotificationEmailReportOrDocumentName = 404;
        public const int DssXmlNotificationEmailProjectName = 405;
        public const int DssXmlNotificationEmailDeliveryMethod = 406;
        public const int DssXmlNotificationEmailSchedule = 407;
        public const int DssXmlNotificationEmailDeliveryStatus = 408;
        public const int DssXmlNotificationEmailDate = 409;
        public const int DssXmlNotificationEmailTime = 410;
        public const int DssXmlNotificationEmailEmailAddress = 411;
        public const int DssXmlNotificationEmailErrorMessage = 412;
        public const int DssXmlNotificationEmailAppendText = 413;
        public const int DssXmlNotificationEmailTextToAppend = 414;
        public const int DssXmlNotificationEmailSendFailureToEmailAddress = 415;
        public const int DssXmlNotificationEmailLastData = 416;
        public const int DssXmlNotificationMobileModuleReserved = 500;
        public const int DssXmlNotificationMobileEnableEmail = 501;
        public const int DssXmlNotificationMobileRecipientName = 502;
        public const int DssXmlNotificationMobileOnwerName = 503;
        public const int DssXmlNotificationMobileReportOrDocumentName = 504;
        public const int DssXmlNotificationMobileProjectName = 505;
        public const int DssXmlNotificationMobileDeliveryMethod = 506;
        public const int DssXmlNotificationMobileSchedule = 507;
        public const int DssXmlNotificationMobileDeliveryStatus = 508;
        public const int DssXmlNotificationMobileDate = 509;
        public const int DssXmlNotificationMobileTime = 510;
        public const int DssXmlNotificationMobileErrorMessage = 511;
        public const int DssXmlNotificationMobileAppendText = 512;
        public const int DssXmlNotificationMobileTextToAppend = 513;
        public const int DssXmlNotificationMobileSendFailureToEmailAddress = 514;
        public const int DssXmlNotificationMobileLastData = 515;
    }

    public class DSSXMLNotificationModuleID
    {
        public const int DssXmlNotificationModuleIDReserved = 0;
        public const int DssXmlNotificationModuleFile = 1;
        public const int DssXmlNotificationModulePrint = 2;
        public const int DssXmlNotificationModuleHistoryList = 3;
        public const int DssXmlNotificationModuleEmail = 4;
        public const int DssXmlNotificationModuleMobile = 5;
        public const int DssXmlNotificationLastModule = 5;
    }

    public class DSSXMLObjectFlags
    {
        public const int DssXmlObjectDefn = 1;
        public const int DssXmlObjectBrowser = 2;
        public const int DssXmlObjectDates = 4;
        public const int DssXmlObjectComments = 8;
        public const int DssXmlObjectDependents = 16;
        public const int DssXmlObjectProperties = 32;
        public const int DssXmlObjectSecurity = 64;
        public const int DssXmlObjectTotalObject = 127;
        public const int DssXmlObjectChangeJournalComments = 128;
        public const int DssXmlObjectDepnDefn = 256;
        public const int DssXmlObjectDepnBrowser = 512;
        public const int DssXmlObjectDepnDates = 1024;
        public const int DssXmlObjectDepnComments = 2048;
        public const int DssXmlObjectDepnDependents = 4096;
        public const int DssXmlObjectDepnProperties = 8192;
        public const int DssXmlObjectDepnSecurity = 16384;
        public const int DssXmlObjectTotalDepn = 32512;
        public const int DssXmlObjectDoNotCache = 131072;
        public const int DssXmlObjectCheckWebCache = 262144;
        public const int DssXmlObjectUseWebCacheOnly = 524288;
        public const int DssXmlObjectSaveOverwrite = 16777216;
        public const int DssXmlObjectRemoveNonPrimaryNameTranslations = 33554432;
        public const int DssXmlObjectAncestors = 268435456;
        public const int DssXmlObjectFindHidden = 536870912;
        public const int DssXmlObjectPlainText = 1073741824;
    }

    public class DSSXMLObjectLockCommands
    {
        public const int DssXmlLockReserved = 0;
        public const int DssXmlLockObject = 1;
        public const int DssXmlUnlockObject = 2;
        public const int DssXmlQueryObjectLock = 3;
    }

    public class DSSXMLObjectLockFlags
    {
        public const int DssXmlObjectLockReserved = 0;
        public const int DssXmlObjectLockConstituents = 2;
        public const int DssXmlObjectLockPermanent = 4;
    }

    public class DSSXMLObjectSearchStatus
    {
        public const int DssXmlObjectSearchStatusResultXML = 0;
        public const int DssXmlObjectSearchStatusRunning = 1;
    }

    public class DSSXMLObjectState
    {
        public const int DssXmlObjectStateDefnLoaded = 1;
        public const int DssXmlObjectStateBrowserLoaded = 2;
        public const int DssXmlObjectStateDatesLoaded = 4;
        public const int DssXmlObjectStateCommentsLoaded = 8;
        public const int DssXmlObjectStateDependentsLoaded = 16;
        public const int DssXmlObjectStatePropertiesLoaded = 32;
        public const int DssXmlObjectStateSecurityLoaded = 64;
        public const int DssXmlObjectStateTotalObjectLoaded = 127;
        public const int DssXmlObjectStatePersisted = 256;
        public const int DssXmlObjectStateDirty = 512;
        public const int DssXmlObjectStateDeleted = 1024;
        public const int DssXmlObjectStateCorrupted = 2048;
        public const int DssXmlObjectStateDependentsDirty = 4096;
        public const int DssXmlObjectStateMissing = 8192;
        public const int DssXmlObjectStateError = 16384;
        public const int DssXmlObjectStateEmbedded = 32768;
        public const int DssXmlObjectStateContainer = 65536;
        public const int DssXmlObjectStateCached = 131072;
        public const int DssXmlObjectStatePromptsSubstituted = 262144;
        public const int DssXmlObjectStateTemplateAppObjectDirty = 1048576;
        public const int DssXmlObjectStateTemplateContentDirty = 2097152;
        public const int DssXmlObjectStateTemplateStructureDirty = 4194304;
        public const int DssXmlObjectStateTemplateViewDirty = 8388608;
        public const int DssXmlObjectStateAppObjectDirty = 16777216;
        public const int DssXmlObjectDocumentDataSourceDirty = 33554432;
    }

    public class DSSXMLObjectSubTypes
    {
        public const int DssXmlSubTypeUnknown = -1;
        public const int DssXmlSubTypeReserved = 0;
        public const int DssXmlSubTypeFilter = 256;
        public const int DssXmlSubTypeCustomGroup = 257;
        public const int DssXmlSubTypeTemplate = 512;
        public const int DssXmlSubTypeReportGrid = 768;
        public const int DssXmlSubTypeReportGraph = 769;
        public const int DssXmlSubTypeReportEngine = 770;
        public const int DssXmlSubTypeReportText = 771;
        public const int DssXmlSubTypeReportDatamart = 772;
        public const int DssXmlSubTypeReportBase = 773;
        public const int DssXmlSubTypeReportGridAndGraph = 774;
        public const int DssXmlSubTypeReportNonInteractive = 775;
        public const int DssXmlSubTypeReportCube = 776;
        public const int DssXmlSubTypeReportIncrementalRefresh = 777;
        public const int DssXmlSubTypeReportTransaction = 778;
        public const int DssXmlSubTypeMetric = 1024;
        public const int DssXmlSubTypeSubtotalDefinition = 1025;
        public const int DssXmlSubTypeSystemSubtotal = 1026;
        public const int DssXmlSubTypeMetricDMX = 1027;
        public const int DssSubTypeMetricTraining = 1028;
        public const int DssXmlSubTypeStyle = 1536;
        public const int DssXmlSubTypeAggMetric = 1792;
        public const int DssXmlSubTypeFolder = 2048;
        public const int DssXmlSubTypePrompt = 2560;
        public const int DssXmlSubTypePromptBoolean = 2561;
        public const int DssXmlSubTypePromptLong = 2562;
        public const int DssXmlSubTypePromptString = 2563;
        public const int DssXmlSubTypePromptDouble = 2564;
        public const int DssXmlSubTypePromptDate = 2565;
        public const int DssXmlSubTypePromptObjects = 2566;
        public const int DssXmlSubTypePromptElements = 2567;
        public const int DssXmlSubTypePromptExpression = 2568;
        public const int DssXmlSubTypePromptExpressionDraft = 2569;
        public const int DssXmlSubTypePromptDimty = 2570;
        public const int DssXmlSubTypePromptBigDecimal = 2571;
        public const int DssXmlSubTypeFunction = 2816;
        public const int DssXmlSubTypeAttribute = 3072;
        public const int DssXmlSubTypeAttributeRole = 3073;
        public const int DssXmlSubTypeAttributeTransformation = 3074;
        public const int DssXmlSubTypeAttributeAbstract = 3075;
        public const int DssXmlSubTypeFact = 3328;
        public const int DssXmlSubTypeDimensionSystem = 3584;
        public const int DssXmlSubTypeDimensionUser = 3585;
        public const int DssXmlSubTypeDimensionOrdered = 3586;
        public const int DssXmlSubTypeDimensionUserHierarchy = 3587;
        public const int DssXmlSubTypeTable = 3840;
        public const int DssXmlSubTypeTablePartitionMD = 3841;
        public const int DssXmlSubTypeTablePartitionWH = 3842;
        public const int DssXmlSubTypeDatamartReport = 4096;
        public const int DssXmlSubTypeFactGroup = 4352;
        public const int DssXmlSubTypeResolution = 4864;
        public const int DssXmlSubTypeAttributeForm = 5376;
        public const int DssXmlSubTypeFormSystem = 5377;
        public const int DssXmlSubTypeFormNormal = 5378;
        public const int DssXmlSubTypeSchema = 5632;
        public const int DssXmlSubTypeFormat = 5888;
        public const int DssXmlSubTypeCatalog = 6144;
        public const int DssXmlSubTypeCatalogDefn = 6400;
        public const int DssXmlSubTypeColumn = 6656;
        public const int DssXmlSubTypePropertyGroup = 6912;
        public const int DssXmlSubTypePropertySet = 7168;
        public const int DssXmlSubTypeDBRole = 7424;
        public const int DssXmlSubTypeDBLogin = 7680;
        public const int DssXmlSubTypeDBConnection = 7936;
        public const int DssXmlSubTypeProject = 8192;
        public const int DssXmlSubTypeServerDef = 8448;
        public const int DssXmlSubTypeUser = 8704;
        public const int DssXmlSubTypeUserGroup = 8705;
        public const int DssXmlSubTypeConfiguration = 9216;
        public const int DssXmlSubTypeRequest = 9472;
        public const int DssXmlSubTypeSearch = 9984;
        public const int DssXmlSubTypeSearchFolder = 10240;
        public const int DssXmlSubTypeDatamart = 10496;
        public const int DssXmlSubTypeFunctionPackageDefinition = 10752;
        public const int DssXmlSubTypeRole = 11008;
        public const int DssXmlSubTypeRoleTransformation = 11009;
        public const int DssXmlSubTypeSecurityRole = 11264;
        public const int DssXmlSubTypeInBox = 11520;
        public const int DssXmlSubTypeInBoxMsg = 11776;
        public const int DssXmlSubTypeConsolidation = 12032;
        public const int DssXmlSubTypeConsolidationElement = 12288;
        public const int DssXmlSubTypeScheduleEvent = 12544;
        public const int DssXmlSubTypeScheduleObject = 12800;
        public const int DssXmlSubTypeScheduleTrigger = 13056;
        public const int DssXmlSubTypeLink = 13312;
        public const int DssXmlSubTypeDBTable = 13568;
        public const int DssXmlSubTypeDBTablePMT = 13569;
        public const int DssXmlSubTypeTableSource = 13824;
        public const int DssXmlSubTypeDocumentDefinition = 14080;
        public const int DssXmlSubTypeReportWritingDocument = 14081;
        public const int DssXmlSubTypeDrillMap = 14336;
        public const int DssXmlSubTypeDBMS = 14592;
        public const int DssXmlSubTypeMDSecurityFilter = 14848;
        public const int DssXmlSubTypeMonitorPerformance = 15104;
        public const int DssXmlSubTypeMonitorJobs = 15105;
        public const int DssXmlSubTypeMonitorUserConnections = 15106;
        public const int DssXmlSubTypeMonitorDBConnections = 15107;
        public const int DssXmlSubTypePromptAnswer = 15232;
        public const int DssXmlSubTypePromptAnswers = 15360;
        public const int DssXmlSubTypePromptAnswerBoolean = 15233;
        public const int DssXmlSubTypePromptAnswerLong = 15234;
        public const int DssXmlSubTypePromptAnswerString = 15235;
        public const int DssXmlSubTypePromptAnswerDouble = 15236;
        public const int DssXmlSubTypePromptAnswerDate = 15237;
        public const int DssXmlSubTypePromptAnswerObjects = 15238;
        public const int DssXmlSubTypePromptAnswerElements = 15239;
        public const int DssXmlSubTypePromptAnswerExpression = 15240;
        public const int DssXmlSubTypePromptAnswerExpressionDraft = 15241;
        public const int DssXmlSubTypePromptAnswerDimty = 15242;
        public const int DssXmlSubTypePromptAnswerBigDecimal = 15243;
    }

    public class DSSXMLObjectTypes
    {
        public const int DssXmlTypeGeneric = -2;
        public const int DssXmlTypeUnknown = -1;
        public const int DssXmlTypeReserved = 0;
        public const int DssXmlTypeFilter = 1;
        public const int DssXmlTypeTemplate = 2;
        public const int DssXmlTypeReportDefinition = 3;
        public const int DssXmlTypeMetric = 4;
        public const int DssXmlTypeStyle = 6;
        public const int DssXmlTypeAggMetric = 7;
        public const int DssXmlTypeFolder = 8;
        public const int DssXmlTypeSubscriptionDevice = 9;
        public const int DssXmlTypePrompt = 10;
        public const int DssXmlTypeFunction = 11;
        public const int DssXmlTypeAttribute = 12;
        public const int DssXmlTypeFact = 13;
        public const int DssXmlTypeDimension = 14;
        public const int DssXmlTypeTable = 15;
        public const int DssXmlTypeDatamartReport = 16;
        public const int DssXmlTypeFactGroup = 17;
        public const int DssXmlTypeShortcut = 18;
        public const int DssXmlTypeResolution = 19;
        public const int DssXmlTypeMonitor = 20;
        public const int DssXmlTypeAttributeForm = 21;
        public const int DssXmlTypeSchema = 22;
        public const int DssXmlTypeFormat = 23;
        public const int DssXmlTypeCatalog = 24;
        public const int DssXmlTypeCatalogDefn = 25;
        public const int DssXmlTypeColumn = 26;
        public const int DssXmlTypePropertySet = 28;
        public const int DssXmlTypeDBRole = 29;
        public const int DssXmlTypeDBLogin = 30;
        public const int DssXmlTypeDBConnection = 31;
        public const int DssXmlTypeProject = 32;
        public const int DssXmlTypeServerDef = 33;
        public const int DssXmlTypeUser = 34;
        public const int DssXmlTypeConfiguration = 36;
        public const int DssXmlTypeRequest = 37;
        public const int DssXmlTypeScript = 38;
        public const int DssXmlTypeSearch = 39;
        public const int DssXmlTypeSearchFolder = 40;
        public const int DssXmlTypeDatamart = 41;
        public const int DssXmlTypeFunctionPackageDefinition = 42;
        public const int DssXmlTypeRole = 43;
        public const int DssXmlTypeSecurityRole = 44;
        public const int DssXmlTypeLocale = 45;
        public const int DssXmlTypeConsolidation = 47;
        public const int DssXmlTypeConsolidationElement = 48;
        public const int DssXmlTypeScheduleEvent = 49;
        public const int DssXmlTypeScheduleObject = 50;
        public const int DssXmlTypeScheduleTrigger = 51;
        public const int DssXmlTypeLink = 52;
        public const int DssXmlTypeDbTable = 53;
        public const int DssXmlTypeTableSource = 54;
        public const int DssXmlTypeDocumentDefinition = 55;
        public const int DssXmlTypeDrillMap = 56;
        public const int DssXmlTypeDBMS = 57;
        public const int DssXmlTypeMDSecurityFilter = 58;
        public const int DssXmlTypePromptAnswer = 59;
        public const int DssXmlTypePromptAnswers = 60;
        public const int DssXmlTypeReservedLastOne = 61;
    }

    public class DSSXMLOrder
    {
        public const int DssXmlAscending = 0;
        public const int DssXmlDescending = 1;
    }

    public class DSSXMLOrderParentFirst
    {
        public const int DssXmlOrderParentFirstReserved = 0;
        public const int DssXmlOrderParentFirstDefault = 1;
        public const int DssXmlOrderParentFirstTrue = 2;
        public const int DssXmlOrderParentFirstFalse = 3;
    }

    public class DSSXMLOuterJoinTypes
    {
        public const int DssXmlOuterJoinTypeReserved = 0;
        public const int DssXmlOuterJoinTypeDefault = 1;
        public const int DssXmlOuterJoinTypeEqui = 2;
        public const int DssXmlOuterJoinTypeOuter = 3;
    }

    public class DSSXMLParetoMinorTypes
    {
        public const int DssXmlParetoMinorType = 1;
        public const int DssXmlParetoMinorTypePercent = 2;
    }

    public class DSSXMLParserActionFlags
    {
        public const int DssXmlParserActionNone = 0;
        public const int DssXmlParserActionNewObject = 1;
        public const int DssXmlParserActionOpenForEdit = 2;
        public const int DssXmlParserActionValidate = 4;
        public const int DssXmlParserActionSave = 8;
        public const int DssXmlParserActionSaveAs = 16;
        public const int DssXmlParserActionOverwrite = 32;
    }

    public class DSSXMLParserOutFlags
    {
        public const int DssXmlParserOutNone = 0;
        public const int DssXmlParserOutMetricObjectInfo = 1;
        public const int DssXmlParserOutMetricProperties = 2;
        public const int DssXmlParserOutMetricHeaderFormat = 4;
        public const int DssXmlParserOutMetricGridFormat = 8;
        public const int DssXmlParserOutMetricSubtotals = 16;
        public const int DssXmlParserOutMetricExtendedProperties = 32;
        public const int DssXmlParserOutTotalMetric = 65535;
        public const int DssXmlParserOutTokenStream = 65536;
        public const int DssXmlParserOutLocalSymbolFolder = 131072;
        public const int DssXmlParserOutSearchResultsFolder = 262144;
    }

    public class DSSXMLParserResultFlags
    {
        public const int DssXmlParserResultDefault = 0;
        public const int DssXmlParserResultIsSmartMustBeFalse = 1;
    }

    public class DSSXMLPartialType
    {
        public const int DssXmlPartialTypeFF = 1;
        public const int DssXmlPartialTypeFP = 2;
        public const int DssXmlPartialTypePF = 3;
        public const int DssXmlPartialTypePP = 4;
        public const int DssXmlPartialTypeDefault = 5;
    }

    public class DSSXMLPerformanceMonitorFlags
    {
        public const int DssXMlPerformancdMonitorDefault = 0;
        public const int DssXmlperformanceMonitroLogToStats = 1;
    }

    public class DSSXMLPieMinorTypes
    {
        public const int DssXmlPieMinorTypePies = 1;
        public const int DssXmlPieMinorTypeRingPie = 2;
        public const int DssXmlPieMinorTypeMultiplePies = 4;
        public const int DssXmlPieMinorTypeMultipleRingPie = 8;
        public const int DssXmlPieMinorTypeMultipleProp = 16;
        public const int DssXmlPieMinorTypeMulPropRingPie = 32;
    }

    public class DSSXMLPolarMinorTypes
    {
        public const int DssXmlPolarMinorTypePolarCoord = 1;
        public const int DssXmlPolarMinorTypeDualAxisPolar = 2;
    }

    public class DSSXMLPrivilegeCategoryTypes
    {
        public const int DssXmlPrivilegeCategoryWebReporter = 1;
        public const int DssXmlPrivilegeCategoryWebAnalyst = 2;
        public const int DssXmlPrivilegeCategoryWebProfessional = 3;
        public const int DssXmlPrivilegeCategoryWebMMT = 4;
        public const int DssXmlPrivilegeCategoryCommonPrivileges = 5;
        public const int DssXmlPrivilegeCategoryOffice = 6;
        public const int DssXmlPrivilegeCategoryMobile = 7;
        public const int DssXmlPrivilegeCategoryDistributionServices = 8;
        public const int DssXmlPrivilegeCategoryMultisource = 9;
        public const int DssXmlPrivilegeCategoryDesktopAnalyst = 10;
        public const int DssXmlPrivilegeCategoryDesktopDesigner = 11;
        public const int DssXmlPrivilegeCategoryArchitect = 12;
        public const int DssXmlPrivilegeCategoryObjectManager = 13;
        public const int DssXmlPrivilegeCategoryCommandManager = 14;
        public const int DssXmlPrivilegeCategoryIntegrityManager = 15;
        public const int DssXmlPrivilegeCategoryAdministration = 16;
    }

    public class DSSXMLPrivilegeTypes
    {
        public const int DssXmlPrivilegesReserved = 0;
        public const int DssXmlPrivilegesCreateAppObj = 1;
        public const int DssXmlPrivilegesCreateConfigObj = 2;
        public const int DssXmlPrivilegesCreateSchemaObj = 3;
        public const int DssXmlPrivilegesScheduleRequest = 4;
        public const int DssXmlPrivilegesUseArchitect = 5;
        public const int DssXmlPrivilegesWebReportDetails = 6;
        public const int DssXmlPrivilegesUseServerAdmin = 7;
        public const int DssXmlPrivilegesUseMetricEditor = 8;
        public const int DssXmlPrivilegesUseFilterEditor = 9;
        public const int DssXmlPrivilegesUseTemplateEditor = 10;
        public const int DssXmlPrivilegesUseReportEditor = 11;
        public const int DssXmlPrivilegesUseConsolidationEditor = 12;
        public const int DssXmlPrivilegesUseCustomGroupEditor = 13;
        public const int DssXmlPrivilegesUseDocumentEditor = 14;
        public const int DssXmlPrivilegesUsePromptEditor = 15;
        public const int DssXmlPrivilegesBypassAccessChecks = 16;
        public const int DssXmlPrivilegesWebAdministrator = 17;
        public const int DssXmlPrivilegesWebUser = 18;
        public const int DssXmlPrivilegesWebViewHistoryList = 19;
        public const int DssXmlPrivilegesWebReportManipulations = 20;
        public const int DssXmlPrivilegesWebCreateNewReport = 21;
        public const int DssXmlPrivilegesWebObjectSearch = 22;
        public const int DssXmlPrivilegesWebChangeUserOptions = 23;
        public const int DssXmlPrivilegesWebSaveReport = 24;
        public const int DssXmlPrivilegesWebAdvancedDrilling = 25;
        public const int DssXmlPrivilegesWebExport = 26;
        public const int DssXmlPrivilegesWebPrintMode = 27;
        public const int DssXmlPrivilegesWebManageObjects = 28;
        public const int DssXmlPrivilegesWebPublish = 29;
        public const int DssXmlPrivilegesUseServerCache = 30;
        public const int DssXmlPrivilegesWebReportSQL = 31;
        public const int DssXmlPrivilegesReserved2 = 32;
        public const int DssXmlPrivilegesCreateDatamart = 33;
        public const int DssXmlPrivilegesUseDatamartEditor = 34;
        public const int DssXmlPrivilegesUseObjectManager = 35;
        public const int DssXmlPrivilegesWebAddToHistoryList = 36;
        public const int DssXmlPrivilegesWebChangeViewMode = 37;
        public const int DssXmlPrivilegesWebNormalDrilling = 38;
        public const int DssXmlPrivilegesWebDrillOnMetrics = 39;
        public const int DssXmlPrivilegesWebFormatGridAndGraph = 40;
        public const int DssXmlPrivilegesWebScheduleReport = 41;
        public const int DssXmlPrivilegesWebSimultaneousExecution = 42;
        public const int DssXmlPrivilegesWebSwitchPageByElements = 43;
        public const int DssXmlPrivilegesWebSort = 44;
        public const int DssXmlPrivilegesUseVLDBPropertyEditor = 45;
        public const int DssXmlPrivilegesWebSaveTemplatesAndFilters = 46;
        public const int DssXmlPrivilegesWebFilterOnSelections = 47;
        public const int DssXmlPrivilegesScheduleAdministration = 48;
        public const int DssXmlPrivilegesWebScheduleEMail = 49;
        public const int DssXmlPrivilegesUseUserManager = 50;
        public const int DssXmlPrivilegesUseJobMonitor = 51;
        public const int DssXmlPrivilegesUseUserConnectionMonitor = 52;
        public const int DssXmlPrivilegesUseDatabaseConnectionMonitor = 53;
        public const int DssXmlPrivilegesUseScheduleMonitor = 54;
        public const int DssXmlPrivilegesUseCacheMonitor = 55;
        public const int DssXmlPrivilegesUseClusterMonitor = 56;
        public const int DssXmlPrivilegesUseDatabaseInstanceManager = 57;
        public const int DssXmlPrivilegesUseScheduleManager = 58;
        public const int DssXmlPrivilegesUseProjectMonitor = 59;
        public const int DssXmlPrivilegesWebSendNow = 60;
        public const int DssXmlPrivilegesModifySchemaObjects = 61;
        public const int DssXmlPrivilegesViewETLInformation = 62;
        public const int DssXmlPrivilegesUseReportObjectsWindow = 63;
        public const int DssXmlPrivilegesUseThresholdsEditor = 64;
        public const int DssXmlPrivilegesUseFormattingEditor = 65;
        public const int DssXmlPrivilegesSaveCustomAutoStyle = 66;
        public const int DssXmlPrivilegesUseReportFilterEditor = 67;
        public const int DssXmlPrivilegesCreateDerivedMetrics = 68;
        public const int DssXmlPrivilegesModifySubtotals = 69;
        public const int DssXmlPrivilegesModifyReportObjectList = 70;
        public const int DssXmlPrivilegesWebCreateDerivedMetrics = 71;
        public const int DssXmlPrivilegesWebModifySubtotals = 72;
        public const int DssXmlPrivilegesWebModifyReportList = 73;
        public const int DssXmlPrivilegesWebUseReportObjectsWindow = 74;
        public const int DssXmlPrivilegesWebUseReportFilterEditor = 75;
        public const int DssXmlPrivilegesWebUseDesignMode = 76;
        public const int DssXmlPrivilegesWebAliasObjects = 77;
        public const int DssXmlPrivilegesWebConfigureToolbars = 78;
        public const int DssXmlPrivilegesWebUseQueryFilterEditor = 79;
        public const int DssXmlPrivilegesWebReExecuteReportAgainstWH = 80;
        public const int DssXmlPrivilegesChooseAttributeFormDisplay = 81;
        public const int DssXmlPrivilegesUseHistoryList = 82;
        public const int DssXmlPrivilegesConfigureToolbars = 83;
        public const int DssXmlPrivilegesChangeUserPreferences = 84;
        public const int DssXmlPrivilegesUseReportDataOptions = 85;
        public const int DssXmlPrivilegesUseDataExplorer = 86;
        public const int DssXmlPrivilegesFormatGraph = 87;
        public const int DssXmlPrivilegesModifySorting = 88;
        public const int DssXmlPrivilegesViewSQL = 89;
        public const int DssXmlPrivilegesCreateNewFolder = 90;
        public const int DssXmlPrivilegesPivotReport = 91;
        public const int DssXmlPrivilegesUseDesignMode = 92;
        public const int DssXmlPrivilegesAliasObjects = 93;
        public const int DssXmlPrivilegesUseGridOptions = 94;
        public const int DssXmlPrivilegesUseSearchEditor = 95;
        public const int DssXmlPrivilegesReExecuteReportAgainstWH = 96;
        public const int DssXmlPrivilegesUseDrillMapEditor = 97;
        public const int DssXmlPrivilegesSendToEMail = 98;
        public const int DssXmlPrivilegesUseFunctionPlugInEditor = 99;
        public const int DssXmlPrivilegesWebSimpleGraphFormatting = 100;
        public const int DssXmlPrivilegesWebUseLockedHeaders = 101;
        public const int DssXmlPrivilegesWebSetColumnWidths = 102;
        public const int DssXmlPrivilegesUseSecurityFilterManager = 103;
        public const int DssXmlPrivilegesUseProjectStatusEditor = 104;
        public const int DssXmlPrivilegesUseProjectDocumentation = 105;
        public const int DssXmlPrivilegesWebExecuteDatamartReport = 106;
        public const int DssXmlPrivilegesUseSubtotalEditor = 107;
        public const int DssXmlPrivilegesUseFindAndReplaceDialog = 108;
        public const int DssXmlPrivilegesUseCommandManager = 109;
        public const int DssXmlPrivilegesUseRWDocumentEditor = 110;
        public const int DssXmlPrivilegesExecuteRWDocument = 111;
        public const int DssXmlPrivilegesWebExecuteRWDocument = 112;
        public const int DssXmlPrivilegesUseOfficeIntelligence = 113;
        public const int DssXmlPrivilegesWebCreateEmailAddress = 114;
        public const int DssXmlPrivilegesWebPrintNow = 115;
        public const int DssXmlPrivilegesWebScheduledPrinting = 116;
        public const int DssXmlPrivilegesWebCreatePrintLocation = 117;
        public const int DssXmlPrivilegesWebExportToFileNow = 118;
        public const int DssXmlPrivilegesWebScheduledExportToFile = 119;
        public const int DssXmlPrivilegesWebCreateFileLocation = 120;
        public const int DssXmlPrivilegesServerMonitoring = 121;
        public const int DssXmlPrivilegesDefineFreeformSQLReport = 122;
        public const int DssXmlPrivilegesDefineOLAPCubeReport = 123;
        public const int DssXmlPrivilegesImportOLAPCube = 124;
        public const int DssXmlPrivilegesWebDocumentDesign = 125;
        public const int DssXmlPrivilegesWebManageDocumentDatasets = 126;
        public const int DssXmlPrivilegesWebModifyUnitInGridInDocument = 127;
        public const int DssXmlPrivilegesWebChooseAttributeFormDisplay = 128;
        public const int DssXmlPrivilegesWebExecuteBulkExport = 129;
        public const int DssXmlPrivilegesWebNumberFormatting = 130;
        public const int DssXmlPrivilegesUseDesktop = 131;
        public const int DssXmlPrivilegesCreateShortcut = 132;
        public const int DssXmlPrivilegesWebEnableMMTAccess = 133;
        public const int DssXmlPrivilegesDrillWithIntelligentCube = 134;
        public const int DssXmlPrivilegesDefineQueryBuilderReport = 135;
        public const int DssXmlPrivilegesWebDefineQueryBuilderReport = 136;
        public const int DssXmlPrivilegesUseMstrMobile = 137;
        public const int DssXmlPrivilegesMobileViewDocument = 138;
        public const int DssXmlPrivilegesUseIntegrityManager = 139;
        public const int DssXmlPrivilegesCacheAdministration = 140;
        public const int DssXmlPrivilegesUseInboxMonitor = 141;
        public const int DssXmlPrivilegesInboxAdministration = 142;
        public const int DssXmlPrivilegesUseCubeMonitor = 143;
        public const int DssXmlPrivilegesCubeAdministration = 144;
        public const int DssXmlPrivilegesFireEvent = 145;
        public const int DssXmlPrivilegesUseTransmitterDeviceManager = 146;
        public const int DssXmlPrivilegesUseContactManager = 147;
        public const int DssXmlPrivilegesUseSecurityRoleManager = 148;
        public const int DssXmlPrivilegesAssignSecurityRoles = 149;
        public const int DssXmlPrivilegesConfigureContactDataProfile = 150;
        public const int DssXmlPrivilegesResetUserPasswords = 151;
        public const int DssXmlPrivilegesLinkToExternalAccounts = 152;
        public const int DssXmlPrivilegesGrantPrivilege = 153;
        public const int DssXmlPrivilegesConfigureGroupMembership = 154;
        public const int DssXmlPrivilegesUseDatabaseLoginManager = 155;
        public const int DssXmlPrivilegesDefineSecurityFilter = 156;
        public const int DssXmlPrivilegesClusterAdministration = 157;
        public const int DssXmlPrivilegesLoadProject = 158;
        public const int DssXmlPrivilegesIdleProject = 159;
        public const int DssXmlPrivilegesDefineViewReport = 160;
        public const int DssXmlPrivilegesUseCubeReportEditor = 161;
        public const int DssXmlPrivilegesExecuteCubeReport = 162;
        public const int DssXmlPrivilegesWebDefineViewReport = 163;
        public const int DssXmlPrivilegesWebExecuteCubeReport = 164;
        public const int DssXmlPrivilegesConfigureServerBasic = 165;
        public const int DssXmlPrivilegesConfigureStatistics = 166;
        public const int DssXmlPrivilegesConfigureSecurity = 167;
        public const int DssXmlPrivilegesConfigureInbox = 168;
        public const int DssXmlPrivilegesConfigureGoverning = 169;
        public const int DssXmlPrivilegesConfigureCaches = 170;
        public const int DssXmlPrivilegesConfigureProjectBasic = 171;
        public const int DssXmlPrivilegesConfigureConnectionMap = 172;
        public const int DssXmlPrivilegesConfigureProjectDataSource = 173;
        public const int DssXmlPrivilegesEnableUser = 174;
        public const int DssXmlPrivilegesConfigureSubscriptionSettings = 175;
        public const int DssXmlPrivilegesConfigureAuditing = 176;
        public const int DssXmlPrivilegesConfigureLanguageSettings = 177;
        public const int DssXmlPrivilegesMonitorChangeJournal = 178;
        public const int DssXmlPrivilegesUseBulkTranslationTool = 179;
        public const int DssXmlPrivilegesUseBulkExportEditor = 180;
        public const int DssXmlPrivilegesJobAdministration = 181;
        public const int DssXmlPrivilegesUserConnectionAdministration = 182;
        public const int DssXmlPrivilegesDatabaseConnectionAdministration = 183;
        public const int DssXmlPrivilegesDuplicateProject = 184;
        public const int DssXmlPrivilegesSubscribeOthers = 185;
        public const int DssXmlPrivilegesSubscribeToEmailAlert = 186;
        public const int DssXmlPrivilegesSendLinkInEmail = 187;
        public const int DssXmlPrivilegesExportToPDF = 188;
        public const int DssXmlPrivilegesExportToHTML = 189;
        public const int DssXmlPrivilegesExportToExcel = 190;
        public const int DssXmlPrivilegesExportToText = 191;
        public const int DssXmlPrivilegesExportToFlash = 192;
        public const int DssXmlPrivilegesViewNotes = 193;
        public const int DssXmlPrivilegesAddNotes = 194;
        public const int DssXmlPrivilegesEditNotes = 195;
        public const int DssXmlPrivilegesUseDistributionServices = 196;
        public const int DssXmlPrivilegesUseImmediateDelivery = 197;
        public const int DssXmlPrivilegesEnableAdministrationFromWeb = 198;
        public const int DssXmlPrivilegesUseTranslationEditor = 199;
        public const int DssXmlPrivilegesWebUsePromptEditor = 200;
        public const int DssXmlPrivilegesCreateHTMLContainer = 201;
        public const int DssXmlPrivilegesImportTableFromMultipleSources = 202;
        public const int DssXmlPrivilegesExecuteMultipleSourceReport = 203;
        public const int DssXmlPrivilegesSavePersonalPromptAnswers = 204;
        public const int DssXmlPrivilegesWebDefineDerivedElements = 205;
        public const int DssXmlPrivilegesWebUseBasicThresholdEditor = 206;
        public const int DssXmlPrivilegesWebDashboardDesign = 207;
        public const int DssXmlPrivilegesWebSaveDerivedElements = 208;
        public const int DssXmlPrivilegesWebUseAdvancedThresholdEditor = 209;
        public const int DssXmlPrivilegesWebEditReportLinks = 210;
        public const int DssXmlPrivilegesDefineDerivedElements = 211;
        public const int DssXmlPrivilegesDashboardDesign = 212;
        public const int DssXmlPrivilegesSaveDerivedElements = 213;
        public const int DssXmlPrivilegesEditReportLinks = 214;
        public const int DssXmlPrivilegesWebEditNotes = 215;
        public const int DssXmlPrivilegesDrillAndLink = 216;
        public const int DssXmlPrivilegesWebDefineAdvancedReportOptions = 217;
        public const int DssXmlPrivilegesWebCreateHTMLContainer = 218;
        public const int DssXmlPrivilegesUseOLAPServices = 219;
        public const int DssXmlPrivilegesUseDynamicSourcing = 220;
        public const int DssXmlPrivilegesUseSQLStatementsTab = 221;
        public const int DssXmlPrivilegesWebUseCustomGroupEditor = 222;
        public const int DssXmlPrivilegesWebImportData = 223;
        public const int DssXmlPrivilegesWebImportDatabase = 224;
        public const int DssXmlPrivilegesDefineTransactionReport = 225;
        public const int DssXmlPrivilegesWebConfigureTransaction = 226;
        public const int DssXmlPrivilegesExecuteTransaction = 227;
        public const int DssXmlPrivilegesWebVisualExploration = 228;
        public const int DssXmlPrivilegesUseObjectManagerReadOnly = 229;
        public const int DssXmlPrivilegesUseObjectManagerImportOnly = 230;
        public const int DssXmlPrivilegesUseTranslationEditorBypass = 231;
        public const int DssXmlPrivilegesWebUseObjectSharingEditor = 232;
        public const int DssXmlPrivilegesWebCreateAnalysis = 233;
        public const int DssXmlPrivilegesWebSaveAnalysis = 234;
        public const int DssXmlPrivilegesPrintFromDevice = 235;
        public const int DssXmlPrivilegesEmailScreenFromDevice = 236;
        public const int DssXmlPrivilegesWebCreateDynamicAddressList = 237;
        public const int DssXmlPrivilegesWebSubscribeDynamicAddressList = 238;
        public const int DssXmlPrivilegesUseSendPreviewNow = 239;
        public const int DssXmlPrivilegesWebUseMetricEditor = 240;
        public const int DssXmlPrivilegesWebExecuteAnalysis = 241;
        public const int DssXmlPrivilegesMobileViewAnalysis = 242;
        public const int DssXmlPrivilegesMobileSaveDocument = 243;
        public const int DssXmlPrivilegesMobileSaveAnalysis = 244;
        public const int DssXmlPrivilegesMobileCreateDocument = 245;
        public const int DssXmlPrivilegesMobileCreateAnalysis = 246;
        public const int DssXmlPrivilegesMobileSaveReport = 247;
        public const int DssXmlPrivilegesMobilePublish = 248;
        public const int DssXmlPrivilegesMobileEditAnalysis = 249;
        public const int DssXmlPrivilegesMaximum = 250;
    }

    public class DSSXMLPrivilegeUpdateTypes
    {
        public const int DssXmlPrivilegeUpdateNone = 0;
        public const int DssPrivilegeUpdateIntroducedIn90 = 1;
        public const int DssPrivilegeUpdateIntroducedIn901 = 2;
        public const int DssPrivilegeUpdateIntroducedIn902 = 4;
        public const int DssPrivilegeUpdateIntroducedIn921 = 8;
        public const int DssPrivilegeUpdateAll = -1;
    }

    public class DSSXMLProductLicenseStatus
    {
        public const int DssXmlProductLicenseStatusFullyActive = 0;
        public const int DssXmlProductLicenseStatusNotActiveNotExpired = 1;
        public const int DssXmlProductLicenseStatusNotActiveExpired = -1;
        public const int DssXmlProductLicenseStatusEvaluationNotExpired = 2;
        public const int DssXmlProductLicenseStatusEvaluationExpired = -2;
        public const int DssXmlProductLicenseStatusNotLicensed = -3;
    }

    public class DSSXMLProjectActions
    {
        public const int DssXmlOpenProject = 0;
        public const int DssXmlCloseProject = 1;
        public const int DssXmlIdleProject = 2;
        public const int DssXmlResumeProject = 3;
    }

    public class DSSXMLProjectConfigurationSettingType
    {
        public const int DssXmlProjectConfigurationSettingTypeProjectSettings = -1;
        public const int DssXmlProjectConfigurationSettingTypeReserved = 0;
        public const int DssXmlProjectConfigurationSettingTypeStatistics = 1;
        public const int DssXmlProjectConfigurationSettingTypeDeliveryNotification = 2;
        public const int DssXmlProjectConfigurationSettingTypeStatisticsTable = 3;
    }

    public class DSSXMLProjectLoadOption
    {
        public const int DssXmlProjectLoadActive = 0;
        public const int DssXmlProjectLoadFullIdle = 1;
        public const int DssXmlProjectLoadOffline = 2;
        public const int DssProjectLoadAsynch = 4;
    }

    public class DSSXMLProjectStatus
    {
        public const int DssXmlProjectStatusOffline = -1;
        public const int DssXmlProjectStatusOfflinePending = -2;
        public const int DssXmlProjectStatusErrorState = -3;
        public const int DssXmlProjectStatusOnlinePending = -4;
        public const int DssXmlProjectStatusActive = 0;
        public const int DssXmlProjectStatusExecIdle = 1;
        public const int DssXmlProjectStatusRequestIdle = 2;
        public const int DssXmlProjectStatusMetadataIdle = 1;
        public const int DssXmlProjectStatusWarehouseIdle = 1;
        public const int DssXmlProjectStatusWHExecIdle = 4;
        public const int DssXmlProjectStatusFullIdle = 7;
    }

    public class DSSXMLPromptAnswerReuse
    {
        public const int DssXmlPromptAnswerReuseReserved = 0;
        public const int DssXmlPromptAnswerReuseList = 1;
        public const int DssXmlPromptAnswerReuseDefault = 2;
        public const int DssXmlPromptAnswerReuseDefaultAndClose = 3;
    }

    public class DSSXMLPromptReuse
    {
        public const int DssXmlReuseReserved = 0;
        public const int DssXmlReuseCancel = 1;
        public const int DssXmlReuseDefault = 3;
        public const int DssXmlReusePrevious = 7;
        public const int DssXmlReuseBreak = 9;
        public const int DssXmlReuseDefaultOrBreak = 11;
        public const int DssXmlReusePreviousOrBreak = 15;
        public const int DssXmlReuseAutoCancel = 17;
        public const int DssXmlReuseAutoDefault = 19;
        public const int DssXmlReuseAutoBreak = 25;
        public const int DssXmlReuseAutoDefaultOrBreak = 27;
        public const int DssXmlReuseAutoClose = 49;
        public const int DssXmlReuseAutoCloseOrBreak = 57;
        public const int DssXmlReuseForceAutoClose = 113;
    }

    public class DSSXMLPromptType
    {
        public const int DssXmlPromptBoolean = 1;
        public const int DssXmlPromptLong = 2;
        public const int DssXmlPromptString = 3;
        public const int DssXmlPromptDouble = 4;
        public const int DssXmlPromptDate = 5;
        public const int DssXmlPromptObjects = 6;
        public const int DssXmlPromptElements = 7;
        public const int DssXmlPromptExpression = 8;
        public const int DssXmlPromptExpressionDraft = 9;
        public const int DssXmlPromptFilterDraft = 10;
        public const int DssXmlPromptMetricDraft = 11;
        public const int DssXmlPromptTemplateDraft = 12;
        public const int DssXmlPromptDimty = 13;
        public const int DssXmlPromptSort = 14;
        public const int DssXmlPromptTemplateSubtotal = 15;
        public const int DssXmlPromptBigDecimal = 16;
    }

    public class DSSXMLPropertyFlags
    {
        public const int DssXmlPropertyReserved = 0;
        public const int DssXmlPropertyNotDefault = 1;
        public const int DssXmlPropertyReportID = 2;
        public const int DssXmlPropertyTemplateID = 4;
    }

    public class DSSXMLPropertyGroupTypes
    {
        public const int DssXmlPropertyGroupReserved = 0;
        public const int DssXmlPropertyGroupFilter = 1;
        public const int DssXmlPropertyGroupTemplate = 2;
        public const int DssXmlPropertyGroupReport = 4;
        public const int DssXmlPropertyGroupMetric = 8;
        public const int DssXmlPropertyGroupColumn = 16;
        public const int DssXmlPropertyGroupAggMetric = 32;
        public const int DssXmlPropertyGroupFolder = 64;
        public const int DssXmlPropertyGroupUser = 128;
        public const int DssXmlPropertyGroupPrompt = 256;
        public const int DssXmlPropertyGroupFunction = 512;
        public const int DssXmlPropertyGroupAttribute = 1024;
        public const int DssXmlPropertyGroupFact = 2048;
        public const int DssXmlPropertyGroupFactGroup = 2048;
        public const int DssXmlPropertyGroupDimension = 4096;
        public const int DssXmlPropertyGroupTable = 8192;
        public const int DssXmlPropertyGroupFormat = 16384;
        public const int DssXmlPropertyGroupShortcut = 32768;
        public const int DssXmlPropertyGroupResolution = 65536;
        public const int DssXmlPropertyGroupSearch = 131072;
        public const int DssXmlPropertyGroupProject = 262144;
        public const int DssXmlPropertyGroupDBRole = 524288;
        public const int DssXmlPropertyGroupConsolidationElement = 1048576;
        public const int DssXmlPropertyGroupAttributeForm = 2097152;
        public const int DssXmlPropertyGroupDBMS = 4194304;
        public const int DssXmlPropertyGroupMDSecurityFilter = 8388608;
        public const int DssXmlPropertyGroupSchedule = 16777216;
        public const int DssXmlPropertyGroupDatamart = 33554432;
        public const int DssXmlPropertyGroupDocument = 67108864;
        public const int DssXmlPropertyGroupGeneralObjects = -2147483648;
        public const int DssXmlPropertyGroupAll = -1;
    }

    public class DSSXMLPropertyXmlFlags
    {
        public const int DssPropertyXmlReserved = 0;
        public const int DssPropertyXmlPropID = 1;
        public const int DssPropertyXmlPropName = 2;
        public const int DssPropertyXmlPropType = 4;
        public const int DssPropertyXmlPropValue = 8;
        public const int DssPropertyXmlPropDefault = 16;
        public const int DssPropertyXmlPropSetID = 32;
        public const int DssPropertyXmlPropUseDefault = 64;
        public const int DssPropertyXmlPropTargetFrom = 128;
        public const int DssPropertyXmlPropIndex = 256;
        public const int DssPropertyXmlPropDescription = 512;
        public const int DssPropertyXmlPropAll = 4095;
        public const int DssPropertyXmlSetID = 4096;
        public const int DssPropertyXmlSetName = 8192;
        public const int DssPropertyXmlSetChildCount = 16384;
        public const int DssPropertyXmlSetPartialChildCount = 32768;
        public const int DssPropertyXmlSetAll = 1044480;
        public const int DssPropertyXmlAll = 1048575;
        public const int DssPropertyXmlNonDefaultOnly = -2147483648;
        public const int DssPropertyXmlNonDefaultOnly2 = 1073741824;
        public const int DssPropertyXmlIgnoreMissing = 536870912;
        public const int DssPropertyXmlIgnoreAssignedDefault = 268435456;
        public const int DssPropertyXmlNameOneTime = 1048576;
    }

    public class DSSXMLPurgeFlag
    {
        public const int DssXmlPurgeAllCache = 0;
        public const int DssXmlPurgeReportCache = 1;
        public const int DssXmlPurgeObjectCache = 2;
        public const int DssXmlPurgeElementCache = 4;
    }

    public class DSSXMLRadarMinorTypes
    {
        public const int DssXmlRadarMinorTypeLine = 1;
        public const int DssXmlRadarMinorTypeStackedArea = 2;
        public const int DssXmlRadarMinorTypeDALine = 4;
        public const int DssXmlRadarMinorTypeDAStackedArea = 8;
        public const int DssXmlRadarMinorTypeArea = 16;
        public const int DssXmlRadarMinorTypeAreaStacked = 32;
    }

    public class DSSXmlReportCacheDisableOptions
    {
        public const int DssXmlReportCacheDisableNone = 0;
        public const int DssXmlReportCacheDisableAll = 1;
        public const int DssXmlReportCacheDisablePromptReport = 2;
        public const int DssXmlReportCacheDisableNonPromptReport = 4;
        public const int DssXmlReportCacheDisableXMLCache = 8;
    }

    public class DSSXMLReportCacheStatus
    {
        public const int DssXmlReportCacheStatusReserved = 0;
        public const int DssXmlReportCacheStatusReady = 1;
        public const int DssXmlReportCacheStatusProcessing = 2;
        public const int DssXmlReportCacheStatusInvalid = 4;
        public const int DssXmlReportCacheStatusExpired = 8;
        public const int DssXmlReportCacheStatusLoaded = 16;
        public const int DssXmlReportCacheStatusUpdated = 32;
        public const int DssXmlReportCacheStatusDirty = 64;
        public const int DssXmlReportCacheStatusWaitToRemove = 128;
        public const int DssXmlReportCacheStatusFiled = 256;
        public const int DssXmlReportCacheStatusResultDirty = 512;
        public const int DssXmlReportCacheStatusDeleted = 1024;
        public const int DssXmlReportCacheStatusXMLCacheDirty = 2048;
        public const int DssXmlReportCacheStatusProcessPending = 4096;
        public const int DssXmlReportCacheStatusUpdating = 8192;
    }

    public class DSSXMLReportCacheType
    {
        public const int DssXmlMatchingCache = 1;
        public const int DssXmlResultCache = 2;
        public const int DssXmlXMLCache = 4;
        public const int DssXmlForeignCache = 8;
    }

    public class DSSXMLReportFilter
    {
        public const int DssXmlReportFilterIgnore = 1;
        public const int DssXmlReportFilterApply = 2;
        public const int DssXmlReportFilterSmart = 3;
    }

    public class DSSXMLReportManipulationMethod
    {
        public const int DssXmlReportManipulationAddUnit = 1;
        public const int DssXmlReportManipulationAddMetric = 2;
        public const int DssXmlReportManipulationRemoveUnit = 3;
        public const int DssXmlReportManipulationRemoveBaseUnit = 4;
        public const int DssXmlReportManipulationSetFilter = 5;
        public const int DssXmlReportManipulationSetBaseFilter = 6;
        public const int DssXmlReportManipulationPivotUnit = 7;
        public const int DssXmlReportManipulationClearTemplate = 8;
        public const int DssXmlReportManipulationPageBy = 9;
        public const int DssXmlReportManipulationFormatAxis = 10;
        public const int DssXmlReportManipulationFormatUnit = 11;
        public const int DssXmlReportManipulationAxisSubtotals = 12;
        public const int DssXmlReportManipulationAxisSubtotalsObject = 13;
        public const int DssXmlReportManipulationUnitSubtotals = 14;
        public const int DssXmlReportManipulationUnitSubtotalsObject = 15;
        public const int DssXmlReportManipulationRemoveSubtotals = 16;
        public const int DssXmlReportManipulationAddDerivedMetric = 17;
        public const int DssXmlReportManipulationUpdateDerivedMetric = 18;
        public const int DssXmlReportManipulationConvertIntoDerivedMetric = 19;
        public const int DssXmlReportManipulationClearUnit = 20;
        public const int DssXmlReportManipulationLockUnit = 21;
        public const int DssXmlReportManipulationUnlockUnit = 22;
        public const int DssXmlReportManipulationAddPercentToTotalMetric = 23;
        public const int DssXmlReportManipulationAddRankMetric = 24;
        public const int DssXmlReportManipulationAddTransformationMetric = 25;
        public const int DssXmlReportManipulationSort = 26;
        public const int DssXmlReportManipulationRemoveSort = 27;
        public const int DssXmlReportManipulationChangeUnitDisplayName = 28;
        public const int DssXmlReportManipulationSetGraphProperties = 29;
        public const int DssXmlReportManipulationShowSubtotals = 30;
        public const int DssXmlReportManipulationHideSubtotals = 31;
        public const int DssXmlReportManipulationShowBanding = 32;
        public const int DssXmlReportManipulationHideBanding = 33;
        public const int DssXmlReportManipulationShowThresholds = 34;
        public const int DssXmlReportManipulationHideThresholds = 35;
        public const int DssXmlReportManipulationApplyStyle = 36;
        public const int DssXmlReportManipulationApplyStyleObject = 37;
        public const int DssXmlReportManipulationPutProperties = 38;
        public const int DssXmlReportManipulationEditBaseFilter = 39;
        public const int DssXmlReportManipulationEditFilter = 40;
        public const int DssXmlReportManipulationSetWidthColumnScenario = 41;
        public const int DssXmlReportManipulationSetWidthRowScenario = 42;
        public const int DssXmlReportManipulationSetWidthHeader = 43;
        public const int DssXmlReportManipulationSetWidthMetric = 44;
        public const int DssXmlReportManipulationSetWidthClear = 45;
        public const int DssXmlReportManipulationAddForm = 46;
        public const int DssXmlReportManipulationAddBaseForm = 47;
        public const int DssXmlReportManipulationRemoveForm = 48;
        public const int DssXmlReportManipulationRemoveBaseForm = 49;
        public const int DssXmlReportManipulationResetForms = 50;
        public const int DssXmlReportManipulationAddUnitToBase = 53;
        public const int DssXmlReportManipulationFormatTemplate = 54;
        public const int DssXmlReportManipulationSetDatamart = 55;
        public const int DSSXmlReportManipulationAddLimi = 64;
        public const int DSSXmlReportManipulationCreateThreshold = 58;
        public const int DSSXmlReportManipulationUpdateThreshold = 59;
        public const int DSSXmlReportManipulationRemoveThreshold = 60;
        public const int DssXmlReportManipulationReorderThresholds = 61;
        public const int DssXmlReportManipulationSetCubeReport = 63;
        public const int DSSXmlReportManipulationAddLimit = 64;
        public const int DSSXmlReportManipulationAddLimitToView = 64;
        public const int DSSXmlReportManipulationRemoveLimit = 65;
        public const int DSSXmlReportManipulationRemoveLimitFromView = 65;
        public const int DSSXmlReportManipulationAddLimitToBase = 66;
        public const int DSSXmlReportManipulationRemoveLimitFromBase = 67;
        public const int DssXmlReportManipulationSetSourceTable = 68;
        public const int DssXmlReportManipulationAddDerivedElement = 69;
        public const int DssXmlReportManipulationUpdateDerivedElement = 70;
        public const int DssXmlReportManipulationFormatDerivedElement = 71;
        public const int DssXmlReportManipulationClearDerivedElementReference = 72;
        public const int DssXmlReportManipulationRemoveDerivedElement = 73;
        public const int DssXmlReportManipulationRemoveSubtotal = 74;
        public const int DssXmlReportManipulationSetDerivedElementProperties = 75;
        public const int DssXmlReportManipulationSetDerivedElementHierarchyOption = 76;
        public const int DssReportManipulationAddListQuickGroup = 77;
        public const int DssReportManipulationAddCalculationQuickGroup = 78;
        public const int DssXmlReportManipulationSetAbbreviation = 79;
        public const int DssXmlReportManipulationMakeTabular = 80;
        public const int DssXmlReportManipulationSetUnitData = 81;
        public const int DssXmlReportManipulationApplyDeliveryThresholds = 82;
        public const int DssXmlReportManipulationAddDerivedElementReference = 83;
        public const int DssXmlReportManipulationSelectDerivedElementReference = 84;
        public const int DssXmlReportManipulationUnSelectDerivedElementReference = 85;
        public const int DssXmlReportManipulationSetReportObjectProperties = 86;
        public const int DssXmlReportManipulationSetEvaluationOrder = 87;
        public const int DssXmlReportManipulationSetTransactionInfoOnUnit = 91;
        public const int DssXmlReportManipulationRemoveTransactionInfoOnUnit = 92;
        public const int DssXmlReportManipulationChangeMetricDynamicAlias = 93;
        public const int DssXmlReportManipulationAddShortcutMetric = 94;
    }

    public class DSSXMLReportObjects
    {
        public const int DssXmlReportObjectMainDefinition = 1;
        public const int DssXmlReportObjectViewDefinition = 2;
        public const int DssXmlReportObjectBaseDefinition = 3;
        public const int DssXmlReportObjectMainTemplate = 4;
        public const int DssXmlReportObjectViewTemplate = 5;
        public const int DssXmlReportObjectBaseTemplate = 6;
        public const int DssXmlReportObjectMainFilter = 7;
        public const int DssXmlReportObjectViewFilter = 8;
        public const int DssXmlReportObjectBaseFilter = 9;
        public const int DssXmlReportObjectDatamart = 10;
    }

    public class DSSXMLReportSaveAsFlags
    {
        public const int DssXmlReportSaveAsOriginalTemplate = 1;
        public const int DssXmlReportSaveAsOriginalFilter = 2;
        public const int DssXmlReportSaveAsWithAnswers = 4;
        public const int DssXmlReportSaveAsDefault = 6;
        public const int DssXmlReportSaveAsPruneAnswers = 8;
        public const int DssXmlReportSaveAsDeclinePrompts = 16;
        public const int DssXmlReportSaveAsOverWrite = 32;
        public const int DssXmlReportSaveAsEmbedded = 64;
        public const int DssXmlReportSaveAsModifyObjects = 128;
        public const int DssXmlReportSaveAsEmbeddedFilter = 256;
        public const int DssXmlReportSaveAsEmbeddedTemplate = 512;
        public const int DssXmlReportSaveAsJustFilter = 1024;
        public const int DssXmlReportSaveAsJustTemplate = 2048;
        public const int DssXmlReportSaveAsWithPrompts = 4096;
        public const int DssXmlReportSaveAsFilterWithPrompts = 8192;
        public const int DssXmlReportSaveAsTemplateWithPrompts = 16384;
        public const int DssXmlReportSaveAsClearCurrentElements = 32768;
        public const int DssXmlReportSaveAs8iWarning = 65536;
        public const int DssXmlReportSaveAsRemoveNonPrimaryNameTranslations = 131072;
        public const int DssXmlReportSaveAsFromWeb = 262144;
    }

    public class DssXMLReportState
    {
        public const int DssXmlReportDrillInstance = 67108864;
    }

    public class DSSXMLReportTypes
    {
        public const int DssXmlReportTypeBase = 1;
        public const int DssXmlReportTypeCube = 7;
        public const int DssXmlReportTypeSubset = 8;
    }

    public class DSSXMLRequestTypes
    {
        public const int DssXmlRequestTypeReserved = 0;
        public const int DssXmlRequestTypeReport = 1;
        public const int DssXmlRequestTypeDynamicReport = 2;
        public const int DssXmlRequestTypeDocument = 3;
        public const int DssXmlRequestTypeWorkbook = 4;
        public const int DssXmlRequestTypeChildReport = 5;
        public const int DssXmlRequestTypeSearch = 6;
        public const int DssXmlRequestTypeDatamartReport = 7;
        public const int DssXmlRequestTypeReportWritingDoc = 8;
        public const int DssXmlRequestTypeRWDataset = 9;
        public const int DssXmlRequestTypeDynamicReportWritingDoc = 10;
        public const int DssXmlRequestTypeCube = 11;
        public const int DssXmlRequestTypeIncrementalFetchReport = 12;
    }

    public class DSSXMLResult2Flags
    {
        public const int DssXmlResult2Widths = 1;
        public const int DssXmlResult2ShortCSSNames = 2;
        public const int DssXmlResult2Widths2 = 4;
        public const int DssXmlResult2RawDates = 8;
        public const int DssXmlResult2FormDetails = 16;
        public const int DssXmlResult2NoElementIDs = 32;
        public const int DssXmlResult2NoFormatXML = 64;
        public const int DssXmlResult2Personalize = 128;
        public const int DssXmlResult2NoGridNode = 256;
        public const int DssXmlResult2MinimalPropertiesXML = 512;
        public const int DssXmlResult2ReportSourceXML = 1024;
        public const int DssXmlResult2LimitsXML = 2048;
        public const int DssXmlResult2PageCurrentElementsOnly = 4096;
        public const int DssXmlResult2DrillPathsHighImportance = 8192;
        public const int DssXmlResult2DrillPathsMediumImportance = 16384;
        public const int DssXmlResult2DrillPathsLowImportance = 24576;
        public const int DssXmlResult2DrillPathsTemplate = 32768;
        public const int DssXmlResult2EnhancedTimeXML = 131072;
        public const int DssXmlResult2XDAHierarchies = 1048576;
        public const int DssXmlResult2EnhancedGraphXML = 524288;
        public const int DssXmlResult2NewSubtotalsXML = 262144;
        public const int DssXmlResult2ImageMap = 4194304;
        public const int DssXmlResult2PageTreeStyle2 = 2097152;
        public const int DssXmlResult2DrillPathsWithin = 8388608;
        public const int DssXmlResult2DrillPathsDesired = 16777216;
        public const int DssXmlResult2RWDGraphsSpecial = 33554432;
        public const int DssXmlResult2RowColumnLabels = 67108864;
        public const int DssXmlResult2InstanceProperties = 134217728;
        public const int DssXmlResult2WidgetStrings = 268435456;
        public const int DssXmlResult2DtlsReportDetails = 536870912;
        public const int DssXmlResult2AllPrompts = 1073741824;
        public const int DssXmlResult2NewCssAndColumnHeader = -2147483648;
    }

    public class DSSXMLResult3Flags
    {
        public const int DssXmlResult3TerseElementIDs = 1;
        public const int DssXmlResult3ObjectComments = 2;
        public const int DssXmlResult3ReportHeaderFooter = 4;
        public const int DssXmlResult3NoThresholdsXML = 64;
        public const int DssXmlResult3OptimizedElementTree = 128;
        public const int DssXmlResult3PageByAxisOnly = 256;
    }

    public class DSSXMLResultFlags
    {
        public const int DssXmlResultDtlsExprNothing = 1;
        public const int DssXmlResultDtlsExprEmbedded = 2;
        public const int DssXmlResultDtlsExprAll = 3;
        public const int DssXmlResultXmlSQL = 4;
        public const int DssXmlResultDtlsTemplateLimit = 8;
        public const int DssXmlResultDtlsTemplateUnits = 16;
        public const int DssXmlResultWorkingSet = 128;
        public const int DssXmlResultNoDerivedPromptXML = 32;
        public const int DssXmlResultDtlsFilterExpr = 64;
        public const int DssXmlResultXmlDisplayProperties = 256;
        public const int DssXmlResultNoResolution = 512;
        public const int DssXmlResultGraph = 1024;
        public const int DssXmlResultFilter = 2048;
        public const int DssXmlResultBandingAutoApply = 4096;
        public const int DssXmlResultRawData = 8192;
        public const int DssXmlResultDrillHigh = 16384;
        public const int DssXmlResultDrillMedium = 32768;
        public const int DssXmlResultDrillLow = 65536;
        public const int DssXmlResultGrid = 131072;
        public const int DssXmlResultBandingStyle = 262144;
        public const int DssXmlResultFolderPath = 524288;
        public const int DssXmlResultRelatedReports = 1048576;
        public const int DssXmlResultNoPageHeader = 2097152;
        public const int DssXmlResultMinimal = 4194304;
        public const int DssXmlResultViewReport = 8388608;
        public const int DssXmlResultPutToInboxRead = 16777216;
        public const int DssXmlResultStatusOnlyIfNotReady = 33554432;
        public const int DssXmlResultNoNumberFormating = 67108864;
        public const int DssXmlResultInboxMessage = 134217728;
        public const int DssXmlResultDefaultFormatProperties = 268435456;
        public const int DssXmlResultDatamartTableName = 536870912;
        public const int DssXmlResultPageTreeStyle = 1073741824;
        public const int DssXmlResultPreserve = -2147483648;
        public const int DssXmlResultPositiveFlags = 815402396;
    }

    public class DSSXMLRWControlActionType
    {
        public const int DssXmlRWControlActionTypeReserved = 0;
        public const int DssXmlRWControlActionTypeUndo = 1;
        public const int DssXmlRWControlActionTypeRedo = 2;
        public const int DssXmlRWControlActionTypeDiscard = 4;
        public const int DssXmlRWControlActionTypeSubmit = 8;
        public const int DssXmlRWControlActionTypeRecalculate = 16;
        public const int DssXmlRWControlActionTypeAll = -1;
    }

    public class DSSXMLRWControlSubsequentAction
    {
        public const int DssXmlRWControlSubsequentActionReserved = 0;
        public const int DssXmlRWControlSubsequentActionDisplayMessage = 1;
        public const int DssXmlRWControlSubsequentActionDisplayTransactionReport = 2;
        public const int DssXmlRWControlSubsequentActionRefreshGrid = 4;
        public const int DssXmlRWControlSubsequentActionReexecuteDocument = 8;
        public const int DssXmlRWControlSubsequentActionExecuteNewObject = 16;
        public const int DssXmlRWControlSubsequentActionForceExecution = 268435456;
        public const int DssXmlRWControlSubsequentActionInvalidateClientCache = 536870912;
        public const int DssXmlRWControlSubsequentActionPassPromptAnswer = 1073741824;
    }

    public class DSSXMLRWControlType
    {
        public const int DssXmlRWControlTypeReserved = 0;
        public const int DssXmlRWControlTypeElementList = 1;
        public const int DssXmlRWControlTypeMetricList = 2;
        public const int DssXmlRWControlTypePanelStack = 3;
        public const int DssXmlRWControlTypeTransactionAction = 4;
        public const int DssXmlRWControlTypeMultiUnit = 6;
    }

    public class DSSXMLRWDCacheAdminAction
    {
        public const int DssXmlInvalidateRWDCache = 1;
        public const int DssXmlDeleteRWDCache = 2;
        public const int DssXmlLoadRWDCache = 3;
        public const int DssXmlUnloadRWDCache = 4;
        public const int DssXmlExpireRWDCache = 5;
    }

    public class DSSXMLRWDCacheInfo
    {
        public const int DssXmlRWDCacheInfoFormat = 1;
        public const int DssXmlRWDCacheInfoStatus = 2;
        public const int DssXmlRWDCacheInfoProjectName = 3;
        public const int DssXmlRWDCacheInfoProjectId = 4;
        public const int DssXmlRWDCacheInfoId = 5;
        public const int DssXmlRWDCacheInfoRWDId = 6;
        public const int DssXmlRWDCacheInfoVersion = 7;
        public const int DssXmlRWDCacheInfoType = 8;
        public const int DssXmlRWDCacheInfoSize = 9;
        public const int DssXmlRWDCacheInfoHitCount = 10;
        public const int DssXmlRWDCacheInfoHistoricHitCount = 11;
        public const int DssXmlRWDCacheInfoCreationTime = 12;
        public const int DssXmlRWDCacheInfoExpireTime = 13;
        public const int DssXmlRWDCacheInfoLastLoadTime = 14;
        public const int DssXmlRWDCacheInfoLastUpdateTime = 15;
        public const int DssXmlRWDCacheInfoLastHitTime = 16;
        public const int DssXmlRWDCacheInfoLocation = 17;
        public const int DssXmlRWDCacheInfoHost = 18;
        public const int DssXmlRWDCacheInfoRWDName = 19;
        public const int DssXmlRWDCacheInfoPromptAnswer = 20;
        public const int DssXmlRWDCacheInfoCreator = 21;
        public const int DssXmlRWDCacheInfoUserId = 22;
        public const int DssXmlRWDCacheInfoSessionId = 23;
        public const int DssXmlRWDCacheInfoJobId = 24;
        public const int DssXmlRWDCacheInfoLayout = 25;
        public const int DssXmlRWDCacheInfoGroupBy = 26;
        public const int DssXmlRWDCacheInfoSecurityFilter = 27;
        public const int DssXmlRWDCacheInfoDBLogin = 28;
        public const int DssXmlRWDCacheInfoDBConnection = 29;
        public const int DssXmlRWDCacheInfoMDLocale = 30;
        public const int DssXmlRWDCacheInfoDataLocale = 31;
        public const int DssXmlRWDCacheInfoRptCaches = 32;
        public const int DssXmlRWDCacheInfoCubes = 33;
    }

    public class DSSXMLRWDCacheOptions
    {
        public const int DssXmlRWDCacheEnableNone = 0;
        public const int DssXmlRWDCacheEnablePrompt = 1;
        public const int DssXmlRWDCacheEnableNonPrompt = 2;
        public const int DssXmlRWDCacheEnableXml = 16;
        public const int DssXmlRWDCacheEnablePdf = 32;
        public const int DssXmlRWDCacheEnableExcel = 64;
        public const int DssXmlRWDCacheEnableCsv = 128;
        public const int DssXmlRWDCacheEnableHtml = 256;
        public const int DssXmlRWDCacheEnableAll = 499;
        public const int DssXmlRWDCacheEnableGroupBy = 512;
    }

    public class DSSXMLRWEaseCreationFlags
    {
        public const int DssXmlRWEaseCreationDefault = 0;
        public const int DssXmlRWEaseCreationIDIdentical = 1;
        public const int DssXmlRWEaseCreationLinkReportView = 2;
        public const int DssXmlRWEaseCreationEmbedReportInstance = 4;
        public const int DssXmlRWEaseCreationDocumentLite = 8;
        public const int DssXmlRWEaseCreationAnalysisDocument = 16;
    }

    public class DSSXMLRWExportFlags
    {
        public const int DssXmlRWExportAll = 0;
        public const int DssXmlRWExportCurrent = 1;
        public const int DssXmlRWExportCurrentWindow = 2;
        public const int DssXmlRWExportCurrentLayout = 3;
    }

    public class DSSXMLRWFieldType
    {
        public const int DssXmlRWFieldShape = 1;
        public const int DssXmlRWFieldImage = 2;
        public const int DssXmlRWFieldConcatenation = 3;
        public const int DssXmlRWFieldObjectName = 4;
        public const int DssXmlRWFieldObjectValue = 5;
        public const int DssXmlRWFieldLabel = 6;
        public const int DssXmlRWFieldLine = 7;
        public const int DssXmlRWFieldAutoText = 8;
        public const int DssXmlRWFieldCalculation = 9;
        public const int DssXmlRWFieldHTML = 10;
    }

    public class DSSXMLRWGroupByUnitType
    {
        public const int DssXmlRWGroupByUnitReserved = 0;
        public const int DssXmlRWGroupByUnitObject = 1;
        public const int DssXmlRWGroupByUnitMetric = 2;
    }

    public class DSSXMLRWLinkType
    {
        public const int DssXmlRWLinkHyperLink = 1;
        public const int DssXmlRWLinkImage = 2;
        public const int DssXmlRWLinkToolTip = 3;
        public const int DssXmlRWlinkURL = 4;
        public const int DssXmlRWLinkHTML = 5;
        public const int DssXmlRWLinkResolvedHyplerinks = 6;
    }

    public class DSSXMLRWManipulationCategory
    {
        public const int DssXmlRWManipulationCategoryReserved = 0;
        public const int DssXmlRWManipulationCategoryDrill = 1;
        public const int DssXmlRWManipulationCategoryDerivedElement = 2;
        public const int DssXmlRWManipulationCategoryPivotSort = 4;
        public const int DssXmlRWManipulationCategoryLastOne = 8;
    }

    public class DSSXMLRWManipulationMethod
    {
        public const int DssXmlRWManipulationReserved = 0;
        public const int DssXmlRWManipulationAddDataSet = 1;
        public const int DssXmlRWManipulationRemoveDataSet = 2;
        public const int DssXmlRWManipulationClearDataSets = 3;
        public const int DssXmlRWManipulationAddNode = 4;
        public const int DssXmlRWManipulationRemoveNode = 5;
        public const int DssXmlRWManipulationClearNodes = 6;
        public const int DssXmlRWManipulationFormatNode = 7;
        public const int DssXmlRWManipulationEditNode = 8;
        public const int DssXmlRWManipulationAddGroupByUnit = 9;
        public const int DssXmlRWManipulationAddSort = 10;
        public const int DssXmlRWManipulationAddField = 11;
        public const int DssXmlRWManipulationAddObjectField = 12;
        public const int DssXmlRWManipulationEditField = 13;
        public const int DssXmlRWManipulationEditObjectField = 14;
        public const int DssXmlRWManipulationRemoveField = 15;
        public const int DssXmlRWManipulationClearFields = 16;
        public const int DssXmlRWManipulationFormatField = 17;
        public const int DssXmlRWManipulationAddAttributeFormToField = 18;
        public const int DssXmlRWManipulationReportManipulation = 19;
        public const int DssXmlRWManipulationSetDocumentProperties = 20;
        public const int DssXmlRWManipulationEditSectionNode = 25;
        public const int DssXmlRWManipulationAddGroupBySection = 26;
        public const int DssXmlRWManipulationRemoveGroupBySection = 27;
        public const int DssXmlRWManipulationMoveGroupBySection = 28;
        public const int DssXmlRWManipulationClearSorts = 29;
        public const int DssXmlRWManipulationMoveNode = 30;
        public const int DssXmlRWManipulationSetDefaultDataSet = 31;
        public const int DssXmlRWManipulationSetGridDisplayMode = 32;
        public const int DssXmlRWManipulationCopyField = 33;
        public const int DssXmlRWManipulationMoveField = 34;
        public const int DssXmlRWManipulationCopyTemplateNode = 35;
        public const int DssXmlRWManipulationMoveTemplateNode = 36;
        public const int DssXmlRWManipulationSetGraphProperties = 37;
        public const int DssXmlRWManipulationSetDefaultFormat = 38;
        public const int DssXmlRWManipulationAddCopyTemplateNodeFromDataset = 39;
        public const int DssXmlRWManipulationAddDerivedMetricToDataset = 40;
        public const int DssXmlRWManipulationUpdateDerivedMetricOnDataset = 41;
        public const int DssXmlRWManipulationRemoveDerivedMetricFromDataset = 42;
        public const int DssXmlRWManipulationSetPageByStyle = 43;
        public const int DssXmlRWManipulationSetCurrentGroupByElement = 44;
        public const int DssXmlRWManipulationSetNextCurrentGroupByElement = 45;
        public const int DssXmlRWManipulationFormatDerivedMetric = 46;
        public const int DssXmlRWManipulationShowDocumentThresholds = 47;
        public const int DssXmlRWManipulationHideDocumentThresholds = 48;
        public const int DssXmlRWManipulationSetViewFilter = 49;
        public const int DssXmlRWManipulationAddLayout = 50;
        public const int DssXmlRWManipulationSetCurrentLayout = 51;
        public const int DssXmlRWManipulationImportDocument = 52;
        public const int DssXmlRWManipulationCreateThreshold = 53;
        public const int DssXmlRWManipulationUpdateThreshold = 54;
        public const int DssXmlRWManipulationRemoveThreshold = 55;
        public const int DssXmlRWManipulationReorderThresholds = 56;
        public const int DssXmlRWManipulationSetViewTemplate = 57;
        public const int DssXmlRWManipulationEditDataSet = 58;
        public const int DssXmlRWManipulationAddLinkTemplateNodeToDataset = 59;
        public const int DssXmlRWManipulationChangeTemplateNodeOrigin = 60;
        public const int DssXmlRWManipulationAddControl = 61;
        public const int DssXmlRWManipulationFormatControl = 62;
        public const int DssXmlRWManipulationSetCurrentControlElements = 63;
        public const int DssXmlRWManipulationInsertControlGroupBySection = 64;
        public const int DssXmlRWManipulationClearCurrentControlElements = 65;
        public const int DssXmlRWManipulationRemoveControlGroupBySection = 66;
        public const int DssXmlRWManipulationAddControlTarget = 67;
        public const int DssXmlRWManipulationRemoveControlTarget = 68;
        public const int DssXmlRWManipulationClearControlTargets = 69;
        public const int DssXmlRWManipulationRemoveControl = 70;
        public const int DssXmlRWManipulationClearControls = 71;
        public const int DssXmlRWManipulationSetCurrentNode = 72;
        public const int DssXMLRWManipulationSetControlSource = 73;
        public const int DssXMLRWManipulationCopyControl = 74;
        public const int DssXMLRWManipulationSetCurrentControlElementsFromGraph = 75;
        public const int DssXmlRWManipulationSetAbbreviation = 76;
        public const int DssXmlRWManipulationClearGroupByUnits = 77;
        public const int DssXMLRWManipulationSetControlShowAll = 78;
        public const int DssXMlRWManipulationMakeTabular = 79;
        public const int DssXmlRWManipulationPopulateTemplateNodeFromDataset = 80;
        public const int DssXmlRWManipulationSetControlAllAlias = 81;
        public const int DssXMLRWManipulationSetCurrentControlElementStatus = 82;
        public const int DssXmlRWManipulationSetRWDObjectProperties = 83;
        public const int DssRWManipulationAddRWDDefinitionSubtotal = 84;
        public const int DssRWManipulationRemoveRWDDefinitionSubtotal = 85;
        public const int DssXmlRWManipulationRemoveGroupByAndFieldGroupSortInLayout = 87;
        public const int DssXmlRWManipulationSetWidgetString = 88;
        public const int DssXmlRWManipulationRemoveWidgetString = 89;
        public const int DssXmlRWManipulationCopyPanel = 90;
        public const int DssXmlRWManipulationCopyPanelStack = 91;
        public const int DssXmlRWManipulationMoveControlGroupBySection = 92;
        public const int DssXmlRWManipulationSwitchControlGroupBySectionToUnitCondition = 94;
        public const int DssXmlRWManipulationSwitchUnitConditionToControlGroupBySection = 95;
        public const int DssXmlRWManipulationSetControlUnsetStatus = 96;
        public const int DssXmlRWManipulationReplaceDataset = 97;
        public const int DssXmlRWManipulationSetCurrentGroupByElementByUnitID = 98;
        public const int DssXmlRWManipulationChangeControlCondition = 99;
        public const int DssXmlRWManipulationToggleCurrentSelectionOnControl = 100;
        public const int DssXmlRWManipulationAddTransactionReport = 101;
        public const int DssXmlRWManipulationRemoveTransactionReport = 102;
        public const int DssXmlRWManipulationClearTransactionReport = 103;
        public const int DssXMLRWManipulationSetTransactionReport = 104;
        public const int DssXmlRWManipulationSetControlAvailableActionTypes = 105;
        public const int DssXmlRWManipulationSetTransactionInfoOnField = 106;
        public const int DssXmlRWManipulationSetControlSubsequentAction = 107;
        public const int DssXmlRWManipulationPerformActionOnActionSelector = 108;
        public const int DssXmlRWManipulationUpdateTransactionData = 109;
        public const int DssXmlRWManipulationSetTransactionChangeType = 110;
        public const int DssXmlRWManipulationSetSubsequentActionObject = 111;
        public const int DssXmlRWManipulationSetSubsequentDisplayMessage = 112;
        public const int DssXmlRWManipulationRemoveTransactionInfoOnField = 113;
        public const int DssXmlRWManipulationSetControlDisplayText = 114;
        public const int DssXmlRWManipulationAddShortcutMetric = 115;
        public const int DssXmlRWManipulationSetControlDelimiter = 116;
        public const int DssXmlRWManipulationSetControlAttributeFormDisplayOption = 117;
        public const int DssXmlRWManipulationSetControlAttributeForms = 118;
        public const int DssXmlRWManipulationAddDocumentView = 119;
        public const int DssXmlRWManipulationRemoveDocumentView = 120;
        public const int DssXmlRWManipulationSetCurrentDocumentView = 121;
        public const int DssXmlRWManipulationEditDocumentView = 122;
        public const int DssXmlRWManipulationSetControlDataSource = 124;
        public const int DssXmlRWManipulationSetControlWriteBackForm = 125;
        public const int DssXmlRWManipulationAddControlTargetDataset = 127;
        public const int DssXmlRWManipulationRemoveControlTargetDataset = 128;
        public const int DssXmlRWManipulationClearControlTargetDatasets = 129;
        public const int DssXmlRWManipulationDrill = 130;
        public const int DssXmlRWManipulationAddAssociatedRWNodes = 131;
        public const int DssXmlRWManipulationClearAssociatedRWNodes = 132;
        public const int DssRWManipulationApplyAttributeRemapping = 149;
        public const int DssXmlRWManipulationSetAutomaticTransactionRecordNumber = 151;
        public const int DssXmlRWManipulationSetControlMatchForm = 161;
        public const int DssXmlRWManipulationResetSelections = 169;
    }

    public class DSSXMLRWNodeTransactionChangeType
    {
        public const int DssXmlRWNodeTransactionChangeTypeReserved = 0;
        public const int DssXmlRWNodeTransactionChangeTypeDefault = 1;
        public const int DssXmlRWNodeTransactionChangeTypeMarkSelection = 2;
    }

    public class DSSXMLRWNodeType
    {
        public const int DssXmlRWNodeSection = 1;
        public const int DssXmlRWNodeTemplate = 3;
        public const int DssXmlRWNodeFieldGroup = 4;
        public const int DssXmlRWNodeDocument = 5;
        public const int DssXmlRWNodeControl = 6;
    }

    public class DSSXMLRWOptions
    {
        public const int DssXmlRWOptionGroupBy = 0;
        public const int DssXmlRWOptionFormatting = 1;
        public const int DssXmlRWOptionDataSlicing = 2;
    }

    public class DSSXMLRWPageByStyle
    {
        public const int DssXmlRWPageByStyleReserved = 0;
        public const int DssXmlRWPageByStyleIndividual = 1;
        public const int DssXmlRWPageByStyleAll = 2;
        public const int DssXmlRWPageByStyleBoth = 3;
    }

    public class DSSXMLRWPreLoadPanelState
    {
        public const int DssXmlRWPreLoadAllPanels = 0;
        public const int DssXmlRWLoadCurrentPanel = 1;
        public const int DssXmlRWUseDocumentSetting = 2;
    }

    public class DSSXMLRWSectionType
    {
        public const int DssXmlRWSectionContent = 1;
        public const int DssXmlRWSectionReportHeader = 2;
        public const int DssXmlRWSectionReportFooter = 3;
        public const int DssXmlRWSectionPageHeader = 4;
        public const int DssXmlRWSectionPageFooter = 5;
        public const int DssXmlRWSectionGroupBySection = 6;
        public const int DssXmlRWSectionGroupByHeader = 7;
        public const int DssXmlRWSectionGroupByFooter = 8;
        public const int DssXmlRWSectionBody = 9;
        public const int DssXmlRWSectionBodyHeader = 10;
        public const int DssXmlRWSectionDetails = 11;
        public const int DssXmlRWSectionBodyFooter = 12;
        public const int DssXmlRWSubSection = 13;
        public const int DssXmlRWSectionGeneric = 14;
        public const int DssXmlRWSectionControlGroupBySection = 15;
        public const int DssXmlRWSectionContainer = 16;
        public const int DssXmlRWSectionPanel = 17;
        public const int DssXmlRWSectionUnitCondition = 18;
    }

    public class DSSXMLRWSelectorUnsetStatus
    {
        public const int DssXmlRWSelectorUnsetReserved = 0;
        public const int DssXmlRWSelectorUnsetDefault = 1;
        public const int DssXmlRWSelectorUnsetFirst = 2;
        public const int DssXmlRWSelectorUnsetLast = 4;
    }

    public class DSSXMLRWTemplateNodeOrigin
    {
        public const int DssXmlRWTemplateNodeOriginReserved = 0;
        public const int DssXmlRWTemplateNodeOriginLocal = 1;
        public const int DssXmlRWTemplateNodeOriginReportView = 2;
    }

    public class DSSXMLRWTreeType
    {
        public const int DssXmlRWTreeTypeReserved = 0;
        public const int DssXmlRWTreeTypeContent = 1;
        public const int DssXmlRWTreeTypePageHeader = 2;
        public const int DssXmlRWTreeTypePageFooter = 3;
        public const int DssXmlRWTreeTypeClipBoard = 4;
    }

    public class DSSXMLScaleTypes
    {
        public const int DssXmlFontScaleAutomatic = 0;
        public const int DssXmlFontScaleManual = 1;
        public const int DssXmlFontScaleManualMin = 2;
        public const int DssXmlFontScaleManualMax = 3;
    }

    public class DSSXMLScatterMinorTypes
    {
        public const int DssXmlScatterMinorTypeXYScatter = 1;
        public const int DssXmlScatterMinorTypeDAXYScatter = 2;
        public const int DssXmlScatterMinorTypeXYScatterLables = 4;
        public const int DssXmlScatterMinorTypeDAXYScatterLables = 8;
    }

    public class DSSXMLScheduleFilterFlags
    {
        public const int DssXmlScheduleFilterReserved = 0;
        public const int DssXmlScheduleFilterDefault = 1;
        public const int DssXmlScheduleFilterReport = 2;
        public const int DssXmlScheduleFilterDocument = 4;
        public const int DssXmlScheduleFilterGeneral = 8;
    }

    public class DSSXMLScheduleTriggerType
    {
        public const int DssXmlTriggerEvent = 0;
        public const int DssXmlTriggerTimePointSeries = 1;
    }

    public class DSSXMLSearchDomain
    {
        public const int DssXmlSearchDomainReserved = 0;
        public const int DssXmlSearchDomainLocal = 1;
        public const int DssXmlSearchDomainProject = 2;
        public const int DssXmlSearchDomainRepository = 3;
        public const int DssXmlSearchDomainConfiguration = 4;
        public const int DssXmlSearchConfigurationAndAllProjects = 5;
    }

    public class DSSXMLSearchFlags
    {
        public const int DssXmlSearchNameWildCard = 1;
        public const int DssXmlSearchAbbreviationWildCard = 2;
        public const int DssXmlSearchDescriptionWildCard = 4;
        public const int DssXmlSearchVisibleOnly = 8;
        public const int DssXmlSearchUsesOneOf = 16;
        public const int DssXmlSearchUsesRecursive = 32;
        public const int DssXmlSearchUsedByOneOf = 64;
        public const int DssXmlSearchUsedByRecursive = 128;
        public const int DssXmlSearchRootRecursive = 256;
        public const int DssXmlSearchModificationTime = 512;
        public const int DssXmlSearchFolderBrowseStyle = 1024;
        public const int DssXmlSearchDereferenceShortcut = 2048;
    }

    public class DSSXMLSearchScope
    {
        public const int DssXmlSearchScopeReserved = 0;
        public const int DssXmlSearchScopeStandaloneObjects = 1;
        public const int DssXmlSearchScopeManagedObjects = 2;
        public const int DssXmlSearchScopeRootedObjects = 4;
        public const int DssXmlSearchScopeUnRootedObjects = 8;
        public const int DssXmlSearchScopeScopeAllObjects = 31;
    }

    public class DSSXMLSearchTypes
    {
        public const int DssXmlSearchTypeNotUsed = -1;
        public const int DssXmlSearchTypeContainsAnyWord = 0;
        public const int DssXmlSearchTypeBeginWith = 1;
        public const int DssXmlSearchTypeBeginWithPhrase = 3;
        public const int DssXmlSearchTypeExactly = 2;
        public const int DssXmlSearchTypeContains = 4;
        public const int DssXmlSearchTypeEndWidth = 5;
    }

    public class DSSXMLSearchUnit
    {
        public const int DssXmlSearchUnitReserved = 0;
        public const int DssXmlSearchUnitHour = 1;
        public const int DssXmlSearchUnitDay = 2;
        public const int DssXmlSearchUnitWeek = 3;
        public const int DssXmlSearchUnitMonth = 4;
        public const int DssXmlSearchUnitYear = 5;
    }

    public class DSSXMLSectionElementType
    {
        public const int DssSectionElementTemplate = 1;
        public const int DssSectionElementFieldGroup = 2;
        public const int DssSectionElementSubReport = 3;
    }

    public class DSSXMLSectionTreeType
    {
        public const int DssXmlSectionTreeTypeContent = 1;
        public const int DssXmlSectionTreeTypePageHeader = 2;
        public const int DssXmlSectionTreeTypePageFooter = 3;
    }

    public class DSSXMLServerDefManipulationMethod
    {
        public const int DssXmlServerDefManipulationReserved = 0;
        public const int DssXmlServerDefManipulationRegisterProject = 1;
        public const int DssXmlServerDefManipulationUnregisterProject = 2;
        public const int DssXmlServerDefManipulationModifyLoadOption = 3;
        public const int DssXmlServerDefManipulationSetServerSetting = 4;
        public const int DssXmlServerDefManipulationSetProjectSetting = 5;
        public const int DssXmlServerDefManipulationModifyComm = 6;
        public const int DssXmlServerDefManipulationSetStatTableSetting = 10;
        public const int DssXmlServerDefManipulationSetDeliveryNotificationSetting = 11;
        public const int DssXmlServerDefManipulationSetStatisticsSetting = 12;
    }

    public class DSSXmlServerProjectReportCacheDBMatchingFlag
    {
        public const int DssXmlServerProjectReportCacheDBMatchingFlagReserved = 0;
        public const int DssXmlServerProjectReportCacheMatchUserID = 1;
        public const int DssXmlServerProjectReportCacheMatchDBLogin = 2;
        public const int DssXmlServerProjectReportCacheMatchDBConnection = 4;
    }

    public class DSSXMLServerProjectSettingID
    {
        public const int DssXmlServerProjectMaxResultRowCount = 0;
        public const int DssXmlServerProjectMaxReportExecutionTime = 1;
        public const int DssXmlServerProjectMaxMessageRetentionTime = 2;
        public const int DssXmlServerProjectMaxJobPerUserAccount = 4;
        public const int DssXmlServerProjectMaxJobPerUserConnection = 5;
        public const int DssXmlServerProjectMaxJobPerProject = 6;
        public const int DssXmlServerProjectMaxUserConnectionPerProject = 7;
        public const int DssXmlServerProjectCollectStatisticsFlag = 8;
        public const int DssXmlServerProjectLoadFlag = 9;
        public const int DssXmlServerProjectMaxReportCacheMemConsumption = 10;
        public const int DssXmlServerProjectMaxReportCacheCount = 11;
        public const int DssXmlServerProjectMaxReportCacheDiskSpace = 12;
        public const int DssXmlServerProjectReportCacheLookupTableSize = 13;
        public const int DssXmlServerProjectMaxObjectCacheMemConsumption = 14;
        public const int DssXmlServerProjectMaxObjectCacheCount = 15;
        public const int DssXmlServerProjectObjectCacheLookupTableSize = 16;
        public const int DssXmlServerProjectMaxElementCacheMemConsumption = 17;
        public const int DssXmlServerProjectMaxElementCacheCount = 18;
        public const int DssXmlServerProjectElementCacheLookupTableSize = 19;
        public const int DssXmlServerProjectObjectCacheTableBucketPerLock = 23;
        public const int DssXmlServerProjectObjectFileCachePath = 24;
        public const int DssXmlServerProjectObjectSwapPolicy = 25;
        public const int DssXmlServerProjectObjectCacheLifeTime = 26;
        public const int DssXmlServerProjectElementCacheTableBucketPerLock = 30;
        public const int DssXmlServerProjectElementFileCachePath = 31;
        public const int DssXmlServerProjectElementSwapPolicy = 32;
        public const int DssXmlServerProjectElementCacheLifeTime = 33;
        public const int DssXmlServerProjectReportCacheTableBucketPerLock = 37;
        public const int DssXmlServerProjectReportFileCachePath = 38;
        public const int DssXmlServerProjectReportSwapPolicy = 39;
        public const int DssXmlServerProjectReportCacheLifeTime = 40;
        public const int DssXmlServerProjectDisableReportCaching = 41;
        public const int DssXmlServerProjectMaxAERowCount = 42;
        public const int DssXmlServerProjectMaxElementRowCount = 43;
        public const int DssXmlServerProjectReportCacheDBMatchingFlag = 44;
        public const int DssXmlServerProjectLinkCacheMaxMemConsumption = 45;
        public const int DssXmlServerProjectLinkCacheTableBucketCount = 46;
        public const int DssXmlServerProjectLinkCacheTableBucketPerLock = 47;
        public const int DssXmlServerProjectLinkCacheSwapPolicy = 48;
        public const int DssXmlServerProjectMX2DBUserName = 49;
        public const int DssXmlServerProjectMX2DBPassword = 50;
        public const int DssXmlServerProjectMX2RepositoryName = 51;
        public const int DssXmlServerProjectMX2RepositoryUserName = 52;
        public const int DssXmlServerProjectMX2RepositoryPassword = 53;
        public const int DssXmlServerProjectMX2DSN = 54;
        public const int DssXmlServerProjectMX2RepositoryFolder = 55;
        public const int DssXmlServerProjectMaxExecutingJobsPerUser = 56;
        public const int DssXmlServerProjectMaxInternalResultRowCount = 57;
        public const int DssXmlServerProjectETLUseCache = 58;
        public const int DssXmlServerProjectReportCacheLoadOnStart = 59;
        public const int DssXmlServerProjectElementCacheUsesSecurityFilters = 60;
        public const int DssXmlServerProjectElementCacheUseConnectionMap = 61;
        public const int DssXmlServerProjectElementCacheUsePassThroughLogin = 62;
        public const int DssXmlServerProjectEnablePersonizedDrillPath = 63;
        public const int DssXmlServerProjectStatsPurgeTimeout = 64;
        public const int DssXmlServerProjectReportCacheSwapFactor = 65;
        public const int DssXmlServerProjectMaxCubeCount = 66;
        public const int DssXmlServerProjectMaxCubeMemUsage = 67;
        public const int DssXmlServerProjectCubeStorage = 68;
        public const int DssXmlServerProjectMaxInteractiveJobPerProject = 69;
        public const int DssXmlServerProjectMaxScheduledReportExecutionTime = 70;
        public const int DssXmlServerProjectScheduledReportExecutionRetryCount = 71;
        public const int DssXmlServerProjectMaxCubeResultRowCount = 72;
        public const int DssXmlServerProjectMaxDatamartResultRowCount = 73;
        public const int DssXmlServerProjectMaxPromptWaitingTime = 74;
        public const int DssXmlServerProjectRWDMaxCacheCount = 75;
        public const int DssXmlServerProjectRWDMaxMemoryConsumption = 76;
        public const int DssXmlServerProjectEnableRWDCaching = 77;
        public const int DssXmlServerProjectFileZippingCompressionEnabled = 78;
        public const int DssXmlServerProjectFileZippingCompressionType = 79;
        public const int DssXmlServerProjectFileZippingCompressionLevel = 80;
        public const int DssXmlServerProjectEmailZippingCompressionEnabled = 81;
        public const int DssXmlServerProjectEmailZippingCompressionType = 82;
        public const int DssXmlServerProjectEmailZippingCompressionLevel = 83;
        public const int DssXmlServerProjectCubeDBMatchingFlag = 84;
        public const int DssXmlServerProjectMaxInteractiveSessionPerUser = 85;
        public const int DssXmlServerProjectRWDCacheStorage = 86;
        public const int DssXmlServerProjectCacheEncryptionOption = 87;
        public const int DssXmlServerProjectMaxHistoryListSubscriptionCount = 88;
        public const int DssXmlServerProjectMaxCacheUpdateSubscriptionCount = 89;
        public const int DssXmlServerProjectMaxEmailSubscriptionCount = 90;
        public const int DssXmlServerProjectMaxFileSubscriptionCount = 91;
        public const int DssXmlServerProjectMaxPrintSubscriptionCount = 92;
        public const int DssXmlServerProjectReRunHistoryListSubscription = 93;
        public const int DssXmlServerProjectReRunEmailFilePrintSubscription = 94;
        public const int DssXmlServerProjectDoNotCreateUpdateCaches = 95;
        public const int DssXmlServerProjectCacheInfoFieldExcluded = 96;
        public const int DssXmlServerProjectMaxWHJobExecTime = 97;
        public const int DssXmlServerProjectMaxMobileSubscriptionCount = 98;
        public const int DssXmlServerProjectWebServerLocation = 99;
        public const int DssXmlServerProjectCacheIgnoreDynamicDate = 100;
        public const int DssXmlServerProjectCubeLoadOnStart = 101;
        public const int DssXmlServerProjectMaxSQLGenMemConsumption = 102;
        public const int DssXmlServerProjectInvalidateCacheEventId = 103;
        public const int DssXmlServerProjectDeliveryErrorHandlingError = 104;
        public const int DssXmlServerProjectDeliveryErrorHandlingNoDataReturned = 105;
        public const int DssXmlServerProjectDeliveryErrorHandlingPartialResults = 106;
        public const int DssXmlServerProjectDistributionServiceEmailFooterEnabled = 107;
        public const int DssXmlServerProjectDistributionServiceEmailFooter = 108;
        public const int DssXmlServerProjectHistoryListKeepDocumentAvailableForManipulation = 109;
        public const int DssXmlServerProjectDistributionServicePrintRangeEnabled = 110;
        public const int DssXmlServerProjectStatisticsSQLTimeout = 111;
        public const int DssXmlServerProjectStatisticsPurgeTimeout = 112;
        public const int DssXmlServerProjectCubePublishOption = 113;
        public const int DssXmlServerProjectEnableMobilePushUpdate = 114;
        public const int DssXmlServerProjectDataUploadSizeLimit = 115;
        public const int DssXmlServerProjectCubeQuota = 116;
        public const int DssXmlServerProjectReportCacheLKUPMaxRAM = 117;
        public const int DssXmlServerProjectRWDCacheLKUPMaxRAM = 118;
        public const int DssXmlServerProjectMaxMemoryPerDataFetch = 119;
        public const int DssXmlServerProjectCubeIndexGrowthCheckFreq = 120;
        public const int DssXmlServerProjectCubeIndexGrowthUpperbound = 121;
        public const int DssXmlServerProjectMaxMsgCountPerProject = 122;
        public const int DssXmlServerProjectHistoryListSaveExportResultForRWD = 123;
        public const int DssXmlServerProjectHistoryListSaveChildMsgForRWD = 124;
        public const int DssXmlServerProjectFileZippingCompressionExtension = 125;
        public const int DssXmlServerProjectEmailZippingCompressionExtension = 126;
        public const int DssXmlServerProjectEnableSearch = 127;
        public const int DssXmlServerProjectEnableFileUpload = 128;
        public const int DssXmlServerProjectEnableFileUploadViaHttpHttps = 129;
        public const int DssXmlServerProjectEnableFileUploadViaFtpFtps = 130;
        public const int DssXmlServerProjectEnableFileUploadViaFile = 131;
        public const int DssXmlServerProjectDeliveryErrorHandlingNotifyNoDataReturned = 132;
        public const int DssXmlServerProjectDeliveryErrorHandlingNotifyPartialResults = 133;
        public const int DssXmlServerProjectMaxPersonalViewSubscriptionCount = 134;
        public const int DssXmlServerProjectStatisticsMobileClientLocation = 135;
        public const int DssXmlServerProjectStatisticsMaxDBConnectionTry = 136;
        public const int DssXmlServerProjectLastSetting = 136;
    }

    public class DSSXMLServerSettingID
    {
        public const int DssXmlServerMaxJobsPerServer = 0;
        public const int DssXmlServerMaxUserConnectionPerServer = 1;
        public const int DssXmlServerBackupPeriodicity = 2;
        public const int DssXmlServerBackupDirectory = 3;
        public const int DssXmlServerMSIPerformanceMonitoringFlag = 4;
        public const int DssXmlServerStartupDiagnosticsFlag = 5;
        public const int DssXmlServerMSILockCheckingFlag = 6;
        public const int DssXmlServerBackupIgnoreFlag = 7;
        public const int DssXmlServerSchedulerFlag = 8;
        public const int DssXmlServerObjectMaxCacheCount = 9;
        public const int DssXmlServerObjectMaxCacheMemoryConsumption = 10;
        public const int DssXmlServerObjectCacheTableBucketCount = 11;
        public const int DssXmlServerObjectCacheTableBucketPerLock = 12;
        public const int DssXmlServerObjectFileCachePath = 13;
        public const int DssXmlServerObjectSwapPolicy = 14;
        public const int DssXmlServerObjectCacheLifeTime = 15;
        public const int DssXmlServerInboxFilePath = 16;
        public const int DssXmlServerMaxUserConnectionIdleTime = 17;
        public const int DssXmlServerMaxInboxMsgCountPerUser = 18;
        public const int DssXmlServerDropTempTableFlag = 19;
        public const int DssXmlServerConnectionLimit = 20;
        public const int DssXmlServerSynchCallTimeout = 21;
        public const int DssXmlServerMaxXmlNodes = 22;
        public const int DssXmlServerLoadBalanceMode = 23;
        public const int DssXmlServerThreadIdleTimeOut = 24;
        public const int DssXmlServerMaxThreadPerCPU = 25;
        public const int DssXmlServerMinBalanceInterval = 26;
        public const int DssXmlServerMaxLoadDeviation = 27;
        public const int DssXmlServerMaxMemoryUsage = 28;
        public const int DssXmlServerMinFreeMemory = 29;
        public const int DssXmlServerClusterIPOption = 30;
        public const int DssXmlServerClusterGroupAddress = 31;
        public const int DssXmlServerDefaultProjectMaxResultRowCount = 32;
        public const int DssXmlServerDefaultProjectMaxReportExecutionTime = 33;
        public const int DssXmlServerDefaultProjectMaxMessageRetentionTime = 34;
        public const int DssXmlServerDefaultProjectMaxJobPerUserAccount = 35;
        public const int DssXmlServerDefaultProjectMaxJobPerUserConnection = 36;
        public const int DssXmlServerDefaultProjectMaxJobPerProject = 37;
        public const int DssXmlServerDefaultProjectMaxUserConnectionPerProject = 38;
        public const int DssXmlServerDefaultProjectCollectStatisticsFlag = 39;
        public const int DssXmlServerDefaultProjectLoadFlag = 40;
        public const int DssXmlServerDefaultProjectMaxReportCacheMemConsumption = 41;
        public const int DssXmlServerDefaultProjectMaxReportCacheCount = 42;
        public const int DssXmlServerDefaultProjectMaxReportCacheDiskSpace = 43;
        public const int DssXmlServerDefaultProjectReportCacheLookupTableSize = 44;
        public const int DssXmlServerDefaultProjectMaxObjectCacheMemConsumption = 45;
        public const int DssXmlServerDefaultProjectMaxObjectCacheCount = 46;
        public const int DssXmlServerDefaultProjectMaxElementCacheMemConsumption = 47;
        public const int DssXmlServerDefaultProjectMaxElementCacheCount = 48;
        public const int DssXmlServerDefaultProjectElementCacheLookupTableSize = 49;
        public const int DssXmlServerDefaultProjectObjectCacheTableBucketPerLock = 50;
        public const int DssXmlServerDefaultProjectObjectFileCachePath = 51;
        public const int DssXmlServerDefaultProjectObjectSwapPolicy = 52;
        public const int DssXmlServerDefaultProjectObjectCacheLifeTime = 53;
        public const int DssXmlServerDefaultProjectElementCacheTableBucketPerLock = 54;
        public const int DssXmlServerDefaultProjectElementFileCachePath = 55;
        public const int DssXmlServerDefaultProjectElementSwapPolicy = 56;
        public const int DssXmlServerDefaultProjectElementCacheLifeTime = 57;
        public const int DssXmlServerDefaultProjectReportCacheTableBucketPerLock = 58;
        public const int DssXmlServerDefaultProjectReportFileCachePath = 59;
        public const int DssXmlServerDefaultProjectReportSwapPolicy = 60;
        public const int DssXmlServerDefaultProjectReportCacheLifeTime = 61;
        public const int DssXmlServerDefaultProjectDisableReportCaching = 62;
        public const int DssXmlServerDefaultProjectMaxAERowCount = 63;
        public const int DssXmlServerDefaultProjectMaxElementRowCount = 64;
        public const int DssXmlServerDefaultProjectReportCacheDBMatchingFlag = 65;
        public const int DssXmlServerDefaultProjectLinkCacheMaxMemConsumption = 66;
        public const int DssXmlServerDefaultProjectLinkCacheTableBucketCount = 67;
        public const int DssXmlServerDefaultProjectLinkCacheTableBucketPerLock = 68;
        public const int DssXmlServerDefaultProjectLinkCacheReplacementPolicy = 69;
        public const int DssXmlServerMaxWebConnectionIdleTime = 70;
        public const int DssXmlServerMaxXmlDrillPaths = 71;
        public const int DssXmlServerAuthSettings = 72;
        public const int DssXmlServerClusterPort = 73;
        public const int DssXmlServerMaxUsedVirtualByte = 74;
        public const int DssXmlServerMaxUsedPrivateByte = 75;
        public const int DssXmlServerMaxContractLimitAbsolute = 76;
        public const int DssXmlServerMaxContractLimitPercentage = 77;
        public const int DssXmlServerMemoryReserve = 78;
        public const int DssXmlServerMemoryReservePercentage = 79;
        public const int DssXmlServerMemoryRequestIdleTime = 80;
        public const int DssXmlServerMCMWatermark = 81;
        public const int DssXmlServerStatsLogging2AllProjects = 82;
        public const int DssXmlServerStatsLogging2ProjectGUID = 83;
        public const int DssXmlServerVirtualByteLimitPercent = 84;
        public const int DssXmlServerWSRPBucketCount = 85;
        public const int DssXmlServerWSRPMaxResultCount = 86;
        public const int DssXmlServerWSRPMaxMemoryConsumption = 87;
        public const int DssXmlServerWSRPSwapPath = 88;
        public const int DssXmlServerDefaultProjectReportCacheLoadOnStart = 89;
        public const int DssXmlServerCacheCleanUpFrequency = 90;
        public const int DssXmlServerMaxInboxMsgLifeTime = 91;
        public const int DssXmlServerDefaultLicenseComplianceCheckTime = 92;
        public const int DssXmlServerMaxSchedulerConnectionIdleTime = 93;
        public const int DssXmlServerMSIPersistPerformanceCounter = 94;
        public const int DssServerMSIPersistPerformanceCounter = 94;
        public const int DssXmlServerMSIPersistPerformanceCounterFrequency = 95;
        public const int DssServerMSIPersistPerformanceCounterFrequency = 95;
        public const int DssXmlServerMSIPersistPerformanceCounterList = 96;
        public const int DssServerMSIPersistPerformanceCounterList = 96;
        public const int DssXmlServerPDFMaxMemoryConsumption = 98;
        public const int DssServerPDFMaxMemoryConsumption = 98;
        public const int DssXmlServerXMLMaxMemoryConsumption = 99;
        public const int DssServerXMLMaxMemoryConsumption = 99;
        public const int DssXmlServerExcelMaxMemoryConsumption = 100;
        public const int DssServerExcelMaxMemoryConsumption = 100;
        public const int DssXmlServerProjectLevelClustering = 101;
        public const int DssServerProjectLevelClustering = 101;
        public const int DssXmlServerProjectLevelClusteringPolicyProjectMapping = 102;
        public const int DssServerProjectLevelClusteringPolicyProjectMapping = 102;
        public const int DssXmlServerProjectFailoverLatency = 104;
        public const int DssServerProjectFailoverLatency = 104;
        public const int DssXmlServerProjectMigrationLatency = 105;
        public const int DssServerProjectMigrationLatency = 105;
        public const int DssXmlServerMaxCommandsInQueueLimit = 106;
        public const int DssXmlServerClusteringStartupMembership = 107;
        public const int DssXmlServerCSDBRole = 108;
        public const int DssXmlServerHLRepositoryType = 109;
        public const int DssXmlServerNCSCSDBRole = 110;
        public const int DssXmlServerMaxInteractiveJobsPerServer = 111;
        public const int DssXmlServerAuthSettings2 = 112;
        public const int DssXmlServerMaxLoginAttempt = 113;
        public const int DssXmlServerClusteringLoadBalancingFactor = 114;
        public const int DssXmlServerHLAutoDeleteMsgCount = 115;
        public const int DssXmlServerHLAutoDeleteErrorMsgFirst = 116;
        public const int DssXmlServerHLAutoDeleteTimestamp = 117;
        public const int DssXmlServerIgnoreGovernorOnChild = 118;
        public const int DssXmlServerAuthSettings3 = 119;
        public const int DssXmlServerMemoryReserveHigh32 = 120;
        public const int DssXmlServerDefaultProjectCubeStorage = 121;
        public const int DssXmlServerHtmlMaxMemoryConsumption = 122;
        public const int DssXmlServerEnableReportCacheBackup = 123;
        public const int DssXmlServerUserAffinityFlag = 124;
        public const int DssXmlServerUserAffinityLoadBalancingFlag = 125;
        public const int DssXmlServerHybridCentralInboxFilePath = 126;
        public const int DssXmlServerIncludeLDAPUsersForAuditoringFlag = 127;
        public const int DssXmlServerDefLastSetting = 128;
    }

    public class DSSXMLSessionFlags
    {
        public const int DssXmlSessionCancelActiveJobsOnClose = 1;
        public const int DssXmlSessionKeepPromptJobOnClose = 2;
        public const int DssXmlSessionDeleteReadMsgsOnClose = 16;
        public const int DssXmlSessionDeleteUnreadMsgsOnClose = 256;
        public const int DssXmlSessionBypassClustering = 512;
        public const int DssXmlSessionChangePwdOnCreate = 4096;
        public const int DssXmlSessionPreserveFlags = 65536;
        public const int DssXmlSessionCheckWebCache = 1048576;
        public const int DssXmlSessionUseWebCacheOnly = 2097152;
        public const int DssXmlSessionFBAppLogin = 268435456;
    }

    public class DSSXMLShapeType
    {
        public const int DssXmlShapeTypeNone = 0;
        public const int DssXmlShapeTypeLines = 1;
        public const int DssXmlShapeTypeRectangle = 2;
        public const int DssXmlShapeTypeFilledRectangle = 3;
        public const int DssXmlShapeTypeRoundedRectangle = 4;
        public const int DssXmlShapeTypeFilledRoundedRectangle = 5;
    }

    public class DSSXMLShowAxis
    {
        public const int DssXmlShowShowAxisLine = 1;
        public const int DssXmlShowAxisLabels = 2;
        public const int DssXmlShowAxisMajorGridlines = 4;
        public const int DssXmlShowAxisMinorGridlines = 8;
        public const int DssXmlShowAxisSuperGridlines = 16;
        public const int DssXmlShowAll = 31;
    }

    public class DSSXMLSortType
    {
        public const int DssXmlSortSubtotalsPosition = 1;
        public const int DssXmlSortAttributeForm = 2;
        public const int DssXmlSortAttributeDefault = 3;
        public const int DssXmlSortMetric = 4;
        public const int DssXmlSortMetricHierarchical = 5;
        public const int DssXmlSortGroupByMetric = 6;
        public const int DssXmlSortGroupBySubtotal = 7;
        public const int DssXmlSortDimensionForm = 8;
        public const int DssXmlSortDimensionDefault = 9;
        public const int DssXmlSortConsolidationDefault = 10;
        public const int DssXmlSortCustomGroupDefault = 11;
        public const int DssXmlSortValue = 12;
    }

    public class DSSXMLSourceFeatures
    {
        public const int DssXmlSourceFeatureSingleHierarchyPerDimensionOnTemplate = 1;
    }

    public class DSSXMLSourceManipulationCommands
    {
        public const int DssSourceManipulationReserved = 0;
        public const int DssSourceManipulationMergeFile = 1;
        public const int DssSourceManipulationUpdateSchema = 2;
        public const int DssSourceManipulationUndoMergeFile = 3;
        public const int DssSourceManipulationCreatePackageFile = 4;
        public const int DssSourceManipulationReadManifest = 5;
        public const int DssSourceManipulationSearchReplacePackage = 6;
        public const int DssSourceManipulationReadManifestFromObject = 7;
    }

    public class DSSXMLSourceManipulatorFlags
    {
        public const int DssSourceManipulatorDefault = 0;
        public const int DssSourceManipulatorNoObjectLocks = 1;
        public const int DssSourceManipulatorChangeModificationTime = 2;
        public const int DssSourceManipulatorValidateOnClose = 32;
        public const int DssSourceManipulatorMergeTranslations = 64;
        public const int DssSourceManipulatorMergeTranslationsReplace = 128;
        public const int DssSourceManipulatorExpectNoActionOnMerge = 512;
        public const int DssSourceManipulatorNoLogUnchanged = 1024;
        public const int DssSourceManipulatorDeferLoadFromSource = 2048;
        public const int DssSourceManipulatorNotPurgeClientCache = 8192;
        public const int DssSourceManipulatorIncludeManifest = 16384;
        public const int DssSourceManipulatorMergeToObjectContext = 32768;
        public const int DssSourceManipulatorLogOnly = 268435456;
        public const int DssSourceManipulatorNoValidation = 536870912;
        public const int DssSourceManipulatorTryReplaceOnNameClash = 1073741824;
    }

    public class DSSXMLSourceManipulatorInheritACL
    {
        public const int DssSourceManipulatorInheritACLReserved = 0;
        public const int DssSourceManipulatorInheritACLOriginal = 1;
        public const int DssSourceManipulatorInheritACLInherit = 2;
        public const int DssSourceManipulatorInheritACLUserFullControl = 4;
        public const int DssSourceManipulatorInheritACLNormal = 3;
        public const int DssInheritACLAll = 7;
        public const int DssInheritACLInheritableFolderOnly = 2;
    }

    public class DSSXMLSourcesInfoFlags
    {
        public const int DssXmlSourcesInfoNone = 0;
        public const int DssXmlSourcesInfoGetReports = 1;
        public const int DssXmlSourcesInfoGetUserName = 2;
        public const int DssXmlSourcesInfoGetTokensInfo = 4;
        public const int DssXmlSourcesInfoRevokeSession = 8;
    }

    public class DSSXMLSQLType
    {
        public const int DssXmlFunctionSQLTypeReserved = 0;
        public const int DssXmlFunctionSQLTypeAggregation = 1;
        public const int DssXmlFunctionSQLTypeLogic = 2;
        public const int DssXmlFunctionSQLTypeArithmetic = 3;
        public const int DssXmlFunctionSQLTypeComparison = 4;
        public const int DssXmlFunctionSQLTypeRelative = 5;
    }

    public class DSSXMLStatementType
    {
        public const int DssXmlStatementTypeReserved = 0;
        public const int DssXmlStatementTypeReport = 1;
        public const int DssXmlStatementTypeDocument = 2;
        public const int DssXmlStatementTypeMessage = 3;
        public const int DssXmlStatementTypeCache = 4;
    }

    public class DSSXMLStatisticDataID
    {
        public const int DssXmlStatisticDataIDReserved = 0;
        public const int DssXmlStatisticUserSessionID = 1;
        public const int DssXmlStatisticJobHeaderID = 2;
        public const int DssXmlStatisticJobID = 3;
        public const int DssXmlStatisticMDReqHeaderID = 4;
        public const int DssXmlStatisticMDReqID = 5;
        public const int DssXmlStatisticElemReqHeaderID = 6;
        public const int DssXmlStatisticElemReqID = 7;
        public const int DssXmlStatisticStepJobID = 8;
        public const int DssXmlStatisticSessionId = 9;
        public const int DssXmlStatisticScheduleSessionID = 10;
        public const int DssXmlStatisticReportJobID = 11;
        public const int DssXmlStatisticSecurityID = 12;
        public const int DssXmlStatisticReportStepID = 13;
        public const int DssXmlStatisticReportSQLID = 14;
        public const int DssXmlStatisticReportCOLID = 15;
        public const int DssXmlStatisticCacheId = 16;
        public const int DssXmlStatisticDocumentJobId = 17;
        public const int DssXmlStatisticUserModuleReserved = 100;
        public const int DssXmlStatisticUserServer = 101;
        public const int DssXmlStatisticUserProject = 102;
        public const int DssXmlStatisticUserName = 103;
        public const int DssXmlStatisticUserMachine = 104;
        public const int DssXmlStatisticUserConnectTime = 105;
        public const int DssXmlStatisticUserDisconnectTime = 106;
        public const int DssXmlStatisticUserLastData = 107;
        public const int DssXmlStatisticReportModuleReserved = 200;
        public const int DssXmlStatisticJobStartTime = 201;
        public const int DssXmlStatisticJobRepResolTime = 202;
        public const int DssXmlStatisticJobSqlEngTime = 203;
        public const int DssXmlStatisticJobQueryExecTime = 204;
        public const int DssXmlStatisticJobAnalytTime = 205;
        public const int DssXmlStatisticJobResolutionCPUTime = 206;
        public const int DssXmlStatisticJobSqlEngineCPUTime = 207;
        public const int DssXmlStatisticJobQueryExecutionCPUTime = 208;
        public const int DssXmlStatisticJobAnalyticalCPUTime = 209;
        public const int DssXmlStatisticJobFinishTime = 210;
        public const int DssXmlStatisticJobDuration = 211;
        public const int DssXmlStatisticJobStatus = 212;
        public const int DssXmlStatisticJobServerName = 213;
        public const int DssXmlStatisticJobProjectID = 214;
        public const int DssXmlStatisticJobUserID = 215;
        public const int DssXmlStatisticJobWebMachine = 216;
        public const int DssXmlStatisticJobClientMachine = 217;
        public const int DssXmlStatisticJobMachineName = 217;
        public const int DssXmlStatisticJobReportID = 218;
        public const int DssXmlStatisticJobTemplate = 219;
        public const int DssXmlStatisticJobFilter = 220;
        public const int DssXmlStatisticJobSource = 221;
        public const int DssXmlStatisticJobCacheIndicator = 222;
        public const int DssXmlStatisticJobSessionID = 223;
        public const int DssXmlStatisticJobSqlSteps = 224;
        public const int DssXmlStatisticJobFinalResultSize = 225;
        public const int DssXmlStatisticHeaderJobTime = 226;
        public const int DssXmlStatisticJobStatsProject = 227;
        public const int DssXmlStatisticJobStatsSessionID = 228;
        public const int DssXmlStatisticJobLastData = 229;
        public const int DssXmlStatisticMdReqModuleReserved = 300;
        public const int DssXmlStatisticMdReqServerName = 301;
        public const int DssXmlStatisticMdReqProjectID = 302;
        public const int DssXmlStatisticMdReqUserID = 303;
        public const int DssXmlStatisticMdReqWebMachine = 304;
        public const int DssXmlStatisticMdReqClientMachine = 305;
        public const int DssXmlStatisticMdReqMachineName = 305;
        public const int DssXmlStatisticMdReqJobSource = 306;
        public const int DssXmlStatisticMdReqCacheIndicator = 307;
        public const int DssXmlStatisticMdReqSessionID = 308;
        public const int DssXmlStatisticMdReqStartTime = 309;
        public const int DssXmlStatisticMdReqObjSrvTime = 310;
        public const int DssXmlStatisticMdReqFinishTime = 311;
        public const int DssXmlStatisticMdReqJobStatus = 312;
        public const int DssXmlStatisticMdReqHeaderTime = 313;
        public const int DssXmlStatisticMdReqStatsProject = 314;
        public const int DssXmlStatisticMdReqStatsSessionID = 315;
        public const int DssXmlStatisticMdReqLastData = 316;
        public const int DssXmlStatisticElemReqModuleReserved = 400;
        public const int DssXmlStatisticElemReqServerName = 401;
        public const int DssXmlStatisticElemReqProjectID = 402;
        public const int DssXmlStatisticElemReqUserID = 403;
        public const int DssXmlStatisticElemReqWebMachine = 404;
        public const int DssXmlStatisticElemReqClientMachine = 405;
        public const int DssXmlStatisticElemReqMachineName = 405;
        public const int DssXmlStatisticElemReqSource = 406;
        public const int DssXmlStatisticElemCacheIndicator = 407;
        public const int DssXmlStatisticElemSessionID = 408;
        public const int DssXmlStatisticElemReqStartTime = 409;
        public const int DssXmlStatisticElemReqSqlEngTime = 410;
        public const int DssXmlStatisticElemReqResolutionTime = 411;
        public const int DssXmlStatisticElemQueryExecTime = 412;
        public const int DssXmlStatisticElemReqElemSrvTime = 413;
        public const int DssXmlStatisticElemReqFinishTime = 414;
        public const int DssXmlStatisticElemReqDuration = 415;
        public const int DssXmlStatisticElemReqResultSize = 416;
        public const int DssXmlStatisticElemReqJobStatus = 417;
        public const int DssXmlStatisticElemReqHeaderTime = 418;
        public const int DssXmlStatisticElemReqStatsProject = 419;
        public const int DssXmlStatisticElemReqStatsSessionID = 420;
        public const int DssXmlStatisticElemReqLastData = 421;
        public const int DssXmlStatisticStepModuleReserved = 500;
        public const int DssXmlStatisticStepSequenceNumber = 501;
        public const int DssXmlStatisticStepTablesAccessed = 502;
        public const int DssXmlStatisticStepResultSize = 503;
        public const int DssXmlStatisticStepDurationTime = 504;
        public const int DssXmlStatisticStepSql = 505;
        public const int DssXmlStatisticStepType = 506;
        public const int DssXmlStatisticStepSessionID = 507;
        public const int DssXmlStatisticStepLastData = 508;
        public const int DssXmlStatisticSessionModuleReserved = 600;
        public const int DssXmlStatisticSessionServerID = 601;
        public const int DssXmlStatisticSessionSessionID = 602;
        public const int DssXmlStatisticSessionServerName = 603;
        public const int DssXmlStatisticSessionServerMachine = 604;
        public const int DssXmlStatisticSessionWebMachine = 605;
        public const int DssXmlStatisticSessionServerInstance = 606;
        public const int DssXmlStatisticSessionProjectID = 607;
        public const int DssXmlStatisticSessionUserID = 608;
        public const int DssXmlStatisticSessionClientMachine = 609;
        public const int DssXmlStatisticSessionConnectTime = 610;
        public const int DssXmlStatisticSessionDisConnectTime = 611;
        public const int DssXmlStatisticSessionSessionEvent = 612;
        public const int DssXmlStatisticSessionEventTime = 613;
        public const int DssXmlStatisticSessionEventSource = 614;
        public const int DssXmlStatisticSessionEventNotes = 615;
        public const int DssXmlStatisticSchedulerSessionID = 616;
        public const int DssXmlStatisticSchedulerServerID = 617;
        public const int DssXmlStatisticSchedulerProjectID = 618;
        public const int DssXmlStatisticSchedulerTriggerID = 619;
        public const int DssXmlStatisticSchedulerScheduleID = 620;
        public const int DssXmlStatisticSchedulerScheduleType = 621;
        public const int DssXmlStatisticSchedulerHitCache = 622;
        public const int DssXmlStatisticSessionLastData = 623;
        public const int DssXmlStatisticReportNewModuleReserved = 700;
        public const int DssXmlStatisticReportServerID = 701;
        public const int DssXmlStatisticReportSessionId = 702;
        public const int DssXmlStatisticReportServerName = 703;
        public const int DssXmlStatisticReportServerMachine = 704;
        public const int DssXmlStatisticReportProjectID = 705;
        public const int DssXmlStatisticReportUserID = 706;
        public const int DssXmlStatisticReportReportID = 707;
        public const int DssXmlStatisticReportReportType = 708;
        public const int DssXmlStatisticReportFilterID = 709;
        public const int DssXmlStatisticReportEmbeddedFilter = 710;
        public const int DssXmlStatisticReportTemplateID = 711;
        public const int DssXmlStatisticReportEmbeddedTemplate = 712;
        public const int DssXmlStatisticReportParentJobID = 713;
        public const int DssXmlStatisticReportDBInstanceID = 714;
        public const int DssXmlStatisticReportDBUserID = 715;
        public const int DssXmlStatisticReportParentIndicator = 716;
        public const int DssXmlStatisticReportRequestRecTime = 717;
        public const int DssXmlStatisticReportRequestQueueTime = 718;
        public const int DssXmlStatisticReportExecStartTime = 719;
        public const int DssXmlStatisticReportExecFinishTime = 720;
        public const int DssXmlStatisticReportSQLPasses = 721;
        public const int DssXmlStatisticReportJobStatus = 722;
        public const int DssXmlStatisticReportJobErrorCode = 723;
        public const int DssXmlStatisticReportCancelledIndicator = 724;
        public const int DssXmlStatisticReportAdhocIndicator = 725;
        public const int DssXmlStatisticReportPromptIndicator = 726;
        public const int DssXmlStatisticReportDatamartIndicator = 727;
        public const int DssXmlStatisticReportElementLoadIndicator = 728;
        public const int DssXmlStatisticReportDrillIndicator = 729;
        public const int DssXmlStatisticReportScheduleIndicator = 730;
        public const int DssXmlStatisticReportCacheCreateIndicator = 731;
        public const int DssXmlStatisticReportPriorityNumber = 732;
        public const int DssXmlStatisticReportJobCost = 733;
        public const int DssXmlStatisticReportFinalResultSize = 734;
        public const int DssXmlStatisticReportErrorMessage = 735;
        public const int DssXmlStatisticReportSecurityJobID = 736;
        public const int DssXmlStatisticReportSecuritySessionID = 737;
        public const int DssXmlStatisticReportSecurityServerID = 738;
        public const int DssXmlStatisticReportSecurityProjectID = 739;
        public const int DssXmlStatisticReportSecurityFilterID = 740;
        public const int DssXmlStatisticReportNewLastData = 741;
        public const int DssXmlStatisticReportDetailModuleReserved = 800;
        public const int DssXmlStatisticReportDetailStepSequence = 801;
        public const int DssXmlStatisticReportDetailSessionID = 802;
        public const int DssXmlStatisticReportDetailServerID = 803;
        public const int DssXmlStatisticReportDetailProjectID = 804;
        public const int DssXmlStatisticReportDetailStepType = 805;
        public const int DssXmlStatisticReportDetailStartTime = 806;
        public const int DssXmlStatisticReportDetailFinishTime = 807;
        public const int DssXmlStatisticReportDetailQueueTime = 808;
        public const int DssXmlStatisticReportDetailCPUTime = 809;
        public const int DssXmlStatisticReportDetailStepDuration = 810;
        public const int DssXmlStatisticReportDetailSQLSessionID = 811;
        public const int DssXmlStatisticReportDetailSQLServerID = 812;
        public const int DssXmlStatisticReportDetailSQLProjectID = 813;
        public const int DssXmlStatisticReportDetailSQLPassSequence = 814;
        public const int DssXmlStatisticReportDetailSQLPassStartTime = 815;
        public const int DssXmlStatisticReportDetailSQLPassFinishTime = 816;
        public const int DssXmlStatisticReportDetailSQLPassExecTime = 817;
        public const int DssXmlStatisticReportDetailSQLStatement = 818;
        public const int DssXmlStatisticReportDetailSQLPassType = 819;
        public const int DssXmlStatisticReportDetailSQLPassTotalTableAccessed = 820;
        public const int DssXmlStatisticReportDetailDBErrorMessage = 821;
        public const int DssXmlStatisticReportDetailColSessionID = 822;
        public const int DssXmlStatisticReportDetailColServerID = 823;
        public const int DssXmlStatisticReportDetailColProjectID = 824;
        public const int DssXmlStatisticReportDetailColTableID = 825;
        public const int DssXmlStatisticReportDetailColColumnID = 826;
        public const int DssXmlStatisticReportDetailColColumnName = 827;
        public const int DssXmlStatisticReportDetailColSQLClauseTypeID = 828;
        public const int DssXmlStatisticReportDetailColCounter = 829;
        public const int DssXmlStatisticReportDetailLastData = 830;
        public const int DssXmlStatisticCacheModuleReserved = 900;
        public const int DssXmlStatisticCacheIndex = 901;
        public const int DssXmlStatisticCacheSessionID = 902;
        public const int DssXmlStatisticCacheServerID = 903;
        public const int DssXmlStatisticCacheProjectID = 904;
        public const int DssXmlStatisticCacheHitTime = 905;
        public const int DssXmlStatisticCacheHitType = 906;
        public const int DssXmlStatisticCacheCreatorJobID = 907;
        public const int DssXmlStatisticCacheCreatorSessionID = 908;
        public const int DssXmlStatisticParentJobID = 909;
        public const int DssXmlStatisticCacheLastData = 910;
        public const int DssXmlStatisticDocumentModuleReserved = 1000;
        public const int DssXmlStatisticDocumentServerID = 1001;
        public const int DssXmlStatisticDocumentSessionID = 1002;
        public const int DssXmlStatisticDocumentServerName = 1003;
        public const int DssXmlStatisticDocumentServerMachine = 1004;
        public const int DssXmlStatisticDocumentProjectID = 1005;
        public const int DssXmlStatisticDocumentUserID = 1006;
        public const int DssXmlStatisticDocumentDocumentID = 1007;
        public const int DssXmlStatisticDocumentRequestRecTime = 1008;
        public const int DssXmlStatisticDocumentRequestQueueTime = 1009;
        public const int DssXmlStatisticDocumentQueueTime = 1010;
        public const int DssXmlStatisticDocumentStartTime = 1011;
        public const int DssXmlStatisticDocumentFinishTime = 1012;
        public const int DssXmlStatisticDocumentJobFinishTime = 1013;
        public const int DssXmlStatisticDocumentExecStatus = 1014;
        public const int DssXmlStatisticDocumentExecErrorCode = 1015;
        public const int DssXmlStatisticDocumentReportNumber = 1016;
        public const int DssXmlStatisticDocumentCancelledIndicator = 1017;
        public const int DssXmlStatisticDocumentPromptIndicator = 1018;
        public const int DssXmlStatisticDocumentCachedIndicator = 1019;
        public const int DssXmlStatisticDocumentStepJobID = 1020;
        public const int DssXmlStatisticDocumentStepSequence = 1021;
        public const int DssXmlStatisticDocumentStepSessionID = 1022;
        public const int DssXmlStatisticDocumentStepServerID = 1023;
        public const int DssXmlStatisticDocumentStepProjectID = 1024;
        public const int DssXmlStatisticDocumentStepType = 1025;
        public const int DssXmlStatisticDocumentStepStartTime = 1026;
        public const int DssXmlStatisticDocumentStepFinishTime = 1027;
        public const int DssXmlStatisticDocumentStepQueueTime = 1028;
        public const int DssXmlStatisticDocumentStepCPUTime = 1029;
        public const int DssXmlStatisticDocumentStepDuration = 1030;
        public const int DssXmlStatisticDocumentLastData = 1031;
        public const int DssXmlStatisticProjSessSessionID = 1100;
        public const int DssXmlStatisticProjSessServerID = 0;
        public const int DssXmlStatisticProjSessServerName = 0;
        public const int DssXmlStatisticProjSessServerMachine = 0;
        public const int DssXmlStatisticProjSessUserID = 0;
        public const int DssXmlStatisticProjSessProjectID = 0;
        public const int DssXmlStatisticProjSessConnectTime = 0;
        public const int DssXmlStatisticProjSessDisconnectTime = 0;
        public const int DssXmlStatisticProjSessEventNotes = 0;
        public const int DssXmlStatisticRepManipJobID = 1200;
        public const int DssXmlStatisticRepManipSessionID = 0;
        public const int DssXmlStatisticRepManipServerID = 0;
        public const int DssXmlStatisticRepManipProjectID = 0;
        public const int DssXmlStatisticRepManipManipSequenceID = 0;
        public const int DssXmlStatisticRepManipManipType = 0;
        public const int DssXmlStatisticRepManipManipXML = 0;
        public const int DssXmlStatisticRepManipStartTime = 0;
        public const int DssXmlStatisticRepManipFinishTime = 0;
        public const int DssXmlStatisticRepManipQueueTime = 0;
        public const int DssXmlStatisticRepManipCPUTime = 0;
        public const int DssXmlStatisticRepManipManipDuration = 0;
        public const int DssXmlStatisticRepManipChildJobID = 0;
        public const int DssXmlStatisticMemoryJobID = 1300;
        public const int DssXmlStatisticMemorySessionID = 0;
        public const int DssXmlStatisticMemoryServerID = 0;
        public const int DssXmlStatisticMemoryProjectID = 0;
        public const int DssXmlStatisticMemorySequenceID = 0;
        public const int DssXmlStatisticMemoryMemoryAMT = 0;
        public const int DssXmlStatisticMemoryFailureIndic = 0;
        public const int DssXmlStatisticMemoryExecutionType = 0;
        public const int DssXmlStatisticPrmptAnsTableReserved = 1400;
        public const int DssXmlStatisticPrmptAnsJobID = 0;
        public const int DssXmlStatisticPrmptAnsSessionID = 0;
        public const int DssXmlStatisticPrmptAnsOrderID = 0;
        public const int DssXmlStatisticPrmptAnsLocationGUID = 0;
        public const int DssXmlStatisticPrmptAnsLocationType = 0;
        public const int DssXmlStatisticPrmptAnsPromptAnswers = 0;
        public const int DssXmlStatisticPrmptAnsAnswersType = 0;
        public const int DssXmlStatisticPrmptAnsTitle = 0;
        public const int DssXmlStatisticPrmptAnsName = 0;
        public const int DssXmlStatisticPrmptAnsGUID = 0;
        public const int DssXmlStatisticPrmptAnsLocationDesc = 0;
        public const int DssXmlStatisticPrmptAnsIsRequired = 0;
        public const int DssXmlStatisticPrmptAnsAnswersGUID = 0;
        public const int DssXmlStatisticPrmptAnsIsDefault = 0;
        public const int DssXmlStatisticPrmptAnsProjectID = 0;
        public const int DssXmlStatisticPrmptAnsServerMachine = 0;
        public const int DssXmlStatisticPrmptAnsServerID = 0;
    }

    public class DSSXMLStatisticModuleID
    {
        public const int DssXmlStatisticModuleIDReserved = 0;
        public const int DssXmlStatisticModuleUser = 1;
        public const int DssXmlStatisticModuleReport = 2;
        public const int DssXmlStatisticModuleMetadata = 3;
        public const int DssXmlStatisticModuleElementReque = 4;
        public const int DssXmlStatisticModuleReportStepLevel = 5;
        public const int DssXmlStatisticModuleSession = 6;
        public const int DssXmlStatisticModuleReportBasic = 7;
        public const int DssXmlStatisticModuleReportDetail = 8;
        public const int DssXmlStatisticModuleCache = 9;
        public const int DssXmlStatisticModuleDocument = 10;
        public const int DssXmlStatisticLastModule = 10;
    }

    public class DSSXMLStatisticTableID
    {
        public const int DssXmlStatisticTableIDReserved = 0;
        public const int DssXmlStatisticSessionTableID = 1;
        public const int DssXmlStatisticScheduleTableID = 2;
        public const int DssXmlStatisticReportTableID = 3;
        public const int DssXmlStatisticReportSecurityTableID = 4;
        public const int DssXmlStatisticReportStepTableID = 5;
        public const int DssXmlStatisticReportSQLTableID = 6;
        public const int DssXmlStatisticCacheHitTableID = 7;
        public const int DssXmlStatisticDocumentTableID = 8;
        public const int DssXmlStatisticDocumentStepTableID = 9;
        public const int DssXmlStatisticReportColTableID = 10;
        public const int DssXmlStatisticProjSessionTableID = 11;
        public const int DssXmlStatisticReportManipTableID = 12;
        public const int DssXmlStatisticMemoryTableID = 13;
        public const int DssXmlStatisticPromptAnswersTableID = 14;
        public const int DssXmlStatisticPerfMonTableID = 15;
        public const int DssXmlStatisticMessageTableID = 16;
        public const int DssXmlStatisticInBoxActTableID = 17;
        public const int DssXmlStatisticCubeRepTableID = 18;
        public const int DssXmlStatisticMBDeviceTableID = 19;
        public const int DssXmlStatisticMBExecTableID = 20;
        public const int DssXmlStatisticMBManipTableID = 21;
        public const int DssXmlStatisticLastTable = 21;
    }

    public class DSSXMLStatus
    {
        public const int DssXmlStatusMsgID = 0;
        public const int DssXmlStatusResult = 1;
        public const int DssXmlStatusPromptXML = 2;
        public const int DssXmlStatusErrMsgXML = 3;
        public const int DssXmlStatusJobRunning = 4;
        public const int DssXmlStatusInSQLEngine = 5;
        public const int DssXmlStatusInQueryEngine = 6;
        public const int DssXmlStatusInAnalyticalEngine = 7;
        public const int DssXmlStatusInResolution = 8;
        public const int DssXmlStatusWaitingForCache = 9;
        public const int DssXmlStatusUpdatingCache = 10;
        public const int DssXmlStatusWaiting = 13;
        public const int DssXmlStatusWaitingOnGovernor = 14;
        public const int DssXmlStatusWaitingForProject = 15;
        public const int DssXmlStatusWaitingForChildren = 16;
        public const int DssXmlStatusPreparingOutput = 17;
        public const int DssXmlStatusConstructResult = 19;
        public const int DssXmlStatusHTMLResult = 20;
        public const int DssXmlStatusXMLResult = 21;
        public const int DssXmlStatusRunningOnOtherNode = 22;
        public const int DssXmlLoadingPrompt = 23;
        public const int DssXmlInExportEngine = 24;
    }

    public class DSSXMLStepTypes
    {
        public const int DssXmlFontStepAutomatic = 0;
        public const int DssXmlFontStepManual = 1;
    }

    public class DSSXMLSubscriptionContactType
    {
        public const int DssXmlSubscriptionContactTypeReserved = 0;
        public const int DssXmlSubscriptionContactTypeContact = 1;
        public const int DssXmlSubscriptionContactTypeContactCollection = 2;
        public const int DssXmlSubscriptionContactTypeMSTRUser = 4;
        public const int DssXmlSubscriptionContactTypeMSTRUserGroup = 8;
        public const int DssXmlSubscriptionContactTypeLDAPUser = 16;
        public const int DssXmlSubscriptionContactTypeDRL = 32;
        public const int DssXmlSubscriptionContactTypeAll = 63;
        public const int DssXmlSubscriptionContactTypeCount = 6;
    }

    public class DSSXMLSubscriptionDeliveryType
    {
        public const int DssXmlDeliveryTypeReserved = 0;
        public const int DssXmlDeliveryTypeEmail = 1;
        public const int DssXmlDeliveryTypeFile = 2;
        public const int DssXmlDeliveryTypePrinter = 4;
        public const int DssXmlDeliveryTypeCustom = 8;
        public const int DssXmlDeliveryTypeInbox = 16;
        public const int DssXmlDeliveryTypeClient = 32;
        public const int DssXmlDeliveryTypeCache = 64;
        public const int DssXmlDeliveryTypeMobile = 128;
        public const int DssXmlDeliveryTypeMobileBlackberry = 256;
        public const int DssXmlDeliveryTypeMobileIPhone = 512;
        public const int DssXmlDeliveryTypeMobileIPad = 1024;
        public const int DssXmlDeliveryTypeSnapshot = 4096;
        public const int DssXmlDeliveryTypeLastOne = 8192;
        public const int DssXmlDeliveryTypeCount = 10;
        public const int DssXmlDeliveryTypeAll = 4095;
        public const int DssXmlDeliveryTypeAllIncludingSnapshot = 8191;
    }

    public class DSSXMLSubscriptionMobileClientType
    {
        public const int DssXmlMobileClientTypeReserved = 0;
        public const int DssXmlMobileClientTypeBlackberry = 1;
        public const int DssXmlMobileClientTypeiPhone = 2;
        public const int DssXmlMobileClientTypeiPad = 4;
    }

    public class DSSXMLSubscriptionPersonalViewMode
    {
        public const int DssXmlPersonalViewModePrivate = 0;
        public const int DssXmlPersonalViewModePublic = 1;
    }

    public class DSSXMLSubtotalsPosition
    {
        public const int DssXmlSubtotalsPositionMix = 1;
        public const int DssXmlSubtotalsPositionFirst = 2;
        public const int DssXmlSubtotalsPositionLast = 3;
        public const int DssXmlSubtotalsPositionInherit = 4;
    }

    public class DSSXMLSubtotalStyle
    {
        public const int DssXmlSubtotalNone = 1;
        public const int DssXmlSubtotalGrandTotal = 2;
        public const int DssXmlSubtotalOutline = 3;
        public const int DssXmlSubtotalCube = 4;
    }

    public class DSSXMLSymbol
    {
        public const int DssXmlSymbolReserved = 0;
        public const int DssXmlSymbolBlackCircle = 1;
        public const int DssXmlSymbolBlackSquare = 2;
        public const int DssXmlSymbolLozenge = 3;
        public const int DssXmlSymbolBlackDiamond = 4;
        public const int DssXmlSymbolInkBlot = 5;
        public const int DssXmlSymbolWheelofDharma = 6;
        public const int DssXmlSymbolBlackFlorette = 7;
        public const int DssXmlSymbolPlaceInterestSign = 8;
        public const int DssXmlSymbolHeavyCheckMark = 9;
        public const int DssXmlSymbolHeavyBallotX = 10;
        public const int DssXmlSymbolRightArrow = 29;
        public const int DssXmlSymbolUpArrow = 30;
        public const int DssXmlSymbolDownArrow = 31;
    }

    public class DSSXMLSystemLinks
    {
        public const int DssXmlSystemLinkReserved = 0;
        public const int DssXmlSystemLinkMDSecurity = 1;
        public const int DssXmlSystemLinkSecurityRole = 2;
        public const int DssXmlSystemLinkDBConnectionMap = 3;
        public const int DssXmlSystemLinkProfile = 4;
        public const int DssXmlSystemLinkPreferences = 5;
        public const int DssXmlSystemLinkSchedules = 6;
        public const int DssXmlSystemLinkReportSchedules = 7;
        public const int DssXmlSystemLinkDocumentSchedules = 8;
        public const int DssXmlSystemLinkGeneralSchedules = 9;
        public const int DssXmlSystemLinkReportScheduleRelation = 10;
        public const int DssXmlSystemLinkDocumentScheduleRelation = 11;
        public const int DssXmlSystemLinkWebPreferences1 = 12;
        public const int DssXmlSystemLinkWebPreferences2 = 13;
        public const int DssXmlSystemLinkWebPreferences3 = 14;
        public const int DssXmlSystemLinkReportAnnotations = 17;
        public const int DssXmlSystemLinkDocumentAnnotations = 18;
        public const int DssXmlSystemLinkLocalizedConnectionMap = 19;
        public const int DssXmlSystemLinkCacheInvalidationSchedule = 20;
        public const int DssXmlSystemLinkDefaultLocalizedConnection = 21;
        public const int DssXmlSystemLinkReservedLastOne = 22;
        public const int DssXmlSystemLinkQuota = 23;
    }

    public class DSSXMLTableExtraInformation
    {
        public const int DssTableExtraInformationDefault = 0;
        public const int DssTableExtraInformationNotForHDAMDX = 1;
    }

    public class DSSXMLTemplateSubtotalType
    {
        public const int DssXMLTemplateSubtotalReserved = 0;
        public const int DssXMLTemplateSubtotalList = -1;
        public const int DssXMLTemplateSubtotalDefinition = -2;
        public const int DssXMLTemplateSubtotalDefault = -3;
        public const int DssXMLTemplateSubtotalAll = -4;
    }

    public class DSSXMLTemplateUnitType
    {
        public const int DssXmlTemplateReserved = 0;
        public const int DssXmlTemplateAttribute = 1;
        public const int DssXmlTemplateDimension = 2;
        public const int DssXmlTemplateMetrics = 3;
        public const int DssXmlTemplateCustomGroup = 4;
        public const int DssXmlTemplateConsolidation = 5;
        public const int DssXmlTemplatePrompt = 6;
        public const int DssXmlTemplateRawUnit = 7;
    }

    public class DSSXMLThresholdScope
    {
        public const int DssThresholdScopeMetricOnly = 1;
        public const int DssThresholdScopeSubtotalOnly = 2;
        public const int DssThresholdScopeMetricAndSubtotal = 3;
    }

    public class DSSXMLTimeOccurrenceType
    {
        public const int DssXmlTimeNoRecurrence = -100;
        public const int DssXmlTimeLastOccurrence = -1;
        public const int DssXmlTimeOneOccurrence = -3;
        public const int DssXmlTimeEveryOccurrence = -2;
    }

    public class DSSXMLTokenLevels
    {
        public const int DssXmlTokenLevelReserved = 0;
        public const int DssXmlTokenLevelClient = 1;
        public const int DssXmlTokenLevelLexed = 2;
        public const int DssXmlTokenLevelResolved = 3;
        public const int DssXmlTokenLevelParsed = 4;
    }

    public class DSSXMLTokenSectionTypes
    {
        public const int DssXmlTokenSectionTypeReserved = 0;
        public const int DssXmlTokenSectionTypeFunction = 65536;
        public const int DssXmlTokenSectionTypeFunctionParameters = 131072;
        public const int DssXmlTokenSectionTypeFunctionParameterSeparator = 131073;
        public const int DssXmlTokenSectionTypeFunctionArguments = 196608;
        public const int DssXmlTokenSectionTypeFunctionArgumentSeparator = 196609;
        public const int DssXmlTokenSectionTypeDimensionality = 262144;
        public const int DssXmlTokenSectionTypeDimensionalityUnitSeparator = 262145;
        public const int DssXmlTokenSectionTypeConditionality = 327680;
        public const int DssXmlTokenSectionTypeTransformations = 393216;
        public const int DssXmlTokenSectionTypeTransformationsUnitSeparator = 393217;
    }

    public class DSSXMLTokenStates
    {
        public const int DssXmlTokenStateError = -1;
        public const int DssXmlTokenStateReserved = 0;
        public const int DssXmlTokenStateInitial = 1;
        public const int DssXmlTokenStateOkay = 2;
    }

    public class DSSXMLTokenTypes
    {
        public const int DssXmlTokenTypeEndOfInput = -1;
        public const int DssXmlTokenTypeReserved = 0;
        public const int DssXmlTokenTypeError = 1;
        public const int DssXmlTokenTypeUnknown = 2;
        public const int DssXmlTokenTypeEmpty = 3;
        public const int DssXmlTokenTypeExclamationPoint = 33;
        public const int DssXmlTokenTypeNumberSign = 35;
        public const int DssXmlTokenTypeDollar = 36;
        public const int DssXmlTokenTypePercent = 37;
        public const int DssXmlTokenTypeAmpersand = 38;
        public const int DssXmlTokenTypeLeftParen = 40;
        public const int DssXmlTokenTypeRightParen = 41;
        public const int DssXmlTokenTypeAsterisk = 42;
        public const int DssXmlTokenTypePlusSign = 43;
        public const int DssXmlTokenTypeComma = 44;
        public const int DssXmlTokenTypeMinusSign = 45;
        public const int DssXmlTokenTypeDot = 46;
        public const int DssXmlTokenTypeFwdSlash = 47;
        public const int DssXmlTokenTypeColon = 58;
        public const int DssXmlTokenTypeSemicolon = 59;
        public const int DssXmlTokenTypeLeftAngleBracket = 60;
        public const int DssXmlTokenTypeEqualSign = 61;
        public const int DssXmlTokenTypeRightAngleBracket = 62;
        public const int DssXmlTokenTypeQuestionMark = 63;
        public const int DssXmlTokenTypeAtSign = 64;
        public const int DssXmlTokenTypeLeftSquareBracket = 91;
        public const int DssXmlTokenTypeRightSquareBracket = 93;
        public const int DssXmlTokenTypeCaret = 94;
        public const int DssXmlTokenTypeUnderscore = 95;
        public const int DssXmlTokenTypeLeftAccolade = 123;
        public const int DssXmlTokenTypeVerticalBar = 124;
        public const int DssXmlTokenTypeRightAccolade = 125;
        public const int DssXmlTokenTypeTilda = 126;
        public const int DssXmlTokenTypeLiteral = 258;
        public const int DssXmlTokenTypeIdentifier = 259;
        public const int DssXmlTokenTypeReserved1 = 260;
        public const int DssXmlTokenTypeDoubleQString = 261;
        public const int DssXmlTokenTypeInteger = 262;
        public const int DssXmlTokenTypeFloat = 263;
        public const int DssXmlTokenTypeAttrRef = 264;
        public const int DssXmlTokenTypeDimRef = 265;
        public const int DssXmlTokenTypeMetricRef = 266;
        public const int DssXmlTokenTypeAggMetricRef = 267;
        public const int DssXmlTokenTypeFactRef = 268;
        public const int DssXmlTokenTypeReportRef = 269;
        public const int DssXmlTokenTypeFilterRef = 270;
        public const int DssXmlTokenTypeRoleRef = 271;
        public const int DssXmlTokenTypeTableRef = 272;
        public const int DssXmlTokenTypePromptRef = 273;
        public const int DssXmlTokenTypeColumnRef = 274;
        public const int DssXmlTokenTypeAttrFormRef = 275;
        public const int DssXmlTokenTypeDimFormRef = 276;
        public const int DssXmlTokenTypeSimpInfixFun1 = 277;
        public const int DssXmlTokenTypeSimpInfixFun2 = 278;
        public const int DssXmlTokenTypeSimpPrefixFun = 279;
        public const int DssXmlTokenTypeAggFun = 280;
        public const int DssXmlTokenTypeRelativeFun = 281;
        public const int DssXmlTokenTypeCompareFun = 282;
        public const int DssXmlTokenTypeAndFun = 283;
        public const int DssXmlTokenTypeOrFun = 284;
        public const int DssXmlTokenTypeNotFun = 285;
        public const int DssXmlTokenTypeBoolPrefixFun = 286;
        public const int DssXmlTokenTypeIntersectFun = 287;
        public const int DssXmlTokenTypeReserved2 = 288;
        public const int DssXmlTokenTypeInSetFun = 289;
        public const int DssXmlTokenTypeBetweenFun = 290;
        public const int DssXmlTokenTypeLikeFun = 291;
        public const int DssXmlTokenTypeEnhCompareFun = 292;
        public const int DssXmlTokenTypeEnhBetweenFun = 293;
        public const int DssXmlTokenTypeArrowUp = 294;
        public const int DssXmlTokenTypeArrowDown = 295;
        public const int DssXmlTokenTypeArrowUpDown = 296;
        public const int DssXmlTokenTypeAggFirstInFact = 297;
        public const int DssXmlTokenTypeAggLastInFact = 298;
        public const int DssXmlTokenTypeKwdTrue = 299;
        public const int DssXmlTokenTypeKwdFalse = 300;
        public const int DssXmlTokenTypeKwdBreakBy = 301;
        public const int DssXmlTokenTypeKwdSortBy = 302;
        public const int DssXmlTokenTypeKwdFactID = 303;
        public const int DssXmlTokenTypeDateTime = 304;
        public const int DssXmlTokenTypeCaseFun = 305;
        public const int DssXmlTokenTypeFreeVariable = 306;
        public const int DssXmlTokenTypeLongNumber = 307;
        public const int DssXmlTokenTypeRWMetricRef = 308;
        public const int DssXmlTokenTypeCSQLRef = 309;
        public const int DssXmlTokenTypeConsolidationRef = 310;
        public const int DssXmlTokenTypeEndOfTypes = 340;
    }

    public class DSSXMLTriggerOptions
    {
        public const int DssXmlTriggerReserved = 0;
        public const int DssXmlTriggerBrowse = 1;
        public const int DssXmlTriggerDetails = 2;
    }

    public class DSSXMLTrustWebServerStatus
    {
        public const int DssXmlTrustWebServerStatusEnabled = 1;
        public const int DssXmlTrustWebServerStatusRemoved = 32768;
    }

    public class DSSXMLUnitTransactionFlags
    {
        public const int DssXmlUnitTransactionReserved = 0;
        public const int DssXmlUnitTransactionEditable = 1;
        public const int DssXmlUnitTransactionRequired = 268435456;
    }

    public class DSSXMLUpdateTransactionDataType
    {
        public const int DssXmlTransactionUpdateReserved = 0;
        public const int DssXmlTransactionUpdateAttributeFormOnTemplateNode = 1;
        public const int DssXmlTransactionUpdateMetricOnTemplateNode = 2;
        public const int DssXmlTransactionUpdateField = 3;
        public const int DssXmlTransactionMarkOnTemplateNode = 4;
        public const int DssXmlTransactionUnMarkOnTemplateNode = 5;
    }

    public class DSSXMLUserAccountServiceMethod
    {
        public const int DssXmlUserAccountServiceCreateUser = 1;
        public const int DssXmlUserAccountServiceDeleteUser = 2;
        public const int DssXmlUserAccountServiceAddMember = 3;
        public const int DssXmlUserAccountServiceRemoveMember = 4;
        public const int DssXmlUserAccountServiceClearMembership = 5;
        public const int DssXmlUserAccountServiceDuplicateUser = 6;
        public const int DssXmlUserAccountServiceGetSecurityFilters = 7;
        public const int DssXmlUserAccountServiceCreateGroup = 8;
        public const int DssXmlUserAccountServiceSetSecurityFilters = 9;
        public const int DssXmlUserAccountServiceDuplicateGroup = 10;
        public const int DssXmlUserAccountServiceAdministrator = 11;
        public const int DssXmlUserAccountServiceEveryone = 12;
        public const int DssXmlUserAccountServicePublic = 13;
        public const int DssXmlUserAccountServiceSystemAdmins = 14;
        public const int DssXmlUserAccountServiceSystemMonitors = 15;
        public const int DssXmlUserAccountServiceUsers = 16;
        public const int DssXmlUserAccountServiceGroups = 17;
        public const int DssXmlUserAccountServiceCreateProfile = 18;
        public const int DssXmlUserAccountServiceDeleteProfile = 19;
        public const int DssXmlUserAccountServiceFindProfileFolder = 20;
        public const int DssXmlUserAccountServiceGetAnalysisQuota = 21;
    }

    public class DSSXMLUserAnswerCommands
    {
        public const int DssXmlUserAnswerReserved = 0;
        public const int DssXmlUserAnswerLoad = 1;
        public const int DssXmlUserAnswerSave = 2;
        public const int DssXmlUserAnswerRemove = 3;
    }

    public class DSSXMLUserConnectionInfo
    {
        public const int DssXmlUserConnUserName = 0;
        public const int DssXmlUserConnProjectName = 1;
        public const int DssXmlUserConnOpenJobs = 2;
        public const int DssXmlUserConnSource = 3;
        public const int DssXmlUserConnConnectTime = 4;
        public const int DssXmlUserConnFirstJobTime = 5;
        public const int DssXmlUserConnLastJobTime = 6;
        public const int DssXmlUserConnDuration = 7;
        public const int DssXmlUserConnServerProjectId = 8;
        public const int DssXmlUserConnProjectGUID = 9;
        public const int DssXmlUserConnSessionID = 10;
        public const int DssXmlUserConnClientMachine = 11;
        public const int DssXmlUserConnPort = 12;
        public const int DssXmlUserConnServerName = 13;
        public const int DssXmlUserConnUserLoginName = 14;
    }

    public class DSSXMLUserFilter
    {
        public const int DssXmlUserFilterIgnore = 1;
        public const int DssXmlUserFilterApply = 2;
        public const int DssXmlUserFilterRelation = 3;
        public const int DssXmlUserFilterEvaluate = 4;
    }

    public class DSSXMLValidationLevel
    {
        public const int DssXmlValidationGood = 1;
        public const int DssXmlValidationRecoverable = 2;
        public const int DssXmlValidationFatal = 3;
        public const int DssXmlValidationBadApplyOrder = 4;
    }

    public class DSSXMLVAreaMinorTypes
    {
        public const int DssXmlVAreaMinorTypeAbsolute = 1;
        public const int DssXmlVAreaMinorTypeStacked = 2;
        public const int DssXmlVAreaMinorTypeBiPolarAbsolute = 4;
        public const int DssXmlVAreaMinorTypeBiPolarStacked = 8;
        public const int DssXmlVAreaMinorTypeDualAxisAbsolute = 16;
        public const int DssXmlVAreaMinorTypeDualAxisStacked = 32;
        public const int DssXmlVAreaMinorTypePercent = 64;
    }

    public class DSSXMLVBarMinorTypes
    {
        public const int DssXmlVBarMinorTypeSideBySide = 1;
        public const int DssXmlVBarMinorTypeStacked = 2;
        public const int DssXmlVBarMinorTypeDualAxisSideBySide = 16;
        public const int DssXmlVBarMinorTypeDualAxisStacked = 32;
        public const int DssXmlVBarMinorTypeBiPolarSideBySide = 4;
        public const int DssXmlVBarMinorTypeBiPolarStacked = 8;
        public const int DssXmlVBarMinorTypePercent = 64;
        public const int DssXmlVBarMinorTypeAbsolute = 128;
    }

    public class DSSXMLViewMedia
    {
        public const int DssXmlViewMediaReserved = 0;
        public const int DssXmlViewMediaViewStatic = 1;
        public const int DssXmlViewMediaViewInteractive = 2;
        public const int DssXmlViewMediaViewEditable = 4;
        public const int DssXmlViewMediaViewFlash = 8;
        public const int DssXmlViewMediaExportExcel = 16;
        public const int DssXmlViewMediaExportPDF = 32;
        public const int DssXmlViewMediaExportHTML = 64;
        public const int DssXmlViewMediaExportFlash = 128;
        public const int DssXmlViewMediaExportExcelPlainText = 256;
        public const int DssXmlViewMediaExportCSV = 512;
        public const int DssXmlViewMediaExportPlainText = 1024;
        public const int DssXmlViewMediaViewAnalysis = 2048;
        public const int DSSXmlViewMediaCustomSQL = 5632;
        public const int DssXmlViewMediaAll = 134217727;
    }

    public class DSSXMLVLineMinorTypes
    {
        public const int DssXmlVLineMinorTypeAbsolute = 1;
        public const int DssXmlVLineMinorTypeStacked = 2;
        public const int DssXmlVLineMinorTypeBiPolarAbsolute = 4;
        public const int DssXmlVLineMinorTypeBiPolarStacked = 8;
        public const int DssXmlVLineMinorTypeDualAxisAbsolute = 16;
        public const int DssXmlVLineMinorTypeDualAxisStacked = 32;
        public const int DssXmlVLineMinorTypePercent = 64;
    }

    public class DSSXMLWidthScenario
    {
        public const int DssXmlWidthFitToDisplay = 1;
        public const int DssXmlWidthFitToContent = 2;
        public const int DssXmlWidthFixed = 3;
    }

    public class DSSXMLXDAType
    {
        public const int DssXmlXDATypeReserved = 0;
        public const int DssXmlXDATypeRelational = 1;
        public const int DssXmlXDATypeMDX = 2;
        public const int DssXmlXDATypeCustomSQLFreeForm = 3;
        public const int DssXmlXDATypeCustomSQLWizard = 4;
        public const int DssXmlXDATypeFlatFile = 5;
        public const int DssXDATypeDataImport = 256;
        public const int DssXDATypeDataImportFileExcel = 272;
        public const int DssXDATypeDataImportFileText = 288;
        public const int DssXDATypeDataImportCustomSQL = 304;
        public const int DssXDATypeDataImportTable = 320;
        public const int DssXDATypeDataImportOAuth = 336;
        public const int DssXDATypeDataImportOldSFDC = 336;
        public const int DssXDATypeDataImportSFDC = 337;
        public const int DssXDATypeDataImportOAuthSFDC = 337;
        public const int DssXDATypeDataImportOAuthGDocs = 338;
        public const int DssXDATypeReduceMask = -256;
        public const int DssXDATypeGroupMask = -256;
        public const int DssXDATypeGroupMaskDataImport = -16;
    }

    public class FillColorEffects
    {
        public const int DssGraphFillSimple = 0;
        public const int DssGraphFillPicture = 4;
        public const int DssGraphFillAdvancedWash = 14;
    }

    public class MSIProjectStatus
    {
        public const int gcProjectOfflinePending = -2;
        public const int gcProjectOffline = -1;
        public const int gcProjectActive = 0;
        public const int gcProjectExecIdle = 1;
        public const int gcProjectEntryIdle = 2;
        public const int gcProjectFullIdle = 3;
    }


}
