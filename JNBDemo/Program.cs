﻿using System;
using com.microstrategy.web.objects;
using java.lang;
using JNBDemo.Constants;

namespace JNBDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = WebObjectsFactory.getInstance();
            var serverSession = factory.getIServerSession();

            serverSession.setServerPort(0);
            serverSession.setAuthMode(DSSXMLAuthModes.DssXmlAuthTrusted);
            //serverSession.setAuthMode(EnumDSSXMLAuthModes.DssXmlAuthStandard);
            serverSession.setProjectName("KRO_DVIS");
            serverSession.setLogin("monster-user@m6demandview.com");
            //serverSession.setLogin("josh.schlesinger@market6.com");
            //serverSession.setPassword("");
            serverSession.setTrustToken("Token203E64E64F00B8FFFD506CA21EF3C6F6");
            serverSession.setServerName("HDCDKMSI01");
            serverSession.setApplicationType(DSSXMLApplicationType.DssXmlApplicationCustomApp);

            var wos = factory.getObjectSource();
            var search = wos.getNewSearchObject();

            search.setDisplayName("*");

            search.setSearchFlags(search.getSearchFlags() | DSSXMLSearchFlags.DssXmlSearchVisibleOnly | DSSXMLSearchFlags.DssXmlSearchNameWildCard);

            search.setAsync(false);

            search.types().add(new Integer(DSSXMLObjectTypes.DssXmlTypeDimension));

            search.submit();

            var f = search.getResults();

            for (var i = 0; i < f.size(); i++)
            {
                var webObject = f.get(i);
                Console.WriteLine(webObject);
                var dim = (WebDimension)webObject;
                if (dim.getName().StartsWith("Product"))
                {
                    dim.populate();
                    Console.WriteLine("Dim Name: {0}", dim.getName());
                    Console.WriteLine("Dim Type: {0}", dim.getType());
                    Console.WriteLine("Dim Size: {0}", dim.size());
                    for (int j = 0; j < dim.size(); j++)
                    {
                        var dimAtt = dim.get(j);
                        Console.WriteLine("Dim Att Name: {0}", dimAtt.getDisplayName());
                    }
                }
                Console.WriteLine();
            }
        }
    }

}
